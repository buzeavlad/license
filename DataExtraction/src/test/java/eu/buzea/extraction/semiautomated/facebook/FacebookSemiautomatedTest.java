package eu.buzea.extraction.semiautomated.facebook;

import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.junit.Test;

import eu.buzea.inputschema.Input;

public class FacebookSemiautomatedTest {

	private static final String OUTPUT_FILE = "./src/test/resources/FacebookSemiautomatedOutput.xml";
	private static final String INPUT_FILE = "./src/test/resources/FacebookSemiautomatedInput.html";

	@Test
	public void facebookHtmlExtractionTest() throws Exception {
		FacebookHtmlDataExtraction dataExtraction = new FacebookHtmlDataExtraction(INPUT_FILE);
		Input input = dataExtraction.extractData();
		JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		FileOutputStream out = new FileOutputStream(OUTPUT_FILE);
		jaxbMarshaller.marshal(input, out);
	}
}
