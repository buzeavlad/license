package eu.buzea.extraction.automated.reddit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.Input.Posts;
import eu.buzea.util.XmlUtil;

public class RedditTest {

	private static final String OUTPUT_FILE1 = "./src/test/resources/RedditHtmlOutput.xml";
	private static final String OUTPUT_FILE2 = "./src/test/resources/RedditExtractionOutput.xml";
	private static final String INPUT_FILE1 = "./src/test/resources/RedditHtmlInput.html";
	private static final String INPUT_FILE2 = "./src/test/resources/RedditExtractionInput.txt";

	@Test
	public void testHtmlParse() throws Exception {
		FileInputStream is = new FileInputStream(new File(INPUT_FILE1));
		String html = IOUtils.toString(is);
		RedditHtmlProcesser processer = new RedditHtmlProcesser();
		Content post = processer.processPost(html);
		Input input = new Input();
		input.setPosts(new Posts());
		input.getPosts().getPost().add(post);
		XmlUtil.serialize(input, new File(OUTPUT_FILE1));

	}

	@Test
	public void testRedditExtraction() throws PropertyException, JAXBException, IOException {
		File input = new File(INPUT_FILE2);
		RedditDataExtraction redditDataExtraction = new RedditDataExtraction("TestData2", input);
		Input output = redditDataExtraction.extractData();
		XmlUtil.serialize(output, new File(OUTPUT_FILE2));

	}

}
