/**
 * 
 */
package eu.buzea.extraction.automated.reddit.start;

import java.io.File;

import eu.buzea.extraction.automated.reddit.RedditDataExtraction;
import eu.buzea.inputschema.Input;
import eu.buzea.util.XmlUtil;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class RedditExtractionMain {

	public static void main(String[] args) {

		if (args.length < 2 || args.length > 4) {
			showMessageAndExit();
		}
		File input = new File(args[0]);
		if (!input.exists() || !input.isFile()) {
			showMessageAndExit();
		}
		String runName = args[1];
		String outputFilePath = "./RedditExtractionOutput.xml";
		boolean isParallelExecution = true;
		if (args.length >= 3) {
			if (!args[2].equals("-serial")) {
				outputFilePath = args[2];
				if (args.length == 4 && args[3].equals("-serial")) {
					isParallelExecution = false;
				}
			} else
				isParallelExecution = false;

		}
		File output = new File(outputFilePath);
		if (output.exists() && input.isDirectory()) {
			showMessageAndExit();
		}

		try {
			RedditDataExtraction entryPoint = new RedditDataExtraction(runName, input);
			entryPoint.setParallelExtraction(isParallelExecution);
			Input result = entryPoint.extractData();
			XmlUtil.serialize(result, output);
		} catch (Exception e) {
			System.err.println("An unexpected error has occured!");
			e.printStackTrace();
		}

	}

	private static void showMessageAndExit() {
		System.err.println("Extraction requires the follwing parameters:");
		System.err.println("<PATH_TO_INPUT_FILE> <RUN_NAME> [<PATH_TO_OUTPUT_FILE>] [-serial]");
		System.exit(1);
	}

}
