package eu.buzea.extraction.automated.reddit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import eu.buzea.inputschema.Content;

public class RedditHtmlProcesser {

	private final Logger LOG = Logger.getLogger(RedditHtmlProcesser.class);
	private static Map<String, Long> users = new HashMap<>();;
	private static long idCount;
	// 2016-03-07T13:37:58+00:00
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public RedditHtmlProcesser() {

	}

	public Content processPost(String html) {
		Document doc = Jsoup.parse(html);
		Element siteTable = doc.getElementsByClass("siteTable").get(0);
		Content post = extractOriginalPost(siteTable);
		Element commentarea = doc.getElementsByClass("commentarea").get(0);
		Elements comments = commentarea.getElementsByClass("comment");
		for (int i = 0; i < comments.size(); i++) {
			Element comm = comments.get(i);
			Content reply = new Content();
			int skip = extractComment(comm, reply);
			i += skip;
			if (reply.getAuthorId() > 0)
				post.getReply().add(reply);

		}

		return post;
	}

	private int extractComment(Element parrent, Content content) {
		int skip = 0;
		Element entry = parrent.getElementsByClass("entry").get(0);
		Elements authors = entry.getElementsByClass("author");
		if (authors.isEmpty()) {
			content.setAuthorId(0);
			return skip;// deleted comm: skip
		}
		Element time = entry.getElementsByAttribute("datetime").get(0);
		String date = time.attr("datetime");
		String postText = entry.getElementsByClass("usertext-body").get(0).text();
		String author = authors.get(0).text();

		Long id = addUser(author);
		content.setAuthorId(id);

		try {
			XMLGregorianCalendar xmlDate = getXmlDate(date);
			content.setDate(xmlDate);
		} catch (Exception e) {
			LOG.error(e);
		}
		content.setText(postText);

		Element child = parrent.getElementsByClass("child").get(0);
		Elements comments = child.getElementsByClass("comment");
		skip = comments.size();
		for (int i = 0; i < comments.size(); i++) {
			Element comm = comments.get(i);
			Content reply = new Content();
			int iSkip = extractComment(comm, reply);
			i += iSkip;
			if (reply.getAuthorId() > 0)
				content.getReply().add(reply);
		}

		return skip;
	}

	private Content extractOriginalPost(Element siteTable) {
		Content content = new Content();
		Element entry = siteTable.getElementsByClass("entry").get(0);

		Element time = entry.getElementsByAttribute("datetime").get(0);
		String date = time.attr("datetime");
		String postText = "";
		Elements titleElements = entry.getElementsByClass("title");
		if (titleElements.size() > 0) {
			Element title = titleElements.get(0);
			postText = title.text() + "\n";
		}

		Elements body = entry.getElementsByClass("usertext-body");
		if (body.size() > 0) {
			Element userText = body.get(0);
			postText += userText.text();
		}

		String author = entry.getElementsByClass("author").get(0).text();

		Long id = addUser(author);
		content.setAuthorId(id);

		try {
			XMLGregorianCalendar xmlDate = getXmlDate(date);
			content.setDate(xmlDate);
		} catch (ParseException | DatatypeConfigurationException e) {
			LOG.error(e);
		}
		content.setText(postText);

		/*
		 * Element child = siteTable.getElementsByClass("child").get(0);
		 * Elements comments = child.getElementsByClass("comment"); for (int i =
		 * 0; i < comments.size(); i++) { Element comm = comments.get(i);
		 * Content reply = extractEntry(comm); content.getReply().add(reply); }
		 */

		return content;
	}

	private XMLGregorianCalendar getXmlDate(String date) throws ParseException, DatatypeConfigurationException {
		// int index = date.indexOf("+");

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(sdf.parse(date));
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}

	private static synchronized Long addUser(String author) {
		if (!users.containsKey(author)) {
			idCount++;// starts at 1
			users.put(author, new Long(idCount));

		}
		return users.get(author);

	}

	public static synchronized Map<String, Long> getUsers() {
		return users;
	}

}
