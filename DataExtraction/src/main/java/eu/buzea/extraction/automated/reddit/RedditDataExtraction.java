package eu.buzea.extraction.automated.reddit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.User;
import eu.buzea.util.XmlUtil;

public class RedditDataExtraction {

	public static final String REDDIT_URL_PATTERN = "http(s?)://(www.)?reddit.com/.+comments/.+(\\r)?";
	private int secondsBetweenPosts;
	private boolean parallelExtraction;
	private static final Logger LOG = Logger.getLogger(RedditDataExtraction.class);
	private static final String REDDIT_UNROLL_POST_BUTTON_CLASS = "button";
	private Set<String> urls;
	private Input xmlResult;

	private RedditDataExtraction(String runName) {
		urls = new HashSet<>();
		xmlResult = new Input();
		xmlResult.setRunName(runName);
		xmlResult.setOverwritePreviousDataSet(true);
		xmlResult.setUsers(new Input.Users());
		xmlResult.setPosts(new Input.Posts());
		secondsBetweenPosts = 5;
		parallelExtraction = true;
	}

	private void putUrlsInList(String urlList) {
		String[] urlsArr = urlList.split("\n");
		for (String s : urlsArr) {
			s.trim();
			if (!s.startsWith("--") && !s.isEmpty()) // -- is comment
				if (s.matches(REDDIT_URL_PATTERN))
					urls.add(s);
		}
	}

	public RedditDataExtraction(String runName, String urlList) {
		this(runName);
		putUrlsInList(urlList);
	}

	public RedditDataExtraction(String runName, File file) throws IOException {
		this(runName);
		InputStream is = new FileInputStream(file);
		String fileContent = IOUtils.toString(is).trim();
		putUrlsInList(fileContent);
	}

	public Input extractData() {
		LOG.info("STARTED NEW EXTRACTION\n\n");
		int failedPosts = 0;
		ArrayList<ExtractionThread> threadList = new ArrayList<>();
		for (String url : urls) {
			ExtractionThread thread = new ExtractionThread(url);
			thread.start();
			if (parallelExtraction) {
				threadList.add(thread);
				try {
					Thread.sleep(secondsBetweenPosts * 1000);
				} catch (InterruptedException e) {
					LOG.error(e);
				}
			} else {
				failedPosts = joinThread(failedPosts, thread);
			}
		}

		if (parallelExtraction) {
			for (ExtractionThread thread : threadList) {
				failedPosts = joinThread(failedPosts, thread);

			}
		}
		addUsers();
		sortPosts();

		LOG.info("Failed Posts = " + failedPosts);
		return xmlResult;
	}

	private int joinThread(int failedPosts, ExtractionThread thread) {
		try {

			thread.join();
			Content post = thread.getPost();
			if (post == null) {
				failedPosts++;
			} else {
				xmlResult.getPosts().getPost().add(post);
			}
		} catch (Exception e) {
			failedPosts++;
		}
		return failedPosts;
	}

	private void sortPosts() {
		xmlResult.getPosts().getPost().sort(new Comparator<Content>() {
			@Override
			public int compare(Content o1, Content o2) {
				XMLGregorianCalendar date1 = o1.getDate();
				XMLGregorianCalendar date2 = o2.getDate();
				return date1.compare(date2);
			}
		});
	}

	private void addUsers() {
		Map<String, Long> users = RedditHtmlProcesser.getUsers();
		for (String user : users.keySet()) {
			User userObject = new User();
			Long id = users.get(user);
			userObject.setId(id);
			userObject.setName(user);
			xmlResult.getUsers().getUser().add(userObject);
		}
	}

	public int getSecondsBetweenPosts() {
		return secondsBetweenPosts;
	}

	public void setSecondsBetweenPosts(int secondsBetweenPosts) {
		this.secondsBetweenPosts = secondsBetweenPosts;
	}

	public boolean isParallelExtraction() {
		return parallelExtraction;
	}

	public void setParallelExtraction(boolean parallelExtraction) {
		this.parallelExtraction = parallelExtraction;
	}

	private class ExtractionThread extends Thread {

		private String url;
		private Content post;

		public ExtractionThread(String url) {
			this.url = url;

		}

		@Override
		public void run() {
			try {
				LOG.info("Starting url : " + url);
				UnrolledHtmlReader unrolledHtmlReader = new UnrolledHtmlReader();
				LOG.info("Unrolled url: " + url);
				RedditHtmlProcesser redditHtmlProcesser = new RedditHtmlProcesser();
				String html = unrolledHtmlReader.readFullHtml(url, REDDIT_UNROLL_POST_BUTTON_CLASS);
				post = redditHtmlProcesser.processPost(html);
				LOG.info("Processed html: " + url);
			} catch (Exception e) {
				LOG.error(e);
				post = null;
			}

		}

		public Content getPost() {
			return post;
		}
	}

	@Deprecated
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Input must be <run-name> <path-to-input>");
			System.exit(1);
		}
		File file = new File(args[1]);
		try {
			RedditDataExtraction rde = new RedditDataExtraction(args[0], file);
			if (args.length >= 3) {
				try {
					int seconds = Integer.parseInt(args[2]);
					if (seconds >= 0) {
						rde.setSecondsBetweenPosts(seconds);
					} else {
						rde.setParallelExtraction(false);
					}
				} catch (NumberFormatException e) {
					System.err.println("Third argument is number of seconds");
					System.exit(3);
				}
			}
			Input input = rde.extractData();
			File outputFile = new File("extractedData.xml");
			XmlUtil.serialize(input, outputFile);
		} catch (IOException e) {
			System.err.println("Invalid input file");
			System.exit(2);
		} catch (JAXBException e) {
			System.err.println("Error creating output file");
			System.exit(4);
			e.printStackTrace();
		}

	}
}
