package eu.buzea.extraction.automated.reddit;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UnrolledHtmlReader {

	private static final int WAIT_LOADING_MILLIS = 500;
	private static final Logger LOG = Logger.getLogger(UnrolledHtmlReader.class);

	public String readFullHtml(String url, String unrollButtonClassName) throws Exception {

		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get(url);
		List<WebElement> buttons = driver.findElements(By.className(unrollButtonClassName));
		filterButtons(buttons);
		while (!buttons.isEmpty()) {
			WebElement link = buttons.remove(0);
			try {
				link.click();
				// allow loading
				WebDriverWait wait = new WebDriverWait(driver, 15);
				wait.until(ExpectedConditions.stalenessOf(link));
			} catch (TimeoutException e) {
				// just go on to the next link, don't even log
			} catch (WebDriverException e) {
				LOG.error(link.getAttribute("outerHTML"), e);
			}
			if (buttons.isEmpty()) {
				Thread.sleep(WAIT_LOADING_MILLIS);// allow loading time
				buttons = driver.findElements(By.className(unrollButtonClassName));
				filterButtons(buttons);
			}
		}

		String pageSource = driver.getPageSource();
		driver.close();
		return pageSource;

	}

	private void filterButtons(List<WebElement> buttons) {
		Iterator<WebElement> i = buttons.iterator();
		while (i.hasNext()) {
			WebElement e = i.next();
			if (!e.isDisplayed())
				i.remove();
			else if (!e.isEnabled())
				i.remove();
		}

	}

}
