package eu.buzea.extraction.semiautomated.facebook;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.User;

/**
 * Class used to extract data from
 * 
 * @author Vlad
 *
 */
public class FacebookHtmlDataExtraction {
	// eg Friday, December 4, 2015 at 12:56pm
	private static final String DATE_REGEX = ".*, .* [0-9]{1,2}, [0-9]{2,4} at .*";
	private File sourceHtml;
	private Input xmlResult;
	private Map<Long, String> users;

	public FacebookHtmlDataExtraction(String filePath) {
		sourceHtml = new File(filePath);
		users = new HashMap<>();
		xmlResult = new Input();
		xmlResult.setRunName("FacebookManualPost");
		xmlResult.setOverwritePreviousDataSet(true);
		xmlResult.setUsers(new Input.Users());
		xmlResult.setPosts(new Input.Posts());

	}

	public Input extractData() throws IOException, DatatypeConfigurationException, ParseException {
		Document doc = Jsoup.parse(sourceHtml, "UTF-8");
		Elements posts = doc.getElementsByClass("userContentWrapper");
		for (int i = 0; i < posts.size(); i++) {
			Element post = posts.get(i);
			Content originalPost = extractOrginalPostData(post);

			Elements comments = post.getElementsByClass("UFICommentContentBlock");
			for (int j = 0; j < comments.size(); j++) {
				Element comment = comments.get(j);
				Element commentAuthorTag = comment.getElementsByClass("UFICommentActorName").get(0);

				Element commentBodyTag = comment.getElementsByClass("UFICommentBody").get(0);
				String commentAuthor = commentAuthorTag.text();
				String commentText = commentBodyTag.text();
				String html = commentAuthorTag.attr("data-hovercard");
				String authorId = extractUserId(html);
				String commentDate = comment.getElementsByClass("livetimestamp").attr("title");
				XMLGregorianCalendar xmlDate = getCalendar(commentDate);
				if (xmlDate == null) {
					xmlDate = correctMissingDate(originalPost);
				}

				System.out.println(commentAuthor + "\t" + authorId);
				System.out.println(commentText + "\n\n");

				addUser(commentAuthor, authorId);
				Content reply = new Content();
				reply.setAuthorId(Long.parseLong(authorId));
				reply.setDate(xmlDate);
				reply.setText(commentText);
				originalPost.getReply().add(reply);

			}
		}
		return xmlResult;
	}

	private XMLGregorianCalendar correctMissingDate(Content originalPost) throws DatatypeConfigurationException {
		XMLGregorianCalendar xmlDate;
		int size = originalPost.getReply().size();
		if (size == 0) {
			xmlDate = (XMLGregorianCalendar) originalPost.getDate().clone();
		} else {
			xmlDate = (XMLGregorianCalendar) originalPost.getReply().get(size - 1).getDate().clone();
		}
		Duration duration = DatatypeFactory.newInstance().newDuration(1000);
		xmlDate.add(duration);
		return xmlDate;
	}

	private Content extractOrginalPostData(Element post) throws DatatypeConfigurationException, ParseException {
		Element postContentTag = post.getElementsByClass("userContent").get(0);
		String postContentText = postContentTag.text();
		System.out.println(postContentText);

		Element postAuthorTag = post.getElementsByClass("_5pbw").get(0);
		String postAuthor = postAuthorTag.text();
		System.out.println(postAuthor);

		String authorTagHtml = postAuthorTag.html();
		String authorId = extractUserId(authorTagHtml);
		System.out.println(authorId);

		// eg Friday, December 4, 2015 at 12:56pm
		String postDate = post.getElementsByClass("_5ptz").attr("title");

		addUser(postAuthor, authorId);
		Content xmlPost = new Content();
		Long id = Long.parseLong(authorId);
		xmlPost.setAuthorId(id);
		xmlPost.setText(postContentText);
		XMLGregorianCalendar postXmlDate = getCalendar(postDate);
		// post date should not be null

		xmlPost.setDate(postXmlDate);
		xmlResult.getPosts().getPost().add(xmlPost);
		return xmlPost;

	}

	private XMLGregorianCalendar getCalendar(String postDate) throws ParseException, DatatypeConfigurationException {
		// eg Friday, December 4, 2015 at 12:56pm
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		if (postDate == null)
			return null;
		if (!postDate.matches(DATE_REGEX))
			return null;

		GregorianCalendar c = new GregorianCalendar();
		int startIndex = postDate.indexOf(",");
		String necesaryData = postDate.substring(startIndex + 1).trim();
		String[] parts = necesaryData.split("at");
		parts[0] = parts[0].trim();
		parts[1] = parts[1].trim();

		DateFormat fmt = new SimpleDateFormat("MMMM dd, yyyy");
		Date d = fmt.parse(parts[0]);
		c.setTime(d);

		String[] time = parts[1].split(":");
		int hour = Integer.parseInt(time[0]);
		time[1] = time[1].substring(0, time[1].length() - 2);
		int minutes = Integer.parseInt(time[1]);

		c.set(Calendar.HOUR, hour);
		c.set(Calendar.MINUTE, minutes);
		if (time[1].endsWith("pm")) {
			c.set(Calendar.AM_PM, Calendar.PM);
		} else {
			c.set(Calendar.AM_PM, Calendar.AM);
		}

		return datatypeFactory.newXMLGregorianCalendar(c);
	}

	private void addUser(String postAuthor, String authorId) {
		Long id = Long.parseLong(authorId);
		if (users.containsKey(id)) {
			if (!users.get(id).equals(postAuthor))
				throw new RuntimeException("Inconsistend Social Network ID");
		} else {
			users.put(id, postAuthor);
			User userObject = new User();
			userObject.setId(id);
			userObject.setName(postAuthor);
			xmlResult.getUsers().getUser().add(userObject);
		}
	}

	private String extractUserId(String authorTagHtml) {
		int startIndex = authorTagHtml.indexOf("id=") + 3;
		int endIndex = authorTagHtml.indexOf("&", startIndex);
		String authorId = authorTagHtml.substring(startIndex, endIndex);
		return authorId;
	}

}
