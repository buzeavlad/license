package com.cb.extraction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class for running the data extraction module
 * 
 * @author Ciprian Budusan
 *
 */
@SpringBootApplication
public class Application {

	/* -Xms512M -Xmx1524M set as VM arguments in Run Configurations to increase heap size */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}