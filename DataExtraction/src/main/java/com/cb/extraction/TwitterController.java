package com.cb.extraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.social.OperationNotPermittedException;
import org.springframework.social.RateLimitExceededException;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.User;
import eu.buzea.util.XmlUtil;

/**
 * Twitter data extraction
 * 
 * @author Ciprian Budusan
 *
 */
@Controller
@RequestMapping("/twitter")
public class TwitterController {

	private Input xmlResult;
	// 2016-03-07T13:37:58+00:00
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private Map<String, Long> users;
	private long idCount;
	private long tweetIdMin;
	private long tweetIdMax;
	// number of extracted posts, comments and replies
	private Integer limit;
	private static final String OUTPUT_FILE = "./src/test/resources/TwitterOutput.xml";

	private Twitter twitter;
	private ConnectionRepository connectionRepository;

	@Inject
	public TwitterController(Twitter twitter, ConnectionRepository connectionRepository) {
		this.twitter = twitter;
		this.connectionRepository = connectionRepository;

		xmlResult = new Input();
		xmlResult.setRunName("FacebookTestData");
		xmlResult.setOverwritePreviousDataSet(true);
		xmlResult.setUsers(new Input.Users());
		xmlResult.setPosts(new Input.Posts());

		users = new HashMap<>();
		idCount = 1;
		tweetIdMin = Long.MAX_VALUE;
		tweetIdMax = -1;
		limit = 100;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String extractTwitter(Model model) {
		if (connectionRepository.findPrimaryConnection(Twitter.class) == null) {
			return "redirect:/connect/twitter";
		}

		model.addAttribute(twitter.userOperations().getUserProfile());

		String query = "#EURO2016";
		SearchResults results = null;
		try {
			results = twitter.searchOperations().search(query, limit);
		} catch (RateLimitExceededException e) {
			e.printStackTrace();
			serialize();
			return "twitter";
		}

		xmlResult.setDescription(query);

		System.out.println("Tweets Extracted!");

		while (!results.isLastPage()) {
			List<Tweet> tweets = results.getTweets();
			if (tweets.isEmpty()) {
				serialize();
				return "twitter";
			}
			for (Tweet tweet : tweets) {
				// keep track of the lowest ID received
				if (tweet.getId() < tweetIdMin)
					tweetIdMin = tweet.getId();
				if (tweet.getId() > tweetIdMax)
					tweetIdMax = tweet.getId();
				Content xmlTweet = null;
				System.out.println(tweet.getText());
				/*
				 * if tweet is a reply to another tweet then get the parent
				 * tweet
				 */
				// the screen name of the original Tweet’s author
				if (!tweet.getInReplyToScreenName().equals("null")) {
					// the original Tweet’s ID
					Long id = tweet.getInReplyToStatusId();
					if (id != null) {
						System.out.println("REPLY");
						Tweet parentTweet = null;
						try {
							parentTweet = twitter.timelineOperations().getStatus(id);
							/*
							 * HTTP 403 — thrown when a Tweet cannot be viewed
							 * by the authenticating user, usually due to the
							 * tweet’s author having protected their tweets.
							 */
						} catch (ResourceNotFoundException | OperationNotPermittedException e) {
							e.printStackTrace();
							continue;
						}
						xmlTweet = setContent(String.valueOf(parentTweet.getFromUserId()),
								parentTweet.getFromUser(), parentTweet.getCreatedAt(),
								parentTweet.getText());
						Content xmlReply = setContent(String.valueOf(tweet.getFromUserId()),
								tweet.getFromUser(), tweet.getCreatedAt(), tweet.getText());

						// if the tweet is already in the xml then add the reply
						// to it
						// else create a new xml element
						int index = indexOfTweet(xmlTweet);
						if (index != -1)
							xmlResult.getPosts().getPost().get(index).getReply().add(xmlReply);
						else {
							xmlTweet.getReply().add(xmlReply);
							xmlResult.getPosts().getPost().add(xmlTweet);
						}

					} else {
						xmlTweet = setContent(String.valueOf(tweet.getFromUserId()),
								tweet.getFromUser(), tweet.getCreatedAt(), tweet.getText());
						xmlResult.getPosts().getPost().add(xmlTweet);
					}
				}
				/*
				 * if tweet is a retweet then add it as a reply
				 */
				else if (tweet.isRetweet()) {
					System.out.println("RETWEET");
					Tweet parentTweet = tweet.getRetweetedStatus();
					xmlTweet = setContent(String.valueOf(parentTweet.getFromUserId()),
							parentTweet.getFromUser(), parentTweet.getCreatedAt(),
							parentTweet.getText());
					Content xmlRetweet = setContent(String.valueOf(tweet.getFromUserId()),
							tweet.getFromUser(), tweet.getCreatedAt(), tweet.getText());

					// if the tweet is already in the xml then add the reply to
					// it
					// else create a new xml element
					int index = indexOfTweet(xmlTweet);
					if (index != -1)
						xmlResult.getPosts().getPost().get(index).getReply().add(xmlRetweet);
					else {
						xmlTweet.getReply().add(xmlRetweet);
						xmlResult.getPosts().getPost().add(xmlTweet);
					}
				}
				/*
				 * if normal tweet then check if it has any replies
				 */
				else {
					System.out.println("NORMAL TWEET");
					xmlTweet = setContent(String.valueOf(tweet.getFromUserId()),
							tweet.getFromUser(), tweet.getCreatedAt(), tweet.getText());

					// getReplies(tweet, xmlTweet);
					// getRetweets(tweet, xmlTweet);

					// if the tweet is already in the xml then add the reply to
					// it
					// else create a new xml element
					int index = indexOfTweet(xmlTweet);
					if (index == -1) {
						xmlResult.getPosts().getPost().add(xmlTweet);
					}
				}
			}
			try {
				results = twitter.searchOperations().search(query, limit, 0, tweetIdMin - 1);
			} catch (RateLimitExceededException e) {
				e.printStackTrace();
				serialize();
				return "twitter";
			}
		}

		serialize();

		System.out.println("File saved!");

		// redirects to twitter.html
		return "twitter";
	}

	public void getRetweets(Tweet tweet, Content xmlTweet) {
		List<Tweet> retweets = twitter.timelineOperations().getRetweets(tweet.getId());
		for (Tweet retweet : retweets) {
			Content xmlRetweet = setContent(String.valueOf(retweet.getFromUserId()),
					retweet.getFromUser(), retweet.getCreatedAt(), retweet.getText());
			xmlTweet.getReply().add(xmlRetweet);
		}
	}

	public void getReplies(Tweet tweet, Content xmlTweet) {
		SearchResults replyResults = null;
		try {
			replyResults = twitter.searchOperations().search("to:" + tweet.getFromUser(), limit,
					tweet.getId(), 0);
		} catch (RateLimitExceededException e) {
			e.printStackTrace();
			serialize();
			return;
		}
		List<Tweet> replies = replyResults.getTweets();
		for (Tweet reply : replies) {
			Long parentId = tweet.getInReplyToStatusId();
			if (parentId != null) {
				if (parentId == tweet.getId()) {
					System.out.println("REPLY FOUND");
					Content xmlReply = setContent(String.valueOf(reply.getFromUserId()),
							reply.getFromUser(), reply.getCreatedAt(), reply.getText());
					xmlTweet.getReply().add(xmlReply);
				}
			}
		}
	}

	private int indexOfTweet(Content xmlTweet) {
		for (int i = 0; i < xmlResult.getPosts().getPost().size(); i++) {
			Content tweet = xmlResult.getPosts().getPost().get(i);
			if (xmlTweet.getAuthorId() == tweet.getAuthorId()
					&& xmlTweet.getText().equals(tweet.getText())
					&& xmlTweet.getDate().equals(tweet.getDate()))
				return i;
		}
		return -1;
	}

	private void serialize() {
		try {
			XmlUtil.serialize(xmlResult, new File(OUTPUT_FILE));
		} catch (FileNotFoundException | JAXBException e) {
			e.printStackTrace();
		}
	}

	private XMLGregorianCalendar getXmlDate(Date date)
			throws ParseException, DatatypeConfigurationException {
		GregorianCalendar gc = new GregorianCalendar();
		String formattedDate = sdf.format(date);
		gc.setTime(sdf.parse(formattedDate));
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}

	private Long addUser(String authorID, String author) {
		if (!users.containsKey(authorID)) {
			User userObject = new User();
			userObject.setId(idCount);
			userObject.setName(author);
			xmlResult.getUsers().getUser().add(userObject);

			users.put(authorID, new Long(idCount));
			idCount++;
		}
		return users.get(authorID);
	}

	private Content setContent(String authorID, String author, Date date, String postText) {
		Content content = new Content();

		Long id = addUser(authorID, author);
		content.setAuthorId(id);

		try {
			XMLGregorianCalendar xmlDate = getXmlDate(date);
			content.setDate(xmlDate);
		} catch (ParseException | DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		content.setText(postText);

		return content;
	}

}
