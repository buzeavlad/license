package com.cb.extraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Comment;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.ObjectFactory;
import eu.buzea.inputschema.User;
import eu.buzea.util.XmlUtil;

/**
 * Facebook data extraction
 * 
 * @author Ciprian Budusan
 *
 */
@Controller
@RequestMapping("/facebook")
public class FacebookController {

	/*
	 * copy here the user access token received from graph api explorer
	 * https://developers.facebook.com/tools/explorer/ change the version of the
	 * GET request to GraphAPI v2.3 and select the user_groups permission
	 */
	private static final String USER_ACCESS_TOKEN = "EAACEdEose0cBANOeSDGq1189sybJGQgq08JMkMRXd1y6kGwZA4Li3cydAHtIZCSNG2ZAWThzZBVw0zkAojAB4PvtZBinXzOHZCeZAoc4fB3oom3oGCHPGHeAJF03ZCZAworevPfB1PRxZBulTfRZBNY3OZBZCKhCnLegf7nkoOzbGFxHQwgZDZD";
	private static final String OUTPUT_FILE = "./src/test/resources/FacebookGraphAPIOutput.xml";
	// set this to true if the group is private
	private static final boolean CLOSED_GROUP = false;
	private static final String GROUP_ID = "1634674436787749"; // Social Network Group
	//private static final String GROUP_ID = "1525027737730062"; // Sensible Politics Debate PRIVATE
	// number of items to limit the list to
	private static final Integer LIMIT = 25;
	private static final String FROM_DATE = "04.01.2015";
	private static final String TO_DATE = "06.12.2016";

	// 2016-03-07T13:37:58+00:00
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private Map<String, Long> users;
	private long idCount;

	private Facebook facebook;
	private ConnectionRepository connectionRepository;
	private ObjectFactory objectFactory;
	private Input xmlResult;

	@Inject
	public FacebookController(Facebook facebook, ConnectionRepository connectionRepository) {
		if (CLOSED_GROUP) {
			this.facebook = new FacebookTemplate(USER_ACCESS_TOKEN);
			((FacebookTemplate) this.facebook).setApiVersion("2.3");
		} else
			this.facebook = facebook;
		this.connectionRepository = connectionRepository;

		objectFactory = new ObjectFactory();
		xmlResult = objectFactory.createInput();
		xmlResult.setRunName("FacebookTestData");
		xmlResult.setOverwritePreviousDataSet(true);
		xmlResult.setUsers(new Input.Users());
		xmlResult.setPosts(new Input.Posts());

		users = new HashMap<>();
		idCount = 1;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String extractFacebook(Model model) {
		if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
			return "redirect:/connect/facebook";
		}
		model.addAttribute("facebookProfile", facebook.userOperations().getUserProfile());
		// set description as the name of the group
		String groupName = facebook.groupOperations().getGroup(GROUP_ID).getName();
		xmlResult.setDescription(groupName);

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		Date fDate = null;
		Date tDate = null;
		try {
			fDate = dateFormat.parse(FROM_DATE);
			tDate = dateFormat.parse(TO_DATE);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		long since = fDate.getTime() / 1000;
		long until = tDate.getTime() / 1000;

		// extract posts
		PagedList<Post> feed = facebook.feedOperations().getFeed(GROUP_ID,
				new PagingParameters(LIMIT, 0, since, until));
		System.out.println("Posts Extracted!");

		while (feed.size() > 0) {
			for (Post post : feed) {
				if (post.getMessage() != null) {
					Content xmlPost = setContent(post.getFrom().getId(), post.getFrom().getName(),
							post.getCreatedTime(), post.getMessage());

					System.out.println("-----------------------------------------------------------------------------");
					System.out.println("POST: " + post.getFrom().getName() + post.getCreatedTime() + post.getMessage());
					System.out.println("-----------------------------------------------------------------------------");
					
					// extract likes for the post
					PagedList<Reference> postLikes = facebook.likeOperations().getLikes(post.getId(),
							new PagingParameters(LIMIT, 0, null, null));
					while (postLikes.size() > 0) {
						for (Reference like : postLikes) {
							// like.getName() is not working, returning null
							// node can be a page so cannot use
							// facebook.userOperations()
							String username = facebook
									.fetchObject(like.getId(), org.springframework.social.facebook.api.User.class)
									.getName();
							Content xmlLike = setContent(like.getId(), username, post.getCreatedTime(), "POSITIVE");

							xmlPost.getReply().add(xmlLike);
						}

						PagingParameters p = postLikes.getNextPage();
						if (p != null)
							postLikes = facebook.likeOperations().getLikes(post.getId(), p);
						else // it has no next page so we set size to 0 to exit
								// loop
							postLikes.clear();
					}

					// extract comments
					PagedList<Comment> comments = facebook.commentOperations().getComments(post.getId(),
							new PagingParameters(LIMIT, 0, null, null));

					while (comments.size() > 0) {
						for (Comment comment : comments) {
							if (comment.getMessage() != null && !comment.getMessage().equals("")) {
								Content xmlComment = setContent(comment.getFrom().getId(), comment.getFrom().getName(),
										comment.getCreatedTime(), comment.getMessage());
								System.out.println("COMMENT: " + comment.getFrom().getName() + comment.getMessage());
								// extract likes for comment
								PagedList<Reference> commentLikes = facebook.likeOperations().getLikes(comment.getId(),
										new PagingParameters(LIMIT, 0, null, null));

								while (commentLikes.size() > 0) {
									for (Reference like : commentLikes) {
										String username = facebook.fetchObject(like.getId(),
												org.springframework.social.facebook.api.User.class).getName();
										Content xmlLike = setContent(like.getId(), username, comment.getCreatedTime(),
												"POSITIVE");

										xmlComment.getReply().add(xmlLike);
									}

									PagingParameters p = commentLikes.getNextPage();
									if (p != null)
										commentLikes = facebook.likeOperations().getLikes(comment.getId(), p);
									else // it has no next page so we set size
											// to 0 to exit loop
										commentLikes.clear();
								}

								// get number of replies
								Comment comm = facebook.commentOperations().getComment(comment.getId());
								Integer commentCount = comm.getCommentCount();
								int repliesNr = 0;
								if (commentCount != null)
									repliesNr = commentCount.intValue();

								// every comment has a list of replies
								if (repliesNr > 0) { // comment has replies
									PagedList<Comment> replies = facebook.commentOperations()
											.getComments(comment.getId(), new PagingParameters(LIMIT, 0, null, null));

									while (replies.size() > 0) {
										for (Comment reply : replies) {
											Content xmlReply = setContent(reply.getFrom().getId(),
													reply.getFrom().getName(), reply.getCreatedTime(),
													reply.getMessage());

											// extract likes for replies
											PagedList<Reference> replyLikes = facebook.likeOperations().getLikes(
													reply.getId(), new PagingParameters(LIMIT, 0, null, null));

											while (replyLikes.size() > 0) {
												for (Reference like : replyLikes) {
													String username = facebook
															.fetchObject(like.getId(),
																	org.springframework.social.facebook.api.User.class)
															.getName();
													Content xmlLike = setContent(like.getId(), username,
															reply.getCreatedTime(), "POSITIVE");

													xmlReply.getReply().add(xmlLike);
												}

												PagingParameters p = replyLikes.getNextPage();
												if (p != null)
													replyLikes = facebook.likeOperations().getLikes(reply.getId(), p);
												else // it has no next page so
														// we set size to 0 to
														// exit loop
													replyLikes.clear();
											}
											xmlComment.getReply().add(xmlReply);
										}

										PagingParameters p = replies.getNextPage();
										if (p != null)
											replies = facebook.commentOperations().getComments(comment.getId(), p);
										else // it has no next page so we set
												// size to 0 to exit loop
											replies.clear();
									}
								}
								xmlPost.getReply().add(xmlComment);
							}
						}

						PagingParameters p = comments.getNextPage();
						if (p != null)
							comments = facebook.commentOperations().getComments(post.getId(), p);
						else // it has no next page so we set size to 0 to exit
								// loop
							comments.clear();
					}
					System.out.println("COMMENTS EXTRACTED!");
					xmlResult.getPosts().getPost().add(xmlPost);
				}
			}

			PagingParameters p = feed.getNextPage();
			if (p != null)
				feed = facebook.feedOperations().getFeed(GROUP_ID, p);
			else
				feed.clear();
		}

		try {
			XmlUtil.serialize(xmlResult, new File(OUTPUT_FILE));
		} catch (FileNotFoundException | JAXBException e) {
			e.printStackTrace();
		}

		System.out.println("File saved!");

		// redirects to facebook.html
		return "facebook";
	}

	private XMLGregorianCalendar getXmlDate(Date date) throws ParseException, DatatypeConfigurationException {
		GregorianCalendar gc = new GregorianCalendar();
		String formattedDate = sdf.format(date);
		gc.setTime(sdf.parse(formattedDate));
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}

	private Long addUser(String authorID, String author) {
		if (!users.containsKey(authorID)) {
			User userObject = new User();
			userObject.setId(idCount);
			userObject.setName(author);
			xmlResult.getUsers().getUser().add(userObject);

			users.put(authorID, new Long(idCount));
			idCount++;
		}
		return users.get(authorID);
	}

	private Content setContent(String authorID, String author, Date date, String postText) {
		Content content = new Content();

		Long id = addUser(authorID, author);
		content.setAuthorId(id);

		try {
			XMLGregorianCalendar xmlDate = getXmlDate(date);
			content.setDate(xmlDate);
		} catch (ParseException | DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		content.setText(postText);

		return content;
	}

}