import graphmodel.NetworkGraph;

/**
 * Created by Florin on 15-Feb-16.
 */
public class Main {
    public static void main(String[] args) {
        /*System.out.println("Hello world!");
        NetworkGraph graph = new NetworkGraph();*/

        Solution solution = new Solution();
        System.out.println(solution.reverseBits(1));
    }

    public static class Solution {
        // you need treat n as an unsigned value
        public long reverseBits(int n) {
            StringBuilder bits = new StringBuilder(Integer.toBinaryString(n));
            bits.reverse();
            int remaining = 32 - bits.length();
            for(int i = 0; i < remaining; i++){
                bits.append('0');
            }

            return Long.parseLong(bits.toString(), 2);
        }
    }
}
