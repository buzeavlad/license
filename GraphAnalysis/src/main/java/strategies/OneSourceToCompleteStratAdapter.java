package strategies;

import graphmodel.Graph;
import graphmodel.GraphNode;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by Florin on 29-Feb-16.
 */
@RequiredArgsConstructor
public class OneSourceToCompleteStratAdapter<T extends GraphNode> implements CompleteShortestPathStrategy<T> {
    private final OneSourceShortestPathStrat<T> underLyingSourceStrat;
    private final Class<T> clazz;

    private final Map<T, Integer> indexes = new HashMap<>();

    private T[][] parrentMatrix;
    private int[][] distanceMatrix;
    private int nodesSoFar = 0;

    @Override
    public void computeShortestPaths(Graph<T> graph) {
        initStructures(graph);
        runAlgorithm(graph);
    }

    private void initStructures(Graph<T> graph) {
        List<T> nodes = graph.getNodes();

        distanceMatrix = new int[nodes.size()][nodes.size()];
        parrentMatrix = (T[][]) Array.newInstance(clazz, graph.getSize(), graph.getSize());

        for (int i = 0; i < distanceMatrix.length; i++) {
            for (int j = 0; j < distanceMatrix.length; j++) {
                distanceMatrix[i][j] = MAX_VALUE;
                parrentMatrix[i][j] = null;
            }
        }

        for (T node : nodes) {
            distanceMatrix[nodesSoFar][nodesSoFar] = 0;
            parrentMatrix[nodesSoFar][nodesSoFar] = node;
            indexes.put(node, nodesSoFar++);
        }
    }

    private void runAlgorithm(Graph<T> graph) {
        for (T nodeI : graph.getNodes()) {
            underLyingSourceStrat.setSource(nodeI);
            underLyingSourceStrat.computeDistances(graph);
            for (T nodeJ : graph.getNodes()) {
                distanceMatrix[indexes.get(nodeI)][indexes.get(nodeJ)] =
                        underLyingSourceStrat.getDistance(nodeJ);
                parrentMatrix[indexes.get(nodeI)][indexes.get(nodeJ)] =
                        underLyingSourceStrat.getParrent(nodeJ);
            }
        }
    }

    @Override
    public int getDistance(T source, T destination) {
        return distanceMatrix[indexes.get(source)][indexes.get(destination)];
    }

    @Override
    public T getParrent(T source, T destination) {
        return parrentMatrix[indexes.get(source)][indexes.get(destination)];
    }
}
