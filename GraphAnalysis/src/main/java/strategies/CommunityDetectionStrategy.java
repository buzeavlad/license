package strategies;

import graphmodel.Graph;
import graphmodel.GraphNode;

import java.util.List;
import java.util.Set;

/**
 * Created by Florin on 15-Feb-16.
 */
public interface CommunityDetectionStrategy {
    <T extends GraphNode> List<Set<T>> getCommunities(Graph<T> graph);
}
