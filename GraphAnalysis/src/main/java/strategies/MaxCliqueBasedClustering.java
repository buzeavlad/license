package strategies;

import gnu.trove.map.TObjectIntMap;
import graphmodel.Graph;
import graphmodel.GraphEdge;
import graphmodel.GraphNode;
import graphmodel.WeightedGraphNode;
import javafx.util.Pair;
import lombok.RequiredArgsConstructor;

import java.util.*;

@RequiredArgsConstructor
public class MaxCliqueBasedClustering implements CommunityDetectionStrategy, LeaderElectionStrategy {
    private static final int WEIGHTED_GRAPH_CONNECTIVITY_THRESHOLD = 50;

    private Map<GraphNode, Integer> indexes = new HashMap<>();
    private int[][] connectivityMatrix;
    private final CentralityStrategy centralityStrat;
    private final CompleteShortestPathStrategy shortestPathStrat;
    private boolean[] hasClique;

    @Override
    public <T extends GraphNode> List<Set<T>> getCommunities(Graph<T> graph) {
        List<Set<T>> result = new ArrayList<>();

        connectivityMatrix = computeConnectivityMatrix(graph); // step  1
        int N = graph.getSize();
        hasClique = new boolean[N];
        int nodesSoFar = 0;

        Map<T, Set<T>> clusters = new HashMap<>();

        findClusters(graph, N, connectivityMatrix, clusters); // step 2

        //step3 clique formation

        for (T node : graph.getNodes()) {
            if (!hasClique[indexes.get(node)]) {
                Set<T> newClique = greedyFindClique(node, graph, connectivityMatrix);

                for (T cliquedNode : newClique) {
                    hasClique[indexes.get(cliquedNode)] = true;
                }
                result.add(newClique);
            }
        }

        return result;
    }

    public <T extends GraphNode> Map<Set<T> , T> getLeaders(List<Set<T>> cliques, Graph<T> graph) {
        Map<Set<T>, T> leaderMap = new HashMap<>();

        for(Set<T> clique : cliques){
            TObjectIntMap<T> centralities = centralityStrat.getCentralities(clique, graph);
            T leader = null;
            int maxCentrality = -1;
            for(T node : clique){
                if(centralities.get(node) > maxCentrality){
                    maxCentrality = centralities.get(node);
                    leader = node;
                }
            }
            leaderMap.put(clique, leader);
        }

        return leaderMap;
    }

    private <T extends GraphNode> Set<T> greedyFindClique(final T node, Graph<T> graph, int[][] connectivityMatrix) {
        Set<T> result = new HashSet<>();
        result.add(node);
        int maxConnections = 1;

        for (int connection : connectivityMatrix[indexes.get(node)]) {
            maxConnections++;
        }

        if (maxConnections > 1) {
            final int[] coomonNeighbours = new int[graph.getNodes().size()];

            for (T graphNode : graph.getNodes()) {
                if (!node.equals(graphNode)) {
                    coomonNeighbours[indexes.get(graphNode)] = getNrCommonNeighbours(graph, node, graphNode, connectivityMatrix);
                }
            }

            Collections.sort(graph.getNodes(), new Comparator<T>() {
                @Override
                public int compare(T node1, T node2) {
                    if (node1.equals(node) || node2.equals(node)) return Integer.MIN_VALUE;
                    else {
                        return coomonNeighbours[indexes.get(node2)] - coomonNeighbours[indexes.get(node1)];
                    }
                }
            });

            int i = 0;

            while (result.size() < maxConnections) {
                result.add(graph.getNodes().get(i));
                i++;
            }
        }

        return result;
    }

    private <T extends GraphNode> int getNrCommonNeighbours(Graph<T> graph, T node1, T node2, int[][] connectivityMatrix) {
        int commonNeighbours = 0;

        for (T node : graph.getNodes()) {
            if (connectivityMatrix[indexes.get(node1)][indexes.get(node)] > 0 &&
                    connectivityMatrix[indexes.get(node2)][indexes.get(node)] > 0) {
                commonNeighbours++;
            }
        }

        return commonNeighbours;
    }

    private <T extends GraphNode> void findClusters(Graph<T> graph, int n, int[][] connectivityMatrix, Map<T, Set<T>> clusters) {

        int[] connectivities = new int[n];

        for (int i = 0; i < n; i++) {
            connectivities[i] = Arrays.stream(this.connectivityMatrix[i]).sum();
        }
        int nodesJoinedClusters = 0;

        while (nodesJoinedClusters < n) {
            boolean maxNeigh = true;

            for (T currentNode : graph.getNodes()) {
                for (GraphEdge<T> edge : currentNode.getEdges()) {
                    T outNode = getOutNode(currentNode, edge);

                    if (connectivities[indexes.get(outNode)] > connectivities[indexes.get(currentNode)]) {
                        maxNeigh = false;
                        break;
                    }
                }

                if (maxNeigh) {
                    Set<T> newCluster = new HashSet<>();
                    clusters.put(currentNode, newCluster);

                    for (GraphEdge<T> edge : currentNode.getEdges()) {
                        T outNode = getOutNode(currentNode, edge);

                        if (isNeighbour(this.connectivityMatrix[indexes.get(currentNode)][indexes.get(outNode)], currentNode)) {
                            newCluster.add(outNode);
                            nodesJoinedClusters++;
                        }
                    }
                }
            }
        }
    }

    private <T extends GraphNode> boolean isNeighbour(int i, T currentNode) {
        if (currentNode instanceof WeightedGraphNode) {
            return i >= WEIGHTED_GRAPH_CONNECTIVITY_THRESHOLD;
        }

        return i > 0;
    }

    private <T extends GraphNode> int[][] computeConnectivityMatrix(Graph<T> graph) {
        shortestPathStrat.computeShortestPaths(graph);
        int nodesSoFar = 0;
        for (T t : graph.getNodes()) {
            indexes.put(t, nodesSoFar);
            nodesSoFar++;
        }

        int[][] centralities = new int[graph.getNodes().size()][graph.getNodes().size()];
        for (T tI : graph.getNodes()) {
            for (T tJ : graph.getNodes()) {
                centralities[indexes.get(tI)][indexes.get(tJ)] = shortestPathStrat.getDistance(tI, tJ);
            }
        }

        return centralities;
    }

    private <T extends GraphNode> T getOutNode(T source, GraphEdge<T> edge) {
        Pair<T, T> nodes = edge.getNodesInvolved();
        if (nodes.getKey().equals(source)) {
            return nodes.getValue();
        } else {
            return nodes.getKey();
        }
    }

    private <T extends GraphNode> T getSourceNode(T source, GraphEdge<T> edge) {
        Pair<T, T> nodes = edge.getNodesInvolved();
        if (nodes.getKey().equals(source)) {
            return nodes.getKey();
        } else {
            return nodes.getValue();
        }
    }
}
