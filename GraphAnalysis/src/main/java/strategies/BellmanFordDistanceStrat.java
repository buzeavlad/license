package strategies;

import graphmodel.Graph;
import graphmodel.GraphEdge;
import graphmodel.GraphNode;
import graphmodel.WeightedGraphEdge;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Florin on 28-Feb-16.
 */
@RequiredArgsConstructor
public class BellmanFordDistanceStrat<T extends GraphNode>
        implements OneSourceShortestPathStrat<T> {
    private volatile boolean sourceSet = false;
    private volatile boolean algoFinished = false;

    private final Object oneRunLock = new Object();

    private final Class<T> clazz;
    private int nodesSoFar = 0;
    private T source;
    private boolean negativeLoopFlag = false;
    private int[] distances;
    private T[] parrents;
    private final Map<T, Integer> indexes = new HashMap<>();

    @Override
    public void computeDistances(Graph<T> graph) {
        if (!sourceSet) {
            throw new UnsupportedOperationException("You need to add a source before starting the algorithm");
        }

        initStructures(graph);
        runAlgorithm(graph);
        algoFinished = true;
    }

    private void initStructures(Graph<T> graph) {
        List<T> nodes = graph.getNodes();
        distances = new int[nodes.size()];
        parrents = (T[]) Array.newInstance(clazz, nodes.size());

        for (int i = 0; i < distances.length; i++) {
            distances[i] = Integer.MAX_VALUE;
            parrents[i] = null;
        }

        for (T node : nodes) {
            indexes.put(node, nodesSoFar++);
        }

        distances[indexes.get(source)] = 0;
        parrents[indexes.get(source)] = source;
    }

    private void runAlgorithm(Graph<T> graph) {
        List<GraphEdge<T>> edges = graph.getEdges();
        int n = graph.getNodes().size();
        for(int i = 0; i < n; i++) {
            for (GraphEdge<T> edge : edges) {
                if (distances[indexes.get(edge.getNodesInvolved().getKey())] + getEdgeCost(edge)
                        < distances[indexes.get(edge.getNodesInvolved().getValue())]) {

                    distances[indexes.get(edge.getNodesInvolved().getValue())] =
                            distances[indexes.get(edge.getNodesInvolved().getKey())] + getEdgeCost(edge);
                    parrents[indexes.get(edge.getNodesInvolved().getValue())] = edge.getNodesInvolved().getKey();
                }
            }
        }

        for (GraphEdge<T> edge : edges) {
            if (distances[indexes.get(edge.getNodesInvolved().getKey())] + getEdgeCost(edge)
                    < distances[indexes.get(edge.getNodesInvolved().getValue())]) {

                negativeLoopFlag = true;
            }
        }
    }

    private int getEdgeCost(GraphEdge<T> edge) {
        if (edge instanceof WeightedGraphEdge) {
            return ((WeightedGraphEdge<T>) edge).getWeight();
        } else {
            return 1;
        }
    }

    @Override
    public void setSource(T source) {
        this.source = source;
        algoFinished = false;
        sourceSet = true;
    }

    @Override
    public T getSource() {
        return source;
    }

    @Override
    public int getDistance(T destination) {
        if (!algoFinished) {
            return distances[indexes.get(destination)];
        } else {
            throw new UnsupportedOperationException("The algorithm did not finish his execution yet!");
        }
    }

    @Override
    public T getParrent(T node) {
        if (!algoFinished) {
            return parrents[indexes.get(node)];
        } else {
            throw new UnsupportedOperationException("The algorithm did not finish his execution yet!");
        }
    }

    public boolean checkNegativeLoop() {
        return negativeLoopFlag;
    }
}
