package strategies;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import graphmodel.Graph;
import graphmodel.GraphNode;
import lombok.RequiredArgsConstructor;

import java.util.Set;


@RequiredArgsConstructor
public class BetweennessCentralityStrat implements CentralityStrategy {
    private final CompleteShortestPathStrategy shortestPathStrategy;
    private boolean algoStarted = false;

    @Override
    public <T extends GraphNode> TObjectIntMap<T> getCentralities(Graph<T> graph) {
        if(!algoStarted) {
            shortestPathStrategy.computeShortestPaths(graph);
            TObjectIntMap<T> result = new TObjectIntHashMap<>();

            for(T node : graph.getNodes()){
                result.put(node, 0);
            }

            for(T nodeI : graph.getNodes()){
                for(T nodeJ : graph.getNodes()){
                    T parrent = (T) shortestPathStrategy.getParrent(nodeI, nodeJ);
                    if(!parrent.equals(nodeI)){
                        int currentBetweenness = result.get(parrent);
                        result.put(parrent, currentBetweenness + 1);
                    }
                }
            }

            algoStarted = false;
            return result;
        }else {
            throw new UnsupportedOperationException("The algorithm did not finish his execution yet!");
        }
    }

    @Override
    public <T extends GraphNode> TObjectIntMap<T> getCentralities(Set<T> subGraph, Graph<T> graph) {
        if(!algoStarted) {
            shortestPathStrategy.computeShortestPaths(graph);
            TObjectIntMap<T> result = new TObjectIntHashMap<>();

            for(T node : subGraph){
                result.put(node, 0);
            }

            for(T nodeI : subGraph){
                for(T nodeJ : subGraph){
                    T parrent = (T) shortestPathStrategy.getParrent(nodeI, nodeJ);
                    if(!parrent.equals(nodeI)){
                        int currentBetweenness = result.get(parrent);
                        result.put(parrent, currentBetweenness + 1);
                    }
                }
            }

            algoStarted = false;
            return result;
        }else {
            throw new UnsupportedOperationException("The algorithm did not finish his execution yet!");
        }
    }
}
