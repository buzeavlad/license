package strategies;

import graphmodel.Graph;
import graphmodel.GraphNode;

/**
 * Created by Florin on 29-Feb-16.
 */
public interface CompleteShortestPathStrategy<T extends GraphNode> {

    void computeShortestPaths(Graph<T> t);

    int getDistance(T source, T destination);

    T getParrent(T source, T destination);
}