package strategies;

import gnu.trove.map.TObjectIntMap;
import graphmodel.Graph;
import graphmodel.GraphNode;

import java.util.Set;

/**
 * Created by Florin on 15-Feb-16.
 */
public interface CentralityStrategy {
     <T extends GraphNode> TObjectIntMap<T> getCentralities(Graph<T> graph);

     <T extends GraphNode> TObjectIntMap<T> getCentralities(Set<T> subGraph, Graph<T> graph);
}
