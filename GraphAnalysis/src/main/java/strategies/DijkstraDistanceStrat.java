package strategies;

import graphmodel.*;
import javafx.util.Pair;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DijkstraDistanceStrat<T extends GraphNode>
        implements OneSourceShortestPathStrat<T> {
    private volatile boolean sourceSet = false;
    private volatile boolean algoFinished = false;

    private final Object oneRunLock = new Object();

    private final Class<T> clazz;
    private int nodesSoFar = 0;
    private T source;
    private int[] distances;
    private boolean[] visited;
    private T[] parrents;
    private Map<T, Integer> indexes = new HashMap<>();

    @Override
    public void computeDistances(Graph<T> graph) {
        if (!sourceSet) {
            throw new UnsupportedOperationException("You need to add a source before starting the algorithm");
        } else {
            synchronized (oneRunLock) {
                initStructures(graph);
                runAlgorithm(graph);
                algoFinished = true;
            }
        }
    }

    private void initStructures(Graph<T> graph) {
        List<T> nodes = graph.getNodes();
        distances = new int[nodes.size()];
        visited = new boolean[nodes.size()];
        parrents = (T[]) Array.newInstance(clazz, nodes.size());
        nodesSoFar = 0;

        for (int i = 0; i < distances.length; i++) {
            distances[i] = Integer.MAX_VALUE;
            parrents[i] = null;
        }

        for (T node : nodes) {
            indexes.put(node, nodesSoFar++);
        }

        distances[indexes.get(source)] = 0;
        parrents[indexes.get(source)] = source;
    }

    private void runAlgorithm(Graph<T> graph) {
        List<T> nodes = new ArrayList<>();
        nodes.addAll(graph.getNodes().stream().collect(Collectors.<T>toList()));

        boolean finished = false;
        while (!finished) {
            T currentNode = getClosestUnvisited(nodes);
            finished = currentNode == null || updateDistances(currentNode);
        }
    }

    private T getClosestUnvisited(List<T> items) {
        T min = null;
        int minDist = Integer.MAX_VALUE;
        for(T t : items){
            int index = indexes.get(t);
            if(!visited[index]){
                if(minDist >= distances[index]){
                    minDist = distances[index];
                    min = t;
                }
            }
        }

        return min;
    }

    @Override
    public void setSource(T source) {
        this.source = source;
        algoFinished = false;
        sourceSet = true;
    }

    @Override
    public T getSource() {
        return source;
    }

    @Override
    public int getDistance(T destination) {
        if (algoFinished) {
            return distances[indexes.get(destination)];
        } else {
            throw new UnsupportedOperationException("The algorithm did not finish his execution yet!");
        }
    }

    @Override
    public T getParrent(T node) {
        if (algoFinished) {
            return parrents[indexes.get(node)];
        } else {
            throw new UnsupportedOperationException("The algorithm did not finish his execution yet!");
        }
    }

    private boolean updateDistances(T node) {
        boolean noChange = true;

        if (!(node instanceof WeightedGraphNode)) {
            Set<GraphEdge> edges = node.getEdges();

            for (GraphEdge edge : edges) {
                if(distances[indexes.get(getSourceNode(node, edge))] < Integer.MAX_VALUE) {
                    if (distances[indexes.get(getOutNode(node, edge))] <
                            distances[indexes.get(getSourceNode(node, edge))] + 1) {
                        distances[indexes.get(getOutNode(node, edge))] =
                                distances[indexes.get(getSourceNode(node, edge))] + 1;
                        parrents[indexes.get(getOutNode(node, edge))] = node;
                        noChange = false;
                    }
                }
            }

        } else {
            Set<GraphEdge> edges = node.getEdges();

            for (GraphEdge edge : edges) {
                if(distances[indexes.get(getSourceNode(node, edge))] < Integer.MAX_VALUE) {
                    if (distances[indexes.get(getOutNode(node, edge))] >
                            distances[indexes.get(getSourceNode(node, edge))] +
                                    ((WeightedGraphEdge) edge).getWeight()) {
                        distances[indexes.get(getOutNode(node, edge))] =
                                distances[indexes.get(getSourceNode(node, edge))] + ((WeightedGraphEdge) edge).getWeight();
                        parrents[indexes.get(getOutNode(node, edge))] = node;
                        noChange = false;
                    }
                }
            }

        }
        visited[indexes.get(node)] = true;
        return noChange;
    }

    private T getOutNode(T source, GraphEdge<T> edge){
        Pair<T,T> nodes = edge.getNodesInvolved();
        if(nodes.getKey().equals(source)){
            return nodes.getValue();
        } else {
            return nodes.getKey();
        }
    }

    private T getSourceNode(T source, GraphEdge<T> edge){
        Pair<T,T> nodes = edge.getNodesInvolved();
        if(nodes.getKey().equals(source)){
            return nodes.getKey();
        } else {
            return nodes.getValue();
        }
    }
}
