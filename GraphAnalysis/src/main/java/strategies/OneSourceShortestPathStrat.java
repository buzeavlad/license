package strategies;

import graphmodel.Graph;
import graphmodel.GraphNode;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface OneSourceShortestPathStrat<T extends GraphNode> {
    void computeDistances(Graph<T> t);

    void setSource(T source);

    T getSource();

    int getDistance(T destination);

    T getParrent(T node);
}
