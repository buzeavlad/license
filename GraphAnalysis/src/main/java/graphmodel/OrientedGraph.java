package graphmodel;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface OrientedGraph<T extends OrientedGraphNode> extends Graph<T>{}
