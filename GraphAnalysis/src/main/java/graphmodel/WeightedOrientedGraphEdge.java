package graphmodel;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface WeightedOrientedGraphEdge extends WeightedGraphEdge, OrientedGraphEdge {}
