package graphmodel;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface WeightedOrientedGraph<T extends WeightedOrientedGraphNode> extends Graph<T>{}
