package graphmodel;

import gnu.trove.list.array.TIntArrayList;
import strategies.CentralityStrategy;
import strategies.CommunityDetectionStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Florin on 18-Apr-16.
 */
public class StanfordGraph implements Graph<StanfordNode> {
    private List<StanfordNode> nodes = new ArrayList<>();

    @Override
    public List<Set<StanfordNode>> findCommunities(CommunityDetectionStrategy strategy) {
        return null;
    }

    @Override
    public TIntArrayList getCentralities(CentralityStrategy strategy) {
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public List<StanfordNode> getNodes() {
        return nodes;
    }

    public void addNode(StanfordNode node){
        nodes.add(node);
    }

    @Override
    public List<GraphEdge<StanfordNode>> getEdges() {
        return null;
    }
}
