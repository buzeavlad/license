package graphmodel;

import javafx.util.Pair;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface GraphEdge<T extends GraphNode> {
    Pair<T, T> getNodesInvolved();
}
