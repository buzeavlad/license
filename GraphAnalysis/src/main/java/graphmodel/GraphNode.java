package graphmodel;

import java.util.Set;

/**
 * Created by Florin on 15-Feb-16.
 */
public interface GraphNode {
    Set<GraphEdge> getEdges();
}
