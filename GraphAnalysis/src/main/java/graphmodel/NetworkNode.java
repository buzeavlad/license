package graphmodel;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * Created by Florin on 15-Feb-16.
 */
@RequiredArgsConstructor
public class NetworkNode implements WeightedOrientedGraphNode {
    //todo make this map on the db model

    @Getter
    private final String name;

    @Override
    public Set<GraphEdge> getEdges() {
        return null;
    }

    @Override
    public List<OrientedGraphEdge> getIncomingEdges() {
        return null;
    }

    @Override
    public List<OrientedGraphEdge> getOutgoingEdges() {
        return null;
    }
}
