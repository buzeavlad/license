package graphmodel;

/**
 * Created by Florin on 26-Feb-16.
 */

public interface WeightedGraph<T extends WeightedGraphNode> extends Graph<T> {}
