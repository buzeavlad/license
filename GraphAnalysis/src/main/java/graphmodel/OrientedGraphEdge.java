package graphmodel;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface OrientedGraphEdge<T extends OrientedGraphNode> extends GraphEdge<T> {
    T getSource();

    T getDestination();
}
