package graphmodel;

import gnu.trove.list.array.TIntArrayList;
import strategies.CentralityStrategy;
import strategies.CommunityDetectionStrategy;

import java.util.List;
import java.util.Set;

/**
 * Created by Florin on 15-Feb-16.
 */
public class NetworkGraph implements WeightedOrientedGraph<NetworkNode> {
    //todo make this map on the db model

    @Override
    public List<Set<NetworkNode>> findCommunities(CommunityDetectionStrategy strategy) {
        return null;
    }

    @Override
    public TIntArrayList getCentralities(CentralityStrategy strategy) {
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public List<NetworkNode> getNodes() {
        return null;
    }

    @Override
    public List<GraphEdge<NetworkNode>> getEdges() {
        return null;
    }
}
