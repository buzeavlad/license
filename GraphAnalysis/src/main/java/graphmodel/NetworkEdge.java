package graphmodel;

import javafx.util.Pair;

/**
 * Created by Florin on 29-Feb-16.
 */
public class NetworkEdge implements WeightedOrientedGraphEdge {
    //todo make this map on the db model

    @Override
    public OrientedGraphNode getSource() {
        return null;
    }

    @Override
    public OrientedGraphNode getDestination() {
        return null;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public Pair getNodesInvolved() {
        return null;
    }
}
