package graphmodel;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface WeightedGraphEdge<T extends GraphNode> extends GraphEdge<T> {
    int getWeight();
}
