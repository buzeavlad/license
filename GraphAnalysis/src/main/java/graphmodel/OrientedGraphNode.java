package graphmodel;

import java.util.List;

/**
 * Created by Florin on 26-Feb-16.
 */
public interface OrientedGraphNode extends GraphNode {
    List<OrientedGraphEdge> getIncomingEdges();

    List<OrientedGraphEdge> getOutgoingEdges();
}
