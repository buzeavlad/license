package graphmodel;

import gnu.trove.list.array.TIntArrayList;
import strategies.CentralityStrategy;
import strategies.CommunityDetectionStrategy;

import java.util.List;
import java.util.Set;

/**
 * Created by Florin on 15-Feb-16.
 */
public interface Graph<T extends GraphNode> {

    List<Set<T>> findCommunities(CommunityDetectionStrategy strategy);

    TIntArrayList getCentralities(CentralityStrategy strategy);

    int getSize();

    List<T> getNodes();

    List<GraphEdge<T>> getEdges();
}
