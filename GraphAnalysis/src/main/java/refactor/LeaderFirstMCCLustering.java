package refactor;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import lombok.Getter;
import refactor.metrics.EmbedednessComputer;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 * Created by Florin on 18-May-16.
 */
public class LeaderFirstMCCLustering implements MCClusteringAlgorithm {
    private final TIntList connectivityList = new TIntArrayList();
    private final BitSet visitedSet = new BitSet();
    private final EmbedednessComputer metric;
    @Getter
    private TIntList clusterHeads = new TIntArrayList();
    private List<TIntSet> clusters = new ArrayList<>();

    public LeaderFirstMCCLustering() {
        this.metric = new EmbedednessComputer();
    }

    @Override
    public List<TIntSet> computeClusters(Graph graph) {
        increaseSizeIfNeeded(connectivityList, graph.size());

        computeConnectivities(graph, connectivityList); //step 1
        List<TIntSet> clusters = formClusters(graph, connectivityList, visitedSet); //step 2 + 3
        selectHeads(graph, clusters);//step 4

        return clusters;
    }

    private void increaseSizeIfNeeded(TIntList connectivityList, int size) {
        if(connectivityList.size() < size){
            int currentSize = connectivityList.size();
            for(int i = 0; i < size - currentSize; i++){
                connectivityList.add(0);
            }
        }
    }

    public void updateNodeEdge(int from, int to, TIntSet cluster){
        //todo
    }

    public void deleteNode(int node){

    }

    public void insertNode(int node){
        //todo maybe add edges as well ??
    }

    private TIntList selectHeads(Graph graph, List<TIntSet> clusters) {
        int nrClusters = clusters.size();
        TIntList clusterHeads = new TIntArrayList();
        int maxHead;
        double maxEmbededNess;

        for(int i = 0; i < nrClusters; i++){
            TIntSet cluster = clusters.get(i);

            if(cluster.size() == 1){
                int singleHead = cluster.iterator().next();
                clusterHeads.add(singleHead);
            }
            else {
                maxEmbededNess = -1;
                maxHead = -1;

                for (TIntIterator iterator = cluster.iterator(); iterator.hasNext(); ) {
                    int node = iterator.next();
                    double embededness = metric.getNodeEmbeddedness(graph, cluster, node);
                    if(embededness > maxEmbededNess){
                        maxEmbededNess = embededness;
                        maxHead = node;
                    }
                }
                clusterHeads.add(maxHead);
            }
        }

        this.clusterHeads = clusterHeads;

        return clusterHeads;
    }

    private TIntSet startClique(Graph graph, int node, TIntList connectivityList, BitSet visitedSet) {
        int currentNode = node;

        if (connectivityList.get(currentNode) == 0) {
            TIntSet singleCluster = new TIntHashSet();
            singleCluster.add(currentNode);
            return singleCluster;
        } else {
            TIntSet newClique = new TIntHashSet();
            int clusterMaxSize = graph.size();
            boolean hasCommonNodes = true;

            while (newClique.size() < clusterMaxSize && hasCommonNodes) {
                hasCommonNodes = false;
                visitedSet.set(currentNode);
                newClique.add(currentNode);

                currentNode = findMaximumConnection(graph, currentNode, visitedSet);

                if (currentNode != -1) {
                    hasCommonNodes = true;
                }
            }

            return newClique;
        }
    }

    private int findMaximumConnection(Graph graph, int currentNode, BitSet visitedSet) {
        int N = graph.size();
        int maxConnections = 0;
        int nextCurrentNode = -1;
        int commonConnections;

        for (int node = 0; node < N; node++) {
            commonConnections = 0;

            if (!visitedSet.get(node)) {
                for (int possibleCommon = 0; possibleCommon < N; possibleCommon++) {
                    if (graph.getEdge(currentNode, possibleCommon) > 0 && graph.getEdge(node, possibleCommon) > 0) {
                        commonConnections++;
                    }
                }
            }

            if (commonConnections > maxConnections) {
                nextCurrentNode = node;
                maxConnections = commonConnections;
            }
        }

        return nextCurrentNode;
    }

    private void computeConnectivities(Graph graph, TIntList connectivityList) {
        int N = graph.size();
        int connectivity = 0;

        for (int i = 0; i < N; i++) {
            connectivity = 0;
            for (int j = 0; j < N; j++) {
                if (graph.getEdge(i, j) > 0) {
                    connectivity++;
                }
            }
            connectivityList.set(i, connectivity);
        }
    }


    private List<TIntSet> formClusters(Graph graph, TIntList connectivityList, BitSet visitedSet) {
        int N = graph.size();
        visitedSet.clear();
        TIntList indexList = new TIntArrayList();
        List<TIntSet> clusters = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            indexList.add(i);
        }

        sortByConnectivity(indexList, connectivityList);

        for (int i = 0; i < N; i++) {
            if (!visitedSet.get(indexList.get(i))) {
                TIntSet newCluster = startClique(graph, indexList.get(i), connectivityList, visitedSet);
                clusters.add(newCluster);
            }
        }

        return clusters;
    }

    private void sortByConnectivity(TIntList indexList, TIntList connectivityList) {
        boolean ok = true;
        int temp;
        while (ok) {//bubble sort
            ok = false;
            for (int i = 0; i < indexList.size() - 1; i++) {
                if (connectivityList.get(indexList.get(i)) < connectivityList.get(indexList.get(i + 1))) {
                    temp = indexList.get(i);
                    indexList.set(i, indexList.get(i + 1));
                    indexList.set(i + 1, temp);
                    ok = true;
                }
            }
        }
    }

}
