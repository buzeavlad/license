package refactor;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;

public class PrimitiveDijkstra implements OneSourceShortestPathStrat{
    @Getter
    @Setter
    private Graph graph;
    private TIntList parents;
    private TIntList distances;
    private boolean[] visited;

    /**
     * @param source node from which the algorithm starts
     * @return a pair of distances and parent nodes
     */
    @Override
    public Pair<TIntList, TIntList> findShortestPaths(int source) {
        clear(graph.size());

        distances.set(source, 0);
        parents.set(source, source);

        runAlgorithm(distances, parents, graph);

        return new Pair<>(distances, parents);
    }

    private void clear(int size) {
        if (distances == null || parents == null || visited == null) {
            distances = new TIntArrayList(graph.size());
            parents = new TIntArrayList(graph.size());
            visited = new boolean[size];
        } else if (distances.size() < size) {
            distances = new TIntArrayList(graph.size());
            parents = new TIntArrayList(graph.size());
            visited = new boolean[size];
        }

        initStructures(distances, parents, visited, size);
    }

    private void initStructures(TIntList distances, TIntList parents, boolean[] visited, int size){
        distances.clear();
        parents.clear();

        for(int i = 0; i < size; i++){
            distances.add(Integer.MAX_VALUE);
            parents.add(- 1);
            visited[i] = false;
        }
    }

    private void runAlgorithm(TIntList distanceList, TIntList paretList, Graph graph) {
        boolean finished = false;

        while (!finished) {
            int currentNode = getClosestUnvisited(graph, visited, distances);
            finished = currentNode == -1 || updateDistances(graph, currentNode);
        }
    }

    private boolean updateDistances(Graph graph, int currentNode) {
        boolean noChange = true;

        for (int node = 0; node < graph.size(); node++) {
            if (distances.get(node) < Integer.MAX_VALUE) {
                if (distances.get(node) > distances.get(currentNode) + graph.getEdge(currentNode, node)) {
                    distances.set(node, distances.get(currentNode) + graph.getEdge(currentNode, node));
                    parents.set(node, currentNode);
                    noChange = false;
                }
            } else {
                if (graph.getEdge(currentNode, node) > 0){
                    distances.set(node, distances.get(currentNode) + graph.getEdge(currentNode, node));
                    parents.set(node, currentNode);
                    noChange = false;
                }
            }
        }
        visited[currentNode] = true;
        return noChange;
    }


    private int getClosestUnvisited(Graph graph, boolean[] visited, TIntList distances) {
        int min = -1;
        int minDist = Integer.MAX_VALUE;
        for (int node = 0; node < graph.size(); node++) {
            if (!visited[node]) {
                if (minDist >= distances.get(node)) {
                    minDist = distances.get(node);
                    min = node;
                }
            }
        }

        return min;
    }

}
