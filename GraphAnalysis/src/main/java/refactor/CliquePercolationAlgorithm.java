package refactor;

import gnu.trove.set.TIntSet;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import static refactor.utils.SetUtils.*;

/**
 * Created by Florin on 27-May-16.
 */
public class CliquePercolationAlgorithm implements MCClusteringAlgorithm {
    private final CliqueSearchStrategy localCliqueStrat;

    private BitSet visitSet = new BitSet();
    private double credibility = 0d;

    public CliquePercolationAlgorithm(CliqueSearchStrategy localCliqueStrat) {
        this(localCliqueStrat, 0d);
    }

    public CliquePercolationAlgorithm(CliqueSearchStrategy localCliqueStrat, double fuzzyThreshold){
        this.credibility = fuzzyThreshold;
        this.localCliqueStrat = localCliqueStrat;
    }

    @Override
    public List<TIntSet> computeClusters(Graph graph) {
        List<TIntSet> cliques = localCliqueStrat.findCliques(graph);
        cleanDuplicates(cliques);
        List<TIntSet> clusters = meshCliques(cliques);
        return clusters;
    }

    private List<TIntSet> meshCliques(List<TIntSet> cliques) {
        visitSet.clear();
        int noCliques = cliques.size();
        List<TIntSet> clusters = new ArrayList<>(noCliques / 5);

        for (int i = 0; i < noCliques; i++) {
            if (!visitSet.get(i)) {
                TIntSet newCluster = cliques.get(i);
                addAdjacentCliques(newCluster, cliques);
                visitSet.set(i);
                clusters.add(newCluster);
            }
        }

        return clusters;
    }

    private void addAdjacentCliques(TIntSet newCluster, List<TIntSet> cliques) {
        boolean foundAdjacentClique = true;
        int noCliques = cliques.size();
        while(foundAdjacentClique){
            foundAdjacentClique = false;

            for(int j = 0; j < noCliques; j++){
                if(!visitSet.get(j)) {
                    TIntSet secondClique = cliques.get(j);

                    if(clustersAdjacent(newCluster, secondClique)) {
                        unionWith(newCluster, secondClique);
                        foundAdjacentClique = true;
                        visitSet.set(j);
                    }
                }
            }

        }
    }

    private boolean clustersAdjacent(TIntSet firstClique, TIntSet secondClique){
        if(credibility == 0d){
            return areClustersAdjacent(firstClique, secondClique);
        } else {
            return areClustersFuzzyAdjacent(firstClique, secondClique, credibility);
        }
    }

    private void addFuzzyAdjacentCliques(TIntSet newCluster, List<TIntSet> cliques, double threshold) {
        boolean foundAdjacentClique = true;
        int noCliques = cliques.size();
        while(foundAdjacentClique){
            foundAdjacentClique = false;

            for(int j = 0; j < noCliques; j++){
                if(!visitSet.get(j)) {
                    TIntSet secondClique = cliques.get(j);

                    if(areClustersFuzzyAdjacent(newCluster, secondClique, threshold)) {
                        unionWith(newCluster, secondClique);
                        foundAdjacentClique = true;
                        visitSet.set(j);
                    }
                }
            }

        }
    }
}
