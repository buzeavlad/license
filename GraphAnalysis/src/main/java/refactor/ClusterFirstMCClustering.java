package refactor;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;

import java.util.*;

import static refactor.utils.SetUtils.cleanDuplicates;

/**
 * Created by Florin on 18-May-16.
 */
public class ClusterFirstMCClustering implements MCClusteringAlgorithm {
    private final CliqueSearchStrategy cliqueStrategy;
    private final CliqueComparator comparator = new CliqueComparator();

    public ClusterFirstMCClustering(CliqueSearchStrategy cliqueStrategy) {
        this.cliqueStrategy = cliqueStrategy;
    }

    @Override
    public List<TIntSet> computeClusters(Graph graph) {
        int N = graph.size();
        List<TIntSet> initialCliques = cliqueStrategy.findCliques(graph);//step 1
        List<TIntSet> secondCliques = new ArrayList<>();
        updateMaximumCliques(N, initialCliques, secondCliques);
        updateFinalCliques(N, secondCliques);

        cleanDuplicates(secondCliques);

        return secondCliques;
    }

    private void updateMaximumCliques(int n, List<TIntSet> initialCliques, List<TIntSet> secondCliques) {
        //step 2
        for (int i = 0; i < n; i++) {
            TIntSet bestCurrentClique = initialCliques.get(i);
            for (int j = 0; j < n; j++) {
                TIntSet possibleNextClique = initialCliques.get(j);
                if (possibleNextClique.contains(i)) {
                    boolean needsUpdate = !comparator.isGreater(bestCurrentClique, possibleNextClique);
                    if (needsUpdate) {
                        bestCurrentClique = possibleNextClique;
                    }
                }
            }
            secondCliques.add(bestCurrentClique);
        }
    }

    private void updateFinalCliques(int size, List<TIntSet> secondCliques) {
        //step 3
        for (int i = 0; i < size; i++) {
            TIntSet cliqueI = secondCliques.get(i);

            for (TIntIterator iterator = cliqueI.iterator(); iterator.hasNext(); ) {
                int j = iterator.next();
                TIntSet cliqueJ = secondCliques.get(j);
                if (!cliqueJ.contains(i)) {
                    iterator.remove();
                }
            }
        }
    }

    private class CliqueComparator {
        public boolean isGreater(TIntSet clique1, TIntSet clique2) {
            if (clique1.size() != clique2.size()) {
                return clique1.size() >= clique2.size();
            } else {
                int aux, c1 = Integer.MAX_VALUE, c2 = Integer.MAX_VALUE;
                for (TIntIterator iterator = clique1.iterator(); iterator.hasNext(); ) {
                    aux = iterator.next();
                    if (!clique2.contains(aux) && aux < c1) {
                        c1 = aux;
                    }
                }

                for (TIntIterator iterator = clique2.iterator(); iterator.hasNext(); ) {
                    aux = iterator.next();
                    if (!clique1.contains(aux) && aux < c2) {
                        c2 = aux;
                    }
                }

                return c1 >= c2;
            }
        }
    }
}
