package refactor;

import gnu.trove.set.TIntSet;

import java.util.List;

/**
 * Created by Florin on 17-May-16.
 */
public interface MCClusteringAlgorithm {
    List<TIntSet> computeClusters(Graph graph);
}
