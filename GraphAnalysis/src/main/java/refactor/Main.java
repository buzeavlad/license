package refactor;

import gnu.trove.set.TIntSet;
import refactor.metrics.*;
import refactor.utils.StatisticsUtils;
import refactor.utils.SocialNetwork;
import refactor.utils.StanfordDataParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Florin on 16-May-16.
 */
public class Main {
    private static final String OUTPUT_PATH = "clustering.out";

    private static final int[] GRAPH_IDS = {0, 107, 348, 414, 686, 1912};

    private static MCClusteringAlgorithm clusteringAlgorithm = new LeaderFirstMCCLustering();
    private static ClusteringComparer randIndexComparer = new RandIndex();
    private static ClusteringComparer vanDongenMeasureComparer = new VanDongenMeasure();
    private static ClusteringComparer normalizedMutualInfoComparer = new NormalizedMutualInfo();
    private static ClusteringComparer normalizedVariationOfInfoComparer = new NormalizedVariationInfo();

    public static void main(String[] args) throws IOException, URISyntaxException {
        BufferedWriter bf = new BufferedWriter(new FileWriter(OUTPUT_PATH));

        for(int i = 0 ; i < GRAPH_IDS.length; i++) {
            int id = GRAPH_IDS[i];
            StanfordDataParser dataParser = new StanfordDataParser(SocialNetwork.FACEBOOK);
            StanfordGraph graph = dataParser.parseData(id);

            List<TIntSet> computedClusters = clusteringAlgorithm.computeClusters(graph);
            List<TIntSet> annotatedClusters = dataParser.parseCircles(id, graph);

            double randIndex = randIndexComparer.compareClusterings(computedClusters, annotatedClusters, graph.size());
            double vanDongenMeasure = vanDongenMeasureComparer.compareClusterings(computedClusters, annotatedClusters, graph.size());
            double normalizedMutualInfo = normalizedMutualInfoComparer.compareClusterings(computedClusters, annotatedClusters, graph.size());
            double normalizedVariationOfInfo = normalizedVariationOfInfoComparer.compareClusterings(computedClusters, annotatedClusters, graph.size());

            bf.write("Graph " + id + "\n");
            bf.write("Graph size : " + graph.size() + "\n");
            bf.write("Rand index : " + randIndex + "\n");
            bf.write("Van Dongen measure : " + vanDongenMeasure + "\n");
            bf.write("Normalized Mutual info : " + normalizedMutualInfo + "\n");
            bf.write("NormalizedVariationOfInfo : " + normalizedVariationOfInfo + "\n");

            bf.write("\n");
            bf.write("--------------------------------------------------------");
            bf.write("\n");
        }

        bf.close();
        System.out.println("Finished clustering!");
    }
}
