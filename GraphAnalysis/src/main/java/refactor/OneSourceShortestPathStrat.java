package refactor;

import gnu.trove.list.TIntList;
import javafx.util.Pair;

public interface OneSourceShortestPathStrat {
        Pair<TIntList, TIntList> findShortestPaths(int source);
}
