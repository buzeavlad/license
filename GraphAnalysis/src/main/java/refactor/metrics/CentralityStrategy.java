package refactor.metrics;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import refactor.Graph;

/**
 * Created by Florin on 16-May-16.
 */
public interface CentralityStrategy {
    TDoubleList getCentralities(Graph graph);
}
