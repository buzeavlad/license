package refactor.metrics;

import refactor.Graph;

/**
 * Created by Florin on 19-May-16.
 */
public class OutDegreeCentrality extends DegreeCentrality {
    @Override
    protected double getNodeCentrality(int node, Graph graph) {
        int graphSize = graph.size();
        double centrality = 0;

        for (int destination = 0; destination < graphSize; destination++) {
            if(graph.getEdge(node, destination) > 0){
                centrality ++;
            }
        }

        return centrality;
    }
}
