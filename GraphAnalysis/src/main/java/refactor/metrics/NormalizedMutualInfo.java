package refactor.metrics;

import gnu.trove.set.TIntSet;
import refactor.utils.SetUtils;
import refactor.utils.StatisticsUtils;

import java.util.List;

import static refactor.utils.StatisticsUtils.getEntropy;
import static refactor.utils.StatisticsUtils.getMutualInfo;


/**
 * Created by Florin on 21-Jun-16.
 */
public class NormalizedMutualInfo implements ClusteringComparer {

    @Override
    public double compareClusterings(List<TIntSet> firstClustering, List<TIntSet> secondClustering, int totalNodes) {
        double entropy1 = getEntropy(firstClustering, totalNodes);
        double entropy2 = getEntropy(secondClustering, totalNodes);
        double mutualInfo = getMutualInfo(firstClustering, secondClustering, totalNodes);
        return 2 * mutualInfo / (entropy1 + entropy2);
    }
}
