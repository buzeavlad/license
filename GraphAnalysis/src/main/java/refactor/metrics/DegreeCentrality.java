package refactor.metrics;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import refactor.Graph;

/**
 * Created by Florin on 19-May-16.
 */
public abstract class DegreeCentrality implements CentralityStrategy{
    @Override
    public TDoubleList getCentralities(Graph graph) {
        int size = graph.size();
        TDoubleList result = new TDoubleArrayList();

        for(int i = 0; i < size; i++){
            double nodeCentrality = getNodeCentrality(i, graph);
            result.add(nodeCentrality);
        }

        return result;
    }

    protected abstract double getNodeCentrality(int node, Graph graph);
}
