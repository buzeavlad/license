package refactor.metrics;

import refactor.Graph;

/**
 * Created by Florin on 19-May-16.
 */
public class SummedNodeDegreeCentrality extends DegreeCentrality {

    @Override
    protected double getNodeCentrality(int node, Graph graph) {
        double centrality = 0;
        int size = graph.size();

        for (int i = 0; i < size; i++) {
            if (graph.getEdge(i, node) > 0) {
                centrality++;
            }
            if (graph.getEdge(node, i) > 0) {
                centrality++;
            }
        }

        return centrality;
    }
}
