package refactor.metrics;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;
import refactor.Graph;

/**
 * Created by Florin on 20-May-16.
 */
public class TransitivityComputer {

    public double getAverageGraphTransitivity(Graph graph) {
        double result = 0;

        int size = graph.size();
        for (int i = 0; i < size - 2; i++) {
            for (int j = i + 1; j < size - 1; j++) {
                for (int k = j + 1; k < size - 2; k++) {
                    if (graph.getEdge(i, j) > 0 && graph.getEdge(j, k) > 0) {
                        if (graph.getEdge(i, k) > 0) {
                            result++;
                        }
                    }
                }
            }
        }

        return result / getCombNtake3(size);
    }

    public double getAverageClusterTransitivity(Graph graph, TIntSet cluster) {
        double result = 0;

        for (TIntIterator itI = cluster.iterator(); itI.hasNext(); ) {
            int i = itI.next();
            for (TIntIterator itJ = cluster.iterator(); itJ.hasNext(); ) {
                int j = itJ.next();
                for (TIntIterator itK = cluster.iterator(); itK.hasNext(); ) {
                    int k = itK.next();
                    if (graph.getEdge(i, j) > 0 && graph.getEdge(j, k) > 0) {
                        if (graph.getEdge(i, k) > 0) {
                            result++;
                        }
                    }
                }
            }
        }

        int clusterSize = cluster.size();

        return result / (2 * getCombNtake3(clusterSize));
    }

    private double getCombNtake3(int n) {
        return n * (n - 1) * (n - 2) / 6;
    }
}
