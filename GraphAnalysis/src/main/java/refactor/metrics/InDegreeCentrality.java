package refactor.metrics;

import refactor.Graph;

/**
 * Created by Florin on 19-May-16.
 */
public class InDegreeCentrality extends DegreeCentrality {
    @Override
    protected double getNodeCentrality(int node, Graph graph) {
        int graphSize = graph.size();
        double centrality = 0;


        for (int source = 0; source < graphSize; source++) {
            if(graph.getEdge(source, node) > 0){
                centrality ++;
            }
        }

        return centrality;
    }
}
