package refactor.metrics;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import refactor.Graph;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class BetweenessCentrality implements CentralityStrategy {
    private BitSet visited = new BitSet();

    @Override
    public TDoubleList getCentralities(Graph graph) {
        int size = graph.size();

        TDoubleList result = new TDoubleArrayList(size);
        List<TIntList> paretLists = new ArrayList<>();

        TIntList ro = new TIntArrayList();
        TIntList distances = new TIntArrayList();

        for (int i = 0; i < size; i++) {
            result.add(0);
            ro.add(0);
            distances.add(Integer.MAX_VALUE);
            paretLists.add(new TIntArrayList());
        }

        TIntList reusableNeighbours = new TIntArrayList();


        for (int i = 0; i < size; i++) {
            resetDataStructures(size, paretLists, ro, distances, reusableNeighbours, i, visited);
            boolean finished = false;
            while (!finished) {
                int currentNode = getClosestUnvisited(graph, visited, distances);
                if (currentNode != -1) {
                    reusableNeighbours.add(currentNode);
                    updatePaths(graph, currentNode, distances, ro, paretLists);
                } else {
                    finished = true;
                }
            }

            TDoubleList delta = new TDoubleArrayList();

            for (int ii = 0; ii < size; ii++) {
                delta.add(0);
            }

            addNodeCentralities(result, paretLists, ro, i, reusableNeighbours, delta);
        }

        for(int i = 0; i < size; i++){
            result.set(i, result.get(i) / 2);
        }

        return result;
    }

    private void addNodeCentralities(TDoubleList result, List<TIntList> paretLists, TIntList ro, int node, TIntList visited, TDoubleList delta) {
        int nodesUpdated = visited.size();
        for (int i = nodesUpdated - 1; i >= 0; i--) {
            int nodeToUpdate = visited.get(i);

            for (TIntIterator iterator = paretLists.get(nodeToUpdate).iterator(); iterator.hasNext(); ) {
                int parent = iterator.next();
                double currentDelta = delta.get(parent);
                delta.set(parent, currentDelta + (ro.get(parent)) * (1 + delta.get(nodeToUpdate)) / ro.get(nodeToUpdate));
            }

            if (nodeToUpdate != node) {
                double currResult = result.get(nodeToUpdate);
                result.set(nodeToUpdate, currResult + delta.get(nodeToUpdate));
            }
        }
    }

    private void updatePaths(Graph graph, int source, TIntList distances, TIntList ro, List<TIntList> parrentList) {
        for (int node = 0; node < graph.size(); node++) {
            if (distances.get(node) != -1 && graph.getEdge(source, node) > 0) {
                if (distances.get(node) > distances.get(source) + graph.getEdge(source, node)) {
                    distances.set(node, distances.get(source) + graph.getEdge(source, node));
                    ro.set(node, ro.get(source));
                    TIntList nodeParents = parrentList.get(node);
                    nodeParents.clear();
                    nodeParents.add(source);
                } else if (distances.get(node) == distances.get(source) + graph.getEdge(source, node)) {
                    int currRo = ro.get(node);
                    ro.set(node, currRo + ro.get(source));
                    parrentList.get(node).add(source);
                }
            } else if (graph.getEdge(source, node) > 0) {
                distances.set(node, distances.get(source) + graph.getEdge(source, node));
                TIntList nodeParents = parrentList.get(node);
                nodeParents.add(source);
                ro.set(node, ro.get(source));
            }
        }

        visited.set(source);
    }

    private int getClosestUnvisited(Graph graph, BitSet visited, TIntList distances) {
        int min = -1;
        int minDist = Integer.MAX_VALUE;
        for (int node = 0; node < graph.size(); node++) {
            if (!visited.get(node)) {
                if (minDist >= distances.get(node)) {
                    minDist = distances.get(node);
                    min = node;
                }
            }
        }

        return min;
    }


    private void resetDataStructures(int size, List<TIntList> pw, TIntList ro, TIntList dist, TIntList reusableNeighbours, int node, BitSet visited) {
        reusableNeighbours.clear();
        visited.clear();

        for (int j = 0; j < size; j++) {
            pw.get(j).clear();
            ro.set(j, 0);
            dist.set(j, Integer.MAX_VALUE);
        }

        ro.set(node, 1);
        dist.set(node, 0);
    }
}
