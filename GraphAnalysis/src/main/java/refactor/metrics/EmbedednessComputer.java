package refactor.metrics;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.set.TIntSet;
import refactor.Graph;

import java.util.List;

/**
 * Created by Florin on 21-May-16.
 */
public class EmbedednessComputer {

    public TDoubleList computeEmbededness(Graph graph, List<TIntSet> clusters) {
        int size = graph.size();
        TDoubleList result = new TDoubleArrayList(size);

        for (int i = 0; i < size; i++) {
            result.add(0d);
        }

        for (TIntSet cluster : clusters) {
            putClusterEmbeddedness(graph, cluster, result);
        }

        return result;
    }

    private void putClusterEmbeddedness(Graph graph, TIntSet cluster, TDoubleList acumulList) {

        for (TIntIterator it = cluster.iterator(); it.hasNext(); ) {
            int node = it.next();
            double embeddedness = getNodeEmbeddedness(graph, cluster, node);
            acumulList.set(node, embeddedness);
        }
    }

    public double getNodeEmbeddedness(Graph graph, TIntSet cluster, int node) {
        int size = graph.size();
        double inside = 0;
        int total = 0;

        for (int i = 0; i < size; i++) {
            if (graph.getEdge(i, node) > 0) {
                if (cluster.contains(i)) {
                    inside++;
                    total++;
                } else {
                    total++;
                }
            }
        }

        return inside / total;
    }
}
