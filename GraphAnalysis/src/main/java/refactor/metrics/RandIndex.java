package refactor.metrics;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.TIntSet;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Florin on 21-Jun-16.
 */
public class RandIndex implements ClusteringComparer {

    @Override
    public double compareClusterings(List<TIntSet> firstClustering, List<TIntSet> secondClustering, int totalNodes) {
        TIntList clustersFirst = getClusterList(firstClustering, totalNodes);
        TIntList clustersSecond = getClusterList(secondClustering, totalNodes);
        int a00 = 0, a11 = 0, a01 = 0, a10 = 0;
        int clusterFI, clusterFJ, clusterSI, clusterSJ;

        for (int i = 1; i <= totalNodes - 1; i++) {
            for (int j = i + 1; j <= totalNodes; j++) {
                clusterFI = clustersFirst.get(i);
                clusterFJ = clustersFirst.get(j);
                clusterSI = clustersSecond.get(i);
                clusterSJ = clustersSecond.get(j);

                if (clusterFI == clusterFJ) {
                    if (clusterSI == clusterSJ) {
                        a11++;
                    } else {
                        a10++;
                    }
                } else {
                    if (clusterSI == clusterSJ) {
                        a01++;
                    } else {
                        a00++;
                    }
                }
            }
        }

        return 1. * (a00 + a11) / (a00 + a11 + a10 + a01);
    }

    public TIntList getClusterList(List<TIntSet> clusters, int nodes) {
        TIntList clusterList = new TIntArrayList();
        for (int i = 0; i <= nodes; i++) {
            clusterList.add(0);
        }

        int i = 0;
        for (Iterator<TIntSet> setIterator = clusters.iterator(); setIterator.hasNext(); ) {
            TIntSet cluster = setIterator.next();
            i++;

            for (TIntIterator nodeIterator = cluster.iterator(); nodeIterator.hasNext(); ) {
                int node = nodeIterator.next();
                clusterList.set(node, i);
            }
        }

        return clusterList;
    }
}
