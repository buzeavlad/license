package refactor.metrics;

import gnu.trove.set.TIntSet;

import java.util.List;

/**
 * Created by Florin on 21-Jun-16.
 */
public interface ClusteringComparer {
    double compareClusterings(List<TIntSet> firstClustering, List<TIntSet> secondClustering, int totalNodes);
}
