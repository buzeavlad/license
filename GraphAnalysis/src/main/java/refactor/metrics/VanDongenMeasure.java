package refactor.metrics;

import gnu.trove.set.TIntSet;
import refactor.utils.SetUtils;

import java.util.List;

/**
 * Created by Florin on 21-Jun-16.
 */
public class VanDongenMeasure implements ClusteringComparer {
    @Override
    public double compareClusterings(List<TIntSet> firstClustering, List<TIntSet> secondClustering, int totalNodes) {
        int result = 2 * totalNodes;

        for (TIntSet clusterFromFirst : firstClustering) {
            result -= getMaximumOverlappCluster(clusterFromFirst, secondClustering);
        }

        for (TIntSet clusterFromSecond : secondClustering) {
            result -= getMaximumOverlappCluster(clusterFromSecond, firstClustering);
        }

        return result;
    }

    private int getMaximumOverlappCluster(TIntSet firstCluster, List<TIntSet> otherClustering) {
        int maxOverlapp = 0;

        for (TIntSet cluster : otherClustering) {
            int intersectionSize = SetUtils.intersectionSize(firstCluster, cluster);
            if (intersectionSize > maxOverlapp) {
                maxOverlapp = intersectionSize;
            }
        }

        return maxOverlapp;
    }
}
