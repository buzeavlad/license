package refactor.metrics;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.array.TDoubleArrayList;
import refactor.AllPairsShortestPath;
import refactor.Graph;
import refactor.HashSparseMatrix;
import refactor.utils.SparseMatrix;

public class ClosenessCentrality implements CentralityStrategy {
    private AllPairsShortestPath allPairsShortestPath = new AllPairsShortestPath();
    private SparseMatrix distanceMatrix = new HashSparseMatrix();
    private SparseMatrix pathMatrix = new HashSparseMatrix();

    @Override
    public TDoubleList getCentralities(Graph graph) {
        allPairsShortestPath.computePaths(distanceMatrix, pathMatrix);
        TDoubleList centralities = new TDoubleArrayList();

        int size = graph.size();
        for (int i = 0; i < size; i++) {
            int sumDistances = 0;
            for (int j = 0; j < size; j++) {
                sumDistances += distanceMatrix.getAt(i, j);
            }

            centralities.add(((double) (size - 1)) / sumDistances);
        }

        return centralities;
    }
}
