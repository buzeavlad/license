package refactor.metrics;

import gnu.trove.set.TIntSet;
import refactor.utils.SetUtils;

import java.util.List;

import static refactor.utils.StatisticsUtils.getEntropy;
import static refactor.utils.StatisticsUtils.getMutualInfo;

/**
 * Created by Florin on 21-Jun-16.
 */
public class NormalizedVariationInfo implements ClusteringComparer {

    @Override
    public double compareClusterings(List<TIntSet> firstClustering, List<TIntSet> secondClustering, int totalNodes) {
        double entropy1 = getEntropy(firstClustering, totalNodes);
        double entropy2 = getEntropy(secondClustering, totalNodes);
        double mutualInfo = getMutualInfo(firstClustering, secondClustering, totalNodes);

        return (entropy1 + entropy2 - 2 * mutualInfo) / Math.log(totalNodes);
    }
}
