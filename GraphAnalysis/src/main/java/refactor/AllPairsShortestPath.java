package refactor;

import gnu.trove.list.TIntList;
import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;
import refactor.utils.SparseMatrix;

public class AllPairsShortestPath {
    private SparseMatrix sparseMatrix;
    @Getter
    @Setter
    private Graph graph;

    private OneSourceShortestPathStrat strategy;

    public AllPairsShortestPath(OneSourceShortestPathStrat strategy) {
        this.strategy = strategy;
    }

    public AllPairsShortestPath() {
        this(new PrimitiveDijkstra());
    }

    public void computePaths(SparseMatrix distanceMatrix, SparseMatrix parrentMatrix){
        int size = graph.size();
        Pair<TIntList, TIntList> distanceParrentPair;

        for(int i = 0; i < size; i++){
            distanceParrentPair = strategy.findShortestPaths(i);

            for(int j = 0; j < size; j++){
                distanceMatrix.setAt(i, j,  distanceParrentPair.getKey().get(j));
                parrentMatrix.setAt(i, j,  distanceParrentPair.getKey().get(j));
            }
        }
    }


}
