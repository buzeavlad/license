package refactor;

import lombok.RequiredArgsConstructor;
import refactor.utils.SparseMatrix;

/**
 * Created by Florin on 02-May-16.
 */
@RequiredArgsConstructor
public abstract class Graph {
    private final int size;
    private final SparseMatrix edges;

    public int size(){
        return size;
    }

    public int getEdge(int from, int to){
        return edges.getAt(from, to);
    }

    public int setEdge(int from, int to, int value){
        if(value > 0) {
            return edges.setAt(from, to, value);
        } else return 0;
    }

    public int addVal(int from, int to, int addition){
        return setEdge(from, to, getEdge(from, to) + addition);
    }
}
