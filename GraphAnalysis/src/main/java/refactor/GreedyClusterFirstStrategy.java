package refactor;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Florin on 26-May-16.
 */
public class GreedyClusterFirstStrategy implements CliqueSearchStrategy {
    private TIntSet reusableNeighbourSet = new TIntHashSet();

    @Override
    public List<TIntSet> findCliques(Graph graph) {
        int N = graph.size();
        List<TIntSet> cliques = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            TIntSet localClique = getLocalClique(i, graph);
            cliques.add(localClique);
        }

        return cliques;
    }

    private TIntSet getLocalClique(int node, Graph graph) {
        TIntSet localClique = new TIntHashSet();
        localClique.add(node);
        reusableNeighbourSet.clear();
        int N = graph.size();


        for (int i = 0; i < N; i++) {
            if (graph.getEdge(i, node) > 0) {
                reusableNeighbourSet.add(i);
            }
        }

        while (!reusableNeighbourSet.isEmpty()) {
            int nextMaxIntersections = findMaximumIntersection(graph, node, reusableNeighbourSet);

            if (nextMaxIntersections != -1) {
                localClique.add(nextMaxIntersections);
                reusableNeighbourSet.remove(nextMaxIntersections);
                for (TIntIterator it = reusableNeighbourSet.iterator(); it.hasNext(); ) {
                    int possibleCommon = it.next();
                    if (graph.getEdge(nextMaxIntersections, possibleCommon) == 0) {
                        it.remove();
                    }
                }
            } else break;
        }

        return localClique;
    }

    private int findMaximumIntersection(Graph graph, int currentNode, TIntSet neighbourSet) {
        int maxConnections = 0;
        int nextCurrentNode = -1;
        int commonConnections;
        int possibleCommon;

        if(neighbourSet.size() == 1){
            return neighbourSet.iterator().next();
        }

        for (TIntIterator itI = neighbourSet.iterator(); itI.hasNext(); ) {
            commonConnections = 0;
            int neighbour = itI.next();
            for (TIntIterator itJ = neighbourSet.iterator(); itJ.hasNext(); ) {
                possibleCommon = itJ.next();
                if (graph.getEdge(currentNode, possibleCommon) > 0 && graph.getEdge(neighbour, possibleCommon) > 0) {
                    commonConnections++;
                }
            }

            if (commonConnections > maxConnections) {
                nextCurrentNode = neighbour;
                maxConnections = commonConnections;
            } else if (commonConnections == maxConnections && neighbour < nextCurrentNode) {
                nextCurrentNode = neighbour;
                maxConnections = commonConnections;
            }
        }

        return nextCurrentNode;
    }
}
