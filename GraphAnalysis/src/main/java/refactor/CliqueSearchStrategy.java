package refactor;

import gnu.trove.set.TIntSet;

import java.util.List;

/**
 * Created by Florin on 26-May-16.
 */
public interface CliqueSearchStrategy {
    List<TIntSet> findCliques(Graph graph);
}
