package refactor.utils;

import refactor.StanfordGraph;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;

/**
 * Created by Florin on 15-Apr-16.
 */
public interface DataParser {
     StanfordGraph parseData(int graphId) throws FileNotFoundException, URISyntaxException;
}
