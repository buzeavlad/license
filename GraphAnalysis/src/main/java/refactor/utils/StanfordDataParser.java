package refactor.utils;

import gnu.trove.impl.hash.TIntIntHash;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import refactor.Graph;
import refactor.HashSparseMatrix;
import refactor.StanfordGraph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by Florin on 15-Apr-16.
 */
public class StanfordDataParser implements DataParser {
    private static final String PATH_SEPARATOR = System.getProperty("path.separator");
    private static final String CIRCLES_SUFIX = ".circles";
    private static final String EDGES_SUFIX = ".edges";
    private static final String FEATURES_SUFIX = ".feat";
    private static final String FEATURE_NAME_SUFIX = ".featnames";

    private final SocialNetwork socialNetwork;

    public StanfordDataParser(SocialNetwork socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    @Override
    public StanfordGraph parseData(int graphId) throws FileNotFoundException, URISyntaxException {
        String edgesPath = "D:\\ToLearn\\LICENTA\\Data\\Facebook\\Stanford\\facebook\\" + graphId + EDGES_SUFIX;

        StanfordGraph graph = parseEdges(edgesPath);

        return graph;
    }

    public List<TIntSet> parseCircles(int graphId, Graph graph) throws URISyntaxException, FileNotFoundException {
        String edgesPath = "D:\\ToLearn\\LICENTA\\Data\\Facebook\\Stanford\\facebook\\" + graphId + CIRCLES_SUFIX;

        FileReader fileReader = new FileReader(new File(edgesPath));
        Scanner scanner = new Scanner(fileReader);

        List<TIntSet> circles = new ArrayList<>();

        TIntIntHashMap graphTranslationMap = new TIntIntHashMap();

        int size = 0;

        while (scanner.hasNext()) {
            scanner.next();
            TIntSet circle = new TIntHashSet();
            int nextVal;

            while (scanner.hasNextInt()) {
                nextVal = scanner.nextInt();

                if(graphTranslationMap.contains(nextVal)){
                    circle.add(graphTranslationMap.get(nextVal));
                } else {
                    circle.add(size);
                    graphTranslationMap.put(nextVal, size++);
                }
            }
            circles.add(circle);
        }
        scanner.close();

        for(int i = size; i < graph.size(); i++){
            TIntSet singleCluster = new TIntHashSet();
            singleCluster.add(i);
            circles.add(singleCluster);
        }

        return circles;
    }

    private void parseFeatures() {
        //todo
    }

    private StanfordGraph parseEdges(String path) throws FileNotFoundException {
        FileReader fileReader = new FileReader(new File(path));
        Scanner scanner = new Scanner(fileReader);

        TIntIntHashMap graphTranslationMap = new TIntIntHashMap();

        int size = 0;

        SparseMatrix sparseMatrix = new HashSparseMatrix();

        int A, B, graphA, graphB;
        while (scanner.hasNext()) {
            A = scanner.nextInt();
            B = scanner.nextInt();

            if(graphTranslationMap.contains(A)){
                graphA = graphTranslationMap.get(A);
            } else {
                graphA = size;
                graphTranslationMap.put(A, size++);
            }

            if(graphTranslationMap.contains(B)){
                graphB = graphTranslationMap.get(B);
            } else {
                graphB = size;
                graphTranslationMap.put(A, size++);
            }

            sparseMatrix.setAt(graphA - 1, graphB - 1, 1);
        }

        StanfordGraph graph = new StanfordGraph(size, sparseMatrix);

        return graph;
    }

    public static void main(String[] args) throws FileNotFoundException, URISyntaxException {
        StanfordDataParser parser = new StanfordDataParser(SocialNetwork.FACEBOOK);
        parser.parseData(0);
    }
}
