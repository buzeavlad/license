package refactor.utils;

import java.util.Arrays;
import java.util.BitSet;

/**
 * Created by Florin on 04-Apr-16.
 */
public class OldYaleBoolMatrix {
    private BitSet A;
    private int[] IA;
    private int[] JA;
    private int lines, columns;

    public OldYaleBoolMatrix(boolean[][] values) {
        assertValidValues(values);
        initStructure(values);
    }

    public boolean getAt(int line, int column) {
        if((line < 0 || line >= lines) || (column < 0 || column >= columns) ){
            throw new ArrayIndexOutOfBoundsException("The indexes need to be inside the matrix!");
        }

        int startIndex = IA[line], endIndex = IA[line + 1];
        int indexInArray = Arrays.binarySearch(JA, startIndex, endIndex, column);

        return indexInArray >= 0;
    }

    public int getNrLines(){
        return lines;
    }

    public int getNrColumns(){
        return columns;
    }

    private void assertValidValues(boolean[][] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("The matrix cannot be empty!");
        } else if (values[0].length == 0) {
            throw new IllegalArgumentException("The matrix cannot have empty lines!");
        }

        for (boolean[] value : values) {
            if (value.length != values[0].length) {
                throw new IllegalArgumentException("The matrix must have equal number of columns on each line!");
            }
        }
    }

    private void initStructure(boolean[][] values) {
        lines = values.length;
        columns = values[0].length;

        int oldAux, aux = 0, NNZ = 0;

        for (boolean[] line : values) {
            for (boolean value : line) {
                if (value) {
                    NNZ++;
                }
            }
        }

        A = new BitSet(NNZ);
        IA = new int[lines + 1];
        JA = new int[NNZ];

        IA[0] = 0;

        for (int i = 0; i < lines; i++) {
            oldAux = aux;

            for (int j = 0; j < columns; j++) {
                if (values[i][j]) {
                    JA[aux] = j;
                    A.set(aux++);
                }
            }

            IA[i + 1] = IA[i] + (aux - oldAux);
        }
    }
}
