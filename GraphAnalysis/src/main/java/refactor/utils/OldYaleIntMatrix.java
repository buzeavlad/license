package refactor.utils;

import java.util.Arrays;

/**
 * Created by Florin on 04-Apr-16.
 */
public class OldYaleIntMatrix implements SparseMatrix {
    private int[] A;
    private int[] IA;
    private int[] JA;
    private int lines, columns;

    public OldYaleIntMatrix(int[][] values) {
        assertValidValues(values);
        initStructure(values);
    }

    @Override
    public int getAt(int line, int column) {
        if((line < 0 || line >= lines) || (column < 0 || column >= columns) ){
            throw new ArrayIndexOutOfBoundsException("The indexes need to be inside the matrix!");
        }

        int startIndex = IA[line], endIndex = IA[line + 1];
        int indexInArray = Arrays.binarySearch(JA, startIndex, endIndex, column);

        if(indexInArray < 0){
            return 0;
        } else {
            return A[indexInArray];
        }
    }

    @Override
    public int setAt(int lint, int column, int val) {
        //todo is this worth it ?
        return 0;
    }

    public int getNrLines(){
        return lines;
    }

    public int getNrColumns(){
        return columns;
    }

    private void assertValidValues(int[][] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("The matrix cannot be empty!");
        } else if (values[0].length == 0) {
            throw new IllegalArgumentException("The matrix cannot have empty lines!");
        }

        for (int[] value : values) {
            if (value.length != values[0].length) {
                throw new IllegalArgumentException("The matrix must have equal number of columns on each line!");
            }
        }
    }

    private void initStructure(int[][] values) {
        lines = values.length;
        columns = values[0].length;

        int oldAux, aux = 0, NNZ = 0;

        for (int[] line : values) {
            for (int value : line) {
                if (value != 0) {
                    NNZ++;
                }
            }
        }

        A = new int[NNZ];
        IA = new int[lines + 1];
        JA = new int[NNZ];

        IA[0] = 0;

        for (int i = 0; i < lines; i++) {
            oldAux = aux;

            for (int j = 0; j < columns; j++) {
                if (values[i][j] != 0) {
                    JA[aux] = j;
                    A[aux++] = values[i][j];
                }
            }

            IA[i + 1] = IA[i] + (aux - oldAux);
        }
    }
}
