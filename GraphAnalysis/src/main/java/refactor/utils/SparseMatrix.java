package refactor.utils;

/**
 * Created by Florin on 02-May-16.
 */
public interface SparseMatrix {
    int getAt(int line, int column);

    int setAt(int lint, int column, int val);
}
