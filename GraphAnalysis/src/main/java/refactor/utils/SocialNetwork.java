package refactor.utils;

import lombok.Getter;

/**
 * Created by Florin on 15-Apr-16.
 */
public enum SocialNetwork {
    FACEBOOK(false, "facebook"),
    GOOGLE_PLUS(true, "gplus"),
    TWITTER(true, "twitter"),
    REDDIT(false, "reddit"),;

    @Getter
    private final boolean orientedNetwork;
    @Getter
    private final String simpleName;


    SocialNetwork(boolean orientedNetwork, String simpleName) {
        this.orientedNetwork = orientedNetwork;
        this.simpleName = simpleName;
    }
}
