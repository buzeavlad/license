package refactor.utils;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

/**
 * Created by Florin on 06-Apr-16.
 */

public class CoordinateListMatrix implements SparseMatrix{
    private int lines, columns;
    private TIntList values;
    private TIntList valuesRow;
    private TIntList valuesColumn;

    public CoordinateListMatrix(int[][] values) {
        assertValidValues(values);
        initStructure(values);
    }

    private CoordinateListMatrix(Builder builder) {
        this.lines = builder.lines;
        this.columns = builder.columns;
        this.values = builder.values;
        this.valuesRow = builder.valuesRow;
        this.valuesColumn = builder.valuesColumn;
    }

    public int getAt(int line, int column) {
        assertInsideMatrix(line, column);

        int startIndex = getFirstOccurrence(line, valuesRow);
        if (startIndex >= 0) {
            int endIndex = getNextValueIndex(startIndex, valuesRow);
            int indexInArray = valuesColumn.binarySearch(column, startIndex, endIndex);

            if (indexInArray < 0) {
                return 0;
            } else {
                return values.get(indexInArray);
            }
        } else {
            return 0;
        }
    }

    public int setAt(int value, int line, int column) {
        assertInsideMatrix(line, column);
        int startIndex = getFirstOccurrence(line, valuesRow);
        if (startIndex >= 0) {
            int endIndex = getNextValueIndex(startIndex, valuesRow);
            int indexInArray = valuesColumn.binarySearch(column, startIndex, endIndex);

            if (indexInArray < 0) {
                if (value != 0) {
                    int insertionPoint = -indexInArray - 1;
                    insertAtIndex(value, line, column, insertionPoint);
                }
                return 0;
            } else {
                int oldVal = values.get(indexInArray);
                if (value != 0) {
                    values.set(indexInArray, value);
                } else {
                    removeAtIndex(indexInArray);
                }
                return oldVal;
            }
        } else {
            if (value != 0) {
                int insertionPoint = getFirstGreaterValueIndex(0, line, valuesRow);
                insertAtIndex(value, line, column, insertionPoint);
            }
            return 0;
        }
    }

    private void removeAtIndex(int indexInArray) {
        values.removeAt(indexInArray);
        valuesRow.removeAt(indexInArray);
        valuesColumn.removeAt(indexInArray);
    }

    private void insertAtIndex(int value, int line, int column, int index) {
        valuesRow.insert(index, line);
        valuesColumn.insert(index, column);
        values.insert(index, value);
    }

    public int getNrLines() {
        return lines;
    }

    public int getNrColumns() {
        return columns;
    }

    private void assertInsideMatrix(int line, int column) {
        if ((line < 0 || line >= lines) || (column < 0 || column >= columns)) {
            throw new ArrayIndexOutOfBoundsException("The indexes need to be inside the matrix!");
        }
    }

    private int getNextValueIndex(int start, TIntList list) {
        int value = list.get(start);
        return getFirstGreaterValueIndex(start, value, list);
    }

    private int getFirstGreaterValueIndex(int startIndex, int value, TIntList list) {
        int start = startIndex;
        int end = list.size();
        int mid;

        while (start < end) {
            mid = start + end >>> 1;
            int midVal = list.get(mid);
            if (midVal > value) {
                end = mid;
            } else {
                if (midVal == value) {
                    start = mid + 1;
                }
            }
        }

        return start;
    }

    private int getFirstOccurrence(int value, TIntList list) {
        int start = 0, end = list.size();
        int mid;

        if(list.isEmpty()){
            return -1;
        }

        while (start <= end) {
            mid = start + end >>> 1;
            int midVal = list.get(mid);
            if (midVal < value) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }

        if (list.get(start) == value) {
            return start;
        } else {
            return -(start + 1);
        }
    }

    private void assertValidValues(int[][] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("The matrix cannot be empty!");
        } else if (values[0].length == 0) {
            throw new IllegalArgumentException("The matrix cannot have empty lines!");
        }

        for (int[] value : values) {
            if (value.length != values[0].length) {
                throw new IllegalArgumentException("The matrix must have equal number of columns on each line!");
            }
        }
    }

    private void initStructure(int[][] content) {
        lines = content.length;
        columns = content[0].length;

        int NNZ = 0;

        for (int[] line : content) {
            for (int value : line) {
                if (value != 0) {
                    NNZ++;
                }
            }
        }

        values = new TIntArrayList(NNZ);
        valuesRow = new TIntArrayList(NNZ + 1);
        valuesColumn = new TIntArrayList(NNZ);

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < columns; j++) {
                if (content[i][j] != 0) {
                    values.add(content[i][j]);
                    valuesRow.add(i);
                    valuesColumn.add(j);
                }
            }
        }

        values.add(lines + 1);
    }

    public static class Builder {
        private final TIntList values = new TIntArrayList();
        private final TIntList valuesRow = new TIntArrayList();
        private final TIntList valuesColumn = new TIntArrayList();
        private final int lines, columns;

        private int currentRow = 0, currentColumn = -1;

        public Builder(int lines, int columns) {
            this.lines = lines;
            this.columns = columns;
        }

        public Builder addRow(int[] rowValues) {
            if (rowValues.length != columns) {
                throw new IllegalArgumentException("The matrix must have columns with size " + columns);
            }

            if(currentColumn != 0){
                currentRow ++;
                currentRow = 0;
            }

            for (int i = 0; i < columns; i++) {
                if (rowValues[i] != 0) {
                    values.add(rowValues[i]);
                    valuesRow.add(currentRow);
                    valuesColumn.add(i);
                }
            }
            currentRow++;
            return this;
        }

        public Builder addElem(int val) {
            if (currentColumn == columns - 1) {
                currentColumn = 0;
                currentRow++;
            } else {
                currentColumn++;
            }

            if(val != 0){
                values.add(val);
                valuesRow.add(currentRow);
                valuesColumn.add(currentColumn);
            }

            return this;
        }

        public Builder fillZeroes(int count){
            currentRow += count / columns;
            currentColumn += count % columns;
            if(currentColumn > columns){
                currentRow++;
                currentColumn -= columns;
            }
            return this;
        }

        public CoordinateListMatrix build(){
            return new CoordinateListMatrix(this);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < lines; i++) {
            builder.append("\n{");
            for (int j = 0; j < columns - 1; j++) {
                builder.append(getAt(i, j)).append(", ");
            }
            builder.append(getAt(i, columns - 1));
            builder.append("}");
        }
        return builder.toString();
    }
}
