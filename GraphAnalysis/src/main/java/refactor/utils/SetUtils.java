package refactor.utils;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.util.BitSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Florin on 26-May-16.
 */
public class SetUtils {

    private SetUtils() {
    }

    public static List<TIntSet> removeDouplicates(List<TIntSet> sets) {

        return null;//todo
    }

    public static TIntSet union(TIntSet first, TIntSet second) {
        TIntSet result = new TIntHashSet(first);

        for (TIntIterator it = second.iterator(); it.hasNext(); ) {
            int nextVal = it.next();
            result.add(nextVal);
        }

        return result;
    }

    public static void unionWith(TIntSet source, TIntSet target) {
        for (TIntIterator it = target.iterator(); it.hasNext(); ) {
            int nextVal = it.next();
            source.add(nextVal);
        }
    }

    public static void cleanDuplicates(List<TIntSet> secondCliques) {
        BitSet visitedSet = new BitSet();

        for (Iterator<TIntSet> iterator = secondCliques.iterator(); iterator.hasNext(); ) {
            TIntSet cluster = iterator.next();
            TIntIterator clusteIter = cluster.iterator();
            int node = clusteIter.next();

            if (visitedSet.get(node)) {
                iterator.remove();
            } else {
                visitedSet.set(node);
                while (clusteIter.hasNext()) {
                    int newNode = clusteIter.next();
                    visitedSet.set(newNode);
                }
            }
        }
    }

    public static TIntSet intersection(TIntSet first, TIntSet second) {
        TIntSet result = new TIntHashSet();

        for (TIntIterator iterator = second.iterator(); iterator.hasNext(); ) {
            int nextVal = iterator.next();
            if (first.contains(nextVal)) {
                result.add(nextVal);
            }
        }

        return result;
    }

    public static void intersectWith(TIntSet source, TIntSet target) {

        for (TIntIterator it = source.iterator(); it.hasNext(); ) {
            int val = it.next();
            if (target.contains(val)) {
                target.remove(val);
            }
        }
    }

    public static int intersectionSize(TIntSet first, TIntSet second) {
        int result = 0;

        for (TIntIterator iterator = second.iterator(); iterator.hasNext(); ) {
            int nextVal = iterator.next();
            if (first.contains(nextVal)) {
                result++;
            }
        }

        return result;
    }



    public static boolean areClustersAdjacent(TIntSet firstClique, TIntSet secondClique) {
        int minSize = firstClique.size() > secondClique.size() ? secondClique.size() : firstClique.size();

        if (minSize < 2) return false;

        return intersectionSize(firstClique, secondClique) == minSize - 1;
    }

    public static boolean areClustersFuzzyAdjacent(TIntSet firstClique, TIntSet secondClique, double credibility) {
        if (credibility < 0 || credibility > 1) {
            throw new IllegalArgumentException("Credibility should be between 0 and 1!");
        }

        int minSize = firstClique.size() > secondClique.size() ? secondClique.size() : firstClique.size();
        int intersections = 0;

        if (minSize < 2) return false;

        for (TIntIterator iterator = secondClique.iterator(); iterator.hasNext(); ) {
            int nextVal = iterator.next();
            if (firstClique.contains(nextVal)) {
                intersections++;
            }

            if (intersections >= minSize - 1 || intersections > minSize * credibility) {
                return true;
            }
        }

        return false;
    }
}
