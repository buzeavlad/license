package refactor.utils;

import gnu.trove.iterator.TDoubleIterator;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import gnu.trove.set.TIntSet;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Florin on 22-May-16.
 */
public class StatisticsUtils {
    private StatisticsUtils() {
    }

    public static double computeAverage(TIntList values) {
        int size = values.size();
        double sum = 0d;

        for (TIntIterator iterator = values.iterator(); iterator.hasNext(); ) {
            int val = iterator.next();
            sum += val;
        }

        return sum / size;
    }

    public static double computeAverage(TDoubleList values) {
        int size = values.size();
        double sum = 0d;

        for (TDoubleIterator iterator = values.iterator(); iterator.hasNext(); ) {
            double val = iterator.next();
            sum += val;
        }

        return sum / size;
    }

    public static double computeAverageSize(List<TIntSet> clusters) {
        int noClusters = clusters.size();
        double sumSizes = 0d;

        for (Iterator<TIntSet> setIterator = clusters.iterator(); setIterator.hasNext(); ) {
            TIntSet cluster = setIterator.next();
            sumSizes += cluster.size();
        }

        return sumSizes / noClusters;
    }

    public static double computeAverageListSize(List<TIntList> lists) {
        int noLists = lists.size();
        double sumSizes = 0d;

        for (Iterator<TIntList> setIterator = lists.iterator(); setIterator.hasNext(); ) {
            TIntList cluster = setIterator.next();
            sumSizes += cluster.size();
        }

        return sumSizes / noLists;
    }

    public static int getMaxSetSize(List<TIntSet> lists) {
        return lists.stream().max(new Comparator<TIntSet>() {
            @Override
            public int compare(TIntSet set1, TIntSet set2) {
                return set1.size() - set2.size();
            }
        }).get().size();
    }

    public static double getVariance(TIntList values) {
        double mean = computeAverage(values);
        int size = values.size();
        double sumVar = 0;

        for (TIntIterator iterator = values.iterator(); iterator.hasNext(); ) {
            int val = iterator.next();
            sumVar += (val - mean) * (val - mean);
        }

        return sumVar / size;
    }

    public static double getEntropy(List<TIntSet> clustering, int totalNodes) {
        double entropy = 0;
        double Pi = 0;

        for (TIntSet cluster : clustering) {
            Pi = ((double) cluster.size()) / totalNodes;
            entropy -= Pi * Math.log(Pi);
        }

        return entropy;
    }

    public static double getMutualInfo(List<TIntSet> firstClustering, List<TIntSet> secondClustering, int totalNodes) {
        double mutualInfo = 0;
        double Pi = 0, Pj = 0, Pij = 0;
        double sizeIJ = 0;

        for (TIntSet setI : firstClustering) {
            Pi = ((double) setI.size()) / totalNodes;
            for (TIntSet setJ : secondClustering) {
                sizeIJ = SetUtils.intersectionSize(setI, setJ);
                Pj = ((double) setJ.size()) / totalNodes;

                if (sizeIJ != 0) {
                    Pij = sizeIJ / totalNodes;
                    mutualInfo += Pij * Math.log(Pij / (Pi * Pj));
                }
            }
        }

        return mutualInfo;
    }
}
