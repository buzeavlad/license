package refactor;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import lombok.EqualsAndHashCode;
import refactor.utils.SparseMatrix;

public class HashSparseMatrix implements SparseMatrix{
    private TObjectIntMap<IntIntPair> matrix = new TObjectIntHashMap<>();
    private IntIntPair reusableKey = new IntIntPair(-1, -1);

    @Override
    public int getAt(int line, int column) {
        reusableKey.from = line;
        reusableKey.to = column;
        return matrix.get(reusableKey);
    }

    @Override
    public int setAt(int line, int column, int val) {
        reusableKey.from = line;
        reusableKey.to = column;

        return matrix.put(reusableKey.clone(), val);
    }

    @EqualsAndHashCode
    private static class IntIntPair{
        private int from;
        private int to;

        public IntIntPair(int from, int to) {
            this.from = from;
            this.to = to;
        }

        @Override
        @SuppressWarnings("all")
        public IntIntPair clone(){
            return new IntIntPair(from, to);
        }
    }
}
