package refactor;

import refactor.utils.SparseMatrix;

/**
 * Created by Florin on 02-May-16.
 */
public class StanfordGraph extends Graph {
    private static final int EDGE_VALUE = 1;
    private static final int NO_EDGE_VALUE = 0;

    public StanfordGraph(int size, SparseMatrix edges) {
        super(size, edges);
    }

    @Override
    public int setEdge(int from, int to, int value) {
        if(value != EDGE_VALUE && value != NO_EDGE_VALUE){
            return noWeightedEdgeException();
        }
        return super.setEdge(from, to, value);
    }

    @Override
    public int addVal(int from, int to, int addition) {
        return noWeightedEdgeException();
    }

    private int noWeightedEdgeException() throws RuntimeException{
        throw new UnsupportedOperationException("In a Stanford graph the edges are unweighted!");
    }
}
