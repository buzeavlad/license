package refactor.utils;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
/**
 * Created by Florin on 04-Apr-16.
 */
public class OldYaleIntMatrixTest {
    private OldYaleIntMatrix actual;
    private int[][] expected;

    @Test
    public void testSimpleMatrix() {
        expected = new int[][]{
                {0, 0, 0, 0},
                {5, 8, 0, 0},
                {0, 0, 3, 0},
                {0, 6, 0, 0}
        };

        actual = new OldYaleIntMatrix(expected);
        assertEquals(actual, expected);
    }

    @Test
    public void classNameMatchesTest() {
        assertThat(OldYaleIntMatrix.class.getName() + "Test",
                equalTo(OldYaleIntMatrixTest.class.getName()));
    }

    private void assertEquals(OldYaleIntMatrix actual, int[][] expected) {
        assertThat(actual.getNrLines(), equalTo(expected.length));
        assertThat(actual.getNrColumns(), equalTo(expected[0].length));

        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                assertThat(actual.getAt(i, j), equalTo(expected[i][j]));
            }
        }
    }
}
