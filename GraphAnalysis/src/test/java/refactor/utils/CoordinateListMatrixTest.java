package refactor.utils;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by Florin on 06-Apr-16.
 */
public class CoordinateListMatrixTest {
    private CoordinateListMatrix actual;
    private int[][] expected1, expected2;

    @Test
    public void testSimpleMatrixConversion() {
        expected1 = new int[][]{
                {0, 0, 0, 0},
                {5, 8, 0, 0},
                {0, 0, 3, 0},
                {0, 6, 0, 0}
        };


        actual = new CoordinateListMatrix(expected1);
        assertEquals(actual, expected1);
    }

    @Test
    public void testSetMatrix(){
        expected1 = new int[][]{
                {0, 0, 0, 0},
                {5, 8, 0, 0},
                {0, 0, 3, 0},
                {0, 6, 0, 0}
        };

        actual = new CoordinateListMatrix(expected1);
        assertEquals(actual, expected1);

        expected2 = expected1;

        expected1 = new int[][] {
                {1, 1, 1, 1},
                {5, 8, 0, 1},
                {2, 0, 0, 1},
                {1, 0, 1, 0}
        };

        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                int oldVal = actual.setAt(expected1[i][j], i, j);
                assertThat(oldVal, equalTo(expected2[i][j]));
            }
        }

        assertEquals(actual, expected1);
    }

    @Test
    public void classNameMatchesTest() {
        assertThat(CoordinateListMatrix.class.getName() + "Test",
                equalTo(CoordinateListMatrixTest.class.getName()));
    }

    private void assertEquals(CoordinateListMatrix actual, int[][] expected) {
        assertThat(actual.getNrLines(), equalTo(expected.length));
        assertThat(actual.getNrColumns(), equalTo(expected[0].length));

        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                assertThat(actual.getAt(i, j), equalTo(expected[i][j]));
            }
        }
    }
}
