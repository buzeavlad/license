package refactor;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import refactor.utils.CoordinateListMatrix;
import refactor.utils.SparseMatrix;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Florin on 14-May-16.
 */
public class PrimitiveDijkstraTest {
    private PrimitiveDijkstra strategy;
    private StanfordGraph graph;

    @Before
    public void buildUp(){
        strategy = new PrimitiveDijkstra();
    }

    @Test
    public void classNameMatchesTest(){
        assertThat(PrimitiveDijkstra.class.getName() + "Test",
                equalTo(PrimitiveDijkstraTest.class.getName()));
    }

    @Test
    public void simpleGraphTest(){
        TIntList nodes = new TIntArrayList(3);
        nodes.add(1);
        nodes.add(2);
        nodes.add(3);
        SparseMatrix sparseMatrix =
                new CoordinateListMatrix.Builder(3, 3)
                        .fillZeroes(1).addElem(1).addElem(4)
                        .addElem(1).fillZeroes(1).addElem(2)
                        .addElem(4).addElem(2).fillZeroes(0).build();
        graph = new StanfordGraph(3, sparseMatrix);

        strategy.setGraph(graph);

        Pair<TIntList, TIntList> distanceParrentPair = strategy.computeDistances(0);
        TIntList distances = distanceParrentPair.getKey();
        TIntList parrents = distanceParrentPair.getValue();

        assertEquals(1, distances.get(1));
        assertEquals(3, distances.get(2));

        assertEquals(0, parrents.get(1));
        assertEquals(1, parrents.get(2));
    }
}
