package refactor.metrics;

import gnu.trove.list.TDoubleList;
import org.junit.Before;
import org.junit.Test;
import refactor.Graph;
import refactor.HashSparseMatrix;
import refactor.StanfordGraph;
import refactor.utils.SparseMatrix;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Florin on 20-May-16.
 */
public class BetweenessCentralityTest {
    public static final double DELTA = 0.0001;

    private BetweenessCentrality centralityMetric;
    private SparseMatrix graphMatrix;

    @Before
    public void setup(){
        centralityMetric = new BetweenessCentrality();
        graphMatrix = new HashSparseMatrix();
    }

    @Test
    public void simpleCentrality(){
        graphMatrix.setAt(0, 1, 1); graphMatrix.setAt(1, 0, 1);
        graphMatrix.setAt(1, 2, 1); graphMatrix.setAt(2, 1, 1);
        graphMatrix.setAt(2, 3, 1); graphMatrix.setAt(3, 2, 1);
        graphMatrix.setAt(2, 4, 1); graphMatrix.setAt(4, 2, 1);
        graphMatrix.setAt(3, 4, 1); graphMatrix.setAt(4, 3, 1);  //    1 - - 2 -- 3 - |
        graphMatrix.setAt(3, 5, 1); graphMatrix.setAt(5, 3, 1);  //    |     |    |   5
        graphMatrix.setAt(3, 5, 1); graphMatrix.setAt(5, 3, 1);  //    0     - -- 4 - |
        graphMatrix.setAt(3, 5, 1); graphMatrix.setAt(5, 3, 1);
        graphMatrix.setAt(4, 5, 1); graphMatrix.setAt(5, 4, 1);
        Graph graph = new StanfordGraph(6, graphMatrix);

        TDoubleList result = centralityMetric.getCentralities(graph);

        assertCentralities(result, 0d, 4d, 6d, 1.5d, 1.5d, 0);
    }

    @Test
    public void classNameMatchesTest() {
        assertThat(BetweenessCentrality.class.getName() + "Test",
                equalTo(BetweenessCentralityTest.class.getName()));
    }

    private void assertCentralities(TDoubleList actual, double... expected) {
        assertEquals(expected.length, actual.size());

        for(int i = 0; i < expected.length; i++){
            assertEquals(expected[i], actual.get(i),  DELTA);
        }
    }
}
