package strategies;

import gnu.trove.list.array.TIntArrayList;
import graphmodel.GraphEdge;
import graphmodel.WeightedGraph;
import graphmodel.WeightedGraphEdge;
import graphmodel.WeightedGraphNode;
import javafx.util.Pair;
import lombok.RequiredArgsConstructor;
import strategies.CentralityStrategy;
import strategies.CommunityDetectionStrategy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Florin on 13-Mar-16.
 */
public class GraphTest {
    public static class DummyGraph implements WeightedGraph<DummyGraphNode> {
        private List<DummyGraphNode> nodes = new ArrayList<>();
        private List<GraphEdge<DummyGraphNode>> edges = new ArrayList<>();

        @Override
        public List<Set<DummyGraphNode>> findCommunities(CommunityDetectionStrategy strategy) {
            throw new UnsupportedOperationException("Dummy graph has no strategy");
        }

        @Override
        public TIntArrayList getCentralities(CentralityStrategy strategy) {
            throw new UnsupportedOperationException("Dummy graph has no strategy");
        }

        @Override
        public int getSize() {
            return nodes.size();
        }

        @Override
        public List<DummyGraphNode> getNodes() {
            return nodes;
        }

        @Override
        public List<GraphEdge<DummyGraphNode>> getEdges() {
            return edges;
        }

        public void addNode(DummyGraphNode graphNode){
            nodes.add(graphNode);
        }

        public void addEdge(DummyGraphEdge edge){
            edge.getNodesInvolved().getKey().addEdge(edge);
            edge.getNodesInvolved().getValue().addEdge(edge);
        }
    }

    @RequiredArgsConstructor
    public static class DummyGraphNode implements WeightedGraphNode {
        private final Set<GraphEdge> edges = new HashSet<>();
        private final String name;

        @Override
        public Set<GraphEdge> getEdges() {
            return edges;
        }

        public void addEdge(WeightedGraphEdge edge){
            edges.add(edge);
        }

        public String getName() {
            return name;
        }
    }

    @RequiredArgsConstructor
    public static class DummyGraphEdge implements WeightedGraphEdge<DummyGraphNode>{
        private final Pair<DummyGraphNode, DummyGraphNode> endNodes;
        private final int weight;

        @Override
        public int getWeight() {
            return weight;
        }

        @Override
        public Pair<DummyGraphNode, DummyGraphNode> getNodesInvolved() {
            return endNodes;
        }
    }
}
