package strategies;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by Florin on 13-Mar-16.
 */
public class OneSourceToCompleteStratAdapterTest {


    private GraphTest.DummyGraph dummyGraph;
    private OneSourceShortestPathStrat<GraphTest.DummyGraphNode> strategy;
    private CompleteShortestPathStrategy completeShortestPathStrategy;

    @Before
    public void buildUp(){
        dummyGraph = new GraphTest.DummyGraph();
        strategy = new DijkstraDistanceStrat<>(GraphTest.DummyGraphNode.class);
        completeShortestPathStrategy = new OneSourceToCompleteStratAdapter<>(
                strategy,
                GraphTest.DummyGraphNode.class
        );
    }


    @Test
    public void simpleGraphTest() {
        GraphTest.DummyGraphNode graphNode1 = new GraphTest.DummyGraphNode("Node1");
        GraphTest.DummyGraphNode graphNode2 = new GraphTest.DummyGraphNode("Node2");
        GraphTest.DummyGraphNode graphNode3 = new GraphTest.DummyGraphNode("Node3");

        GraphTest.DummyGraphEdge edge12 = new GraphTest.DummyGraphEdge(
                new Pair<>(graphNode1, graphNode2),
                1
        );

        GraphTest.DummyGraphEdge edge23 = new GraphTest.DummyGraphEdge(
                new Pair<>(graphNode2, graphNode3),
                2
        );

        GraphTest.DummyGraphEdge edge13 = new GraphTest.DummyGraphEdge(
                new Pair<>(graphNode1, graphNode3),
                4
        );

        graphNode1.addEdge(edge12);
        graphNode1.addEdge(edge13);
        graphNode2.addEdge(edge23);
        graphNode2.addEdge(edge12);
        graphNode3.addEdge(edge13);
        graphNode3.addEdge(edge23);

        addEdges(edge12, edge13, edge23);
        addNodes(graphNode1, graphNode2, graphNode3);

        completeShortestPathStrategy.computeShortestPaths(dummyGraph);

        assertThat(1, equalTo(completeShortestPathStrategy.getDistance(graphNode1, graphNode2)));
        assertThat(3, equalTo(completeShortestPathStrategy.getDistance(graphNode1, graphNode3)));

        assertThat(2, equalTo(completeShortestPathStrategy.getDistance(graphNode2, graphNode3)));
        assertThat(1, equalTo(completeShortestPathStrategy.getDistance(graphNode2, graphNode1)));

        assertThat(2, equalTo(completeShortestPathStrategy.getDistance(graphNode3, graphNode2)));
        assertThat(3, equalTo(completeShortestPathStrategy.getDistance(graphNode3, graphNode1)));

        assertThat(graphNode1, equalTo(completeShortestPathStrategy.getParrent(graphNode1, graphNode2)));
        assertThat(graphNode2, equalTo(completeShortestPathStrategy.getParrent(graphNode1, graphNode3)));

        assertThat(graphNode2, equalTo(completeShortestPathStrategy.getParrent(graphNode2, graphNode3)));
        assertThat(graphNode2, equalTo(completeShortestPathStrategy.getParrent(graphNode2, graphNode1)));

        assertThat(graphNode2, equalTo(completeShortestPathStrategy.getParrent(graphNode3, graphNode1)));
        assertThat(graphNode3, equalTo(completeShortestPathStrategy.getParrent(graphNode3, graphNode2)));

        assertThat(graphNode1, equalTo(completeShortestPathStrategy.getParrent(graphNode1, graphNode1)));
        assertThat(graphNode2, equalTo(completeShortestPathStrategy.getParrent(graphNode2, graphNode2)));
        assertThat(graphNode3, equalTo(completeShortestPathStrategy.getParrent(graphNode3, graphNode3)));
    }

    @Test
    public void classNameMatchesTest(){
        assertThat(OneSourceToCompleteStratAdapter.class.getName()+ "Test",
                equalTo(OneSourceToCompleteStratAdapterTest.class.getName()));
    }

    private void addEdges(GraphTest.DummyGraphEdge... edges){
        for(GraphTest.DummyGraphEdge graphEdge : edges){
            dummyGraph.addEdge(graphEdge);
        }
    }

    private void addNodes(GraphTest.DummyGraphNode... nodes){
        for (GraphTest.DummyGraphNode graphNode : nodes) {
            dummyGraph.addNode(graphNode);
        }
    }
}
