package strategies;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import strategies.GraphTest.DummyGraph;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Florin on 28-Feb-16.
 */
public class DijkstraDistanceStratTest {

    private DummyGraph dummyGraph;
    private DijkstraDistanceStrat<GraphTest.DummyGraphNode> strategy;

    @Before
    public void buildUp(){
        dummyGraph = new DummyGraph();
        strategy = new DijkstraDistanceStrat<>(GraphTest.DummyGraphNode.class);
    }

    @Test
    public void classNameMatchesTest(){
        assertThat(DijkstraDistanceStrat.class.getName() + "Test",
                equalTo(DijkstraDistanceStratTest.class.getName()));
    }

    @Test
    public void simpleGraphTest(){
        GraphTest.DummyGraphNode graphNode1 = new GraphTest.DummyGraphNode("Node1");
        GraphTest.DummyGraphNode graphNode2 = new GraphTest.DummyGraphNode("Node2");
        GraphTest.DummyGraphNode graphNode3 = new GraphTest.DummyGraphNode("Node3");

        GraphTest.DummyGraphEdge edge12 = new GraphTest.DummyGraphEdge(
                new Pair<>(graphNode1, graphNode2),
                1
        );

        GraphTest.DummyGraphEdge edge23 = new GraphTest.DummyGraphEdge(
                new Pair<>(graphNode2, graphNode3),
                2
        );

        GraphTest.DummyGraphEdge edge13 = new GraphTest.DummyGraphEdge(
                new Pair<>(graphNode1, graphNode3),
                4
        );

        graphNode1.addEdge(edge12);
        graphNode1.addEdge(edge13);
        graphNode2.addEdge(edge23);
        graphNode2.addEdge(edge12);
        graphNode3.addEdge(edge13);
        graphNode3.addEdge(edge23);

        addEdges(edge12, edge13, edge23);
        addNodes(graphNode1, graphNode2, graphNode3);

        strategy.setSource(graphNode1);

        assertEquals(graphNode1, strategy.getSource());
        strategy.computeDistances(dummyGraph);

        assertEquals(1, strategy.getDistance(graphNode2));
        assertEquals(3, strategy.getDistance(graphNode3));

        assertEquals(graphNode1, strategy.getParrent(graphNode2));
        assertEquals(graphNode2, strategy.getParrent(graphNode3));
    }

    private void addEdges(GraphTest.DummyGraphEdge... edges){
        for(GraphTest.DummyGraphEdge graphEdge : edges){
            dummyGraph.addEdge(graphEdge);
        }
    }

    private void addNodes(GraphTest.DummyGraphNode... nodes){
        for (GraphTest.DummyGraphNode graphNode : nodes) {
            dummyGraph.addNode(graphNode);
        }
    }
}
