package eu.buzea.datumbox.server;

public class ServerStart {

	public static void main(String[] args) {
		DatumboxServer datumboxServer = new DatumboxServer();
		datumboxServer.start();
	}
}
