package eu.buzea.datumbox.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.datumbox.common.dataobjects.Record;

import eu.buzea.datumbox.server.DatumboxServer;

public class DatumboxClient {
	
	public enum Sentiment{
		POSITIVE,NEGATIVE,NEUTRAL
	}

	public Sentiment sentimentAnalysis(String text) {
		
		try {
			Socket socket = new Socket("localhost", DatumboxServer.PORT);
			System.out.println("Connecting...");
			ObjectOutputStream outToServer = new ObjectOutputStream(socket.getOutputStream());
			outToServer.writeObject(text);
			ObjectInputStream inFromServer = new ObjectInputStream(socket.getInputStream());
			
			
			
			Record r = (Record) inFromServer.readObject();
			
			outToServer.close();
			inFromServer.close();
			socket.close();
			
			String sentiment = (String) r.getYPredicted();
			switch (sentiment) {
			case "negative":
				return Sentiment.NEGATIVE;
			case "positive":
				return Sentiment.POSITIVE;

			default:
				return Sentiment.NEUTRAL;
			}
			
			
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
