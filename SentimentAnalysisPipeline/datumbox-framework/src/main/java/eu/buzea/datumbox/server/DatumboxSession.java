package eu.buzea.datumbox.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.datumbox.applications.nlp.TextClassifier;
import com.datumbox.common.dataobjects.Record;

public class DatumboxSession extends Thread {

	private Socket socket;
	private TextClassifier classifier;

	public DatumboxSession(Socket socket, TextClassifier classifier) {
		super();
		this.socket = socket;
		this.classifier = classifier;
	}

	
	@Override
	public void run() {
		super.run();
		try {
			handleRequest();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void handleRequest() throws IOException, ClassNotFoundException {
		
			ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
			String sentance = (String) objectInputStream.readObject();
			Record r = classifier.predict(sentance);
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(r);
			
			socket.getInputStream().close();
			out.close();
			socket.close();

	}

}
