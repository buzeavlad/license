package eu.buzea.datumbox.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.datumbox.applications.nlp.TextClassifier;
import com.datumbox.common.persistentstorage.ConfigurationFactory;
import com.datumbox.common.persistentstorage.interfaces.DatabaseConfiguration;
import com.datumbox.common.utilities.RandomGenerator;
import com.datumbox.framework.machinelearning.classification.MultinomialNaiveBayes;
import com.datumbox.framework.machinelearning.common.bases.mlmodels.BaseMLmodel;
import com.datumbox.framework.machinelearning.featureselection.categorical.ChisquareSelect;
import com.datumbox.framework.utilities.text.extractors.NgramsExtractor;

public class DatumboxServer extends Thread {

	private TextClassifier classifier;
	private ServerSocket serverSocket;
	public static final int PORT = 8891;

	@Override
	public void run() {

		try {

			setUp();
			System.out.println("Server ready");
			serverSocket = new ServerSocket(PORT);
			while (true) {
				Socket clientSocket;
				clientSocket = serverSocket.accept();
				DatumboxSession cThread = new DatumboxSession(clientSocket, classifier);
				cThread.start();

			}

		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}

		/*
		 * String sentence = "Datumbox sucks"; Record r =
		 * classifier.predict(sentence); System.out.println(
		 * "Classifing sentence: \"" + sentence + "\""); System.out.println(
		 * "Predicted class: " + r.getYPredicted());
		 */
	}

	private void setUp() throws IOException, URISyntaxException {

		/**
		 * There are two configuration files in the resources folder:
		 * 
		 * - datumbox.config.properties: It contains the configuration for the
		 * storage engines (required) - logback.xml: It contains the
		 * configuration file for the logger (optional)
		 */

		// Initialization
		// --------------
		RandomGenerator.setGlobalSeed(42L); // optionally set a specific seed
											// for all Random objects
		DatabaseConfiguration dbConf = ConfigurationFactory.INMEMORY.getConfiguration(); // in-memory
																							// maps
		// DatabaseConfiguration dbConf =
		// ConfigurationFactory.MAPDB.getConfiguration(); //mapdb maps

		// Reading Data
		// ------------
		Map<Object, URI> dataset = new HashMap<>(); // The examples of each
													// category are stored on
													// the same file, one
													// example per row.

		dataset.put("positive",
				getClass().getClassLoader().getResource("datasets/sentiment-analysis/rt-polarity.pos").toURI());
		dataset.put("negative",
				getClass().getClassLoader().getResource("datasets/sentiment-analysis/rt-polarity.neg").toURI());

		// Setup Training Parameters
		// -------------------------
		TextClassifier.TrainingParameters trainingParameters = new TextClassifier.TrainingParameters();

		// Classifier configuration
		trainingParameters.setMLmodelClass(MultinomialNaiveBayes.class);
		trainingParameters.setMLmodelTrainingParameters(new MultinomialNaiveBayes.TrainingParameters());

		// Set data transfomation configuration
		trainingParameters.setDataTransformerClass(null);
		trainingParameters.setDataTransformerTrainingParameters(null);

		// Set feature selection configuration
		trainingParameters.setFeatureSelectionClass(ChisquareSelect.class);
		trainingParameters.setFeatureSelectionTrainingParameters(new ChisquareSelect.TrainingParameters());

		// Set text extraction configuration
		trainingParameters.setTextExtractorClass(NgramsExtractor.class);
		trainingParameters.setTextExtractorParameters(new NgramsExtractor.Parameters());

		classifier = new TextClassifier("SentimentAnalysis", dbConf);
		classifier.fit(dataset, trainingParameters);

		// Use the classifier
		// ------------------

		// Get validation metrics on the training set
		BaseMLmodel.ValidationMetrics vm = classifier.validate(dataset);
		classifier.setValidationMetrics(vm); // store them in the model for
												// future reference
	}

}
