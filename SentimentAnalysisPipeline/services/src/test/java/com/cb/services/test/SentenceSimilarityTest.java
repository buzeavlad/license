package com.cb.services.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.cb.services.topicsimilarity.SentenceSimilarity;

/**
 * Test class for Topic Similarity
 * Process-based approach
 * 
 * @author Ciprian Budusan
 *
 */
public class SentenceSimilarityTest {

	public static final String FIRST_TEXT = "Will this be a game-changer for the Brexit campaign. The REAL figures on immigration into the UK? To stop the madness, there is but one solution. Vote OUT.";
	public static final String SECOND_TEXT = "You seem surprisingly confident mr McCormick, surprising, especially as it is those older voters who remember what national sovereignty used to mean, prior to the rope of the EU. Those older voters are also statistically the most likely to come out and vote.";

	private static final String FILENAME = "src/test/resources/corpus.txt";

	@Test
	public void testProcess() {
		SentenceSimilarity sim = new SentenceSimilarity();

		// double result = sim.computeSimilarity(FIRST_TEXT, SECOND_TEXT);
		// System.out.println(String.format("%.3f", result));

		List<String> texts = new ArrayList<String>();
		// read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(FILENAME))) {
			texts = stream.collect(Collectors.toList());
			// stream.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String firstText : texts)
			for (String secondText : texts) {
				double res = sim.computeSimilarity(firstText, secondText);
				System.out.println(firstText + "\t " + secondText + "\t "
						+ String.format("%.3f", res));
			}
	}
}
