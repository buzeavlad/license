package com.cb.services.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.cb.services.topicsimilarity.TopicSimilarityClient;

/**
 * Test class for Topic Similarity Socket-based approach
 * 
 * @author Ciprian Budusan
 *
 */
public class TopicSimilarityClientTest {
	public static final String FIRST_TEXT = "Red alcoholic drink";
	public static final String SECOND_TEXT = "A bottle of wine";

	private static final String CORPUS_FILENAME = "src/test/resources/corpus.txt";
	private static final String MICROSOFT_CORPUS = "src/test/resources/msr_paraphrase_test.txt";
	private static final String SENTENCES_FILENAME = "src/test/resources/sentences.txt";

	private static final double THRESHOLD = 0.35;

	@Test
	public void testCorpus() {
		// double result = sim.computeSimilarity(FIRST_TEXT, SECOND_TEXT);
		// System.out.println(String.format("%.3f", result));

		List<String> texts = new ArrayList<String>();
		// read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(CORPUS_FILENAME))) {
			texts = stream.collect(Collectors.toList());
			// stream.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String firstText : texts)
			for (String secondText : texts) {
				TopicSimilarityClient sim = new TopicSimilarityClient();
				double res = sim.computeSimilarity(firstText, secondText);
				System.out.println(
						firstText + "\t " + secondText + "\t " + String.format("%.3f", res));
			}
	}

	@Test
	public void testSentences() {
		List<String> texts = new ArrayList<String>();
		// read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(SENTENCES_FILENAME))) {
			texts = stream.collect(Collectors.toList());
			// stream.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String text : texts) {
			String[] sentences = text.split("\\t+");
			TopicSimilarityClient sim = new TopicSimilarityClient();
			double res = sim.computeSimilarity(sentences[0], sentences[1]);
			System.out.println(
					sentences[0] + "\t " + sentences[1] + "\t " + String.format("%.3f", res));
		}
	}

	@Test
	public void testMicrosoftCorpus() {
		int samples = 0;
		int correct = 0;
		List<String> texts = new ArrayList<String>();
		// read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(MICROSOFT_CORPUS))) {
			texts = stream.collect(Collectors.toList());
			// stream.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 1; i < texts.size(); i++) {
			String[] sentences = texts.get(i).split("\\t");
			TopicSimilarityClient sim = new TopicSimilarityClient();
			double res = sim.computeSimilarity(sentences[3], sentences[4]);
			samples++;
			int result = 0;
			if (res > THRESHOLD)
				result = 1;
			if (result == Integer.parseInt(sentences[0]))
				correct++;
			System.out.println(sentences[3] + "\t " + sentences[4] + "\t "
					+ String.format("%.3f", res) + "\t " + sentences[0]);
			// System.out.println(String.format("%.3f", res) + "\t " +
			// sentences[0]);
		}
		double accuracy = (double) correct / samples;
		System.out.println("Accuracy: " + accuracy);
	}
}
