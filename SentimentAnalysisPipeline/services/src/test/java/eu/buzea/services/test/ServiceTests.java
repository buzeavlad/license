package eu.buzea.services.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.MalformedURLException;

import org.junit.Test;

import com.cb.services.alchemy.AlchemyImageTaggingService;
import com.cb.services.alchemy.AlchemySentimentAnalysisService;
import com.cb.services.languagetool.LanguageToolAPI;
import com.cb.services.monkeylearn.MonkeyLearnSentimentAnalysisService;

import eu.buzea.services.ImageTaggingService;
import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.alyen.AlyenSentimentAnalysisService;
import eu.buzea.services.datumbox.DatumboxSentimentAnalysisService;
import eu.buzea.services.heavenondemand.HeavenOnDemandSentimentAnalysisService;
import eu.buzea.services.indico.IndicoSentimentAnalysisService;
import eu.buzea.services.sentigem.SentigemSentimentAnalysisService;
import eu.buzea.services.vivekn.ViveknSentimentAnalysisService;

public class ServiceTests {

	/**
	 * String used for testing SentimentAnalysisServices
	 */
	private static final String TV_IS_AWESOME = "TV is awesome";

	@Test
	public void testAlchemyApiImagetagging() throws MalformedURLException {
		ImageTaggingService alchemy = new AlchemyImageTaggingService();
		File file = new File("./src/test/resources/alchemy/brian.jpg");
		// File file = new File("./src/test/resources/alchemy/ski.jpg");
		System.out.println("Keywords: " + alchemy.imageTagging(file) + "\nImage Text: " + alchemy.imageText(file));
	}

	@Test
	public void testLanguageToolApi() {
		LanguageToolAPI langTool = new LanguageToolAPI();
		String correctedText = langTool
				.correctText("Paste your own text here and click the 'Check Text' button. "
						+ "Click the colored phrases for details on potential errors. "
						+ "or use this text too see an few of of the problems that LanguageTool can detecd. "
						+ "What do you thinks of grammar checkers? Please not that they are not perfect. "
						+ "Style issues get a blue marker: It's 5 P.M. in the afternoon.");

		System.out.println(correctedText);
	}

	private void checkTv(SentimentAnalysisService service) {
		Sentiment s = service.process(TV_IS_AWESOME);
		assertEquals(Sentiment.POSITIVE, s);
	}

	@Test
	public void testDatumboxService() {
		DatumboxSentimentAnalysisService service = new DatumboxSentimentAnalysisService();
		checkTv(service);
	}

	@Test
	public void testHeavenOnDemandService() {
		HeavenOnDemandSentimentAnalysisService service = new HeavenOnDemandSentimentAnalysisService();
		checkTv(service);
	}

	@Test
	public void testMonkeyLearnService() {
		MonkeyLearnSentimentAnalysisService service = new MonkeyLearnSentimentAnalysisService();
		checkTv(service);
	}

	@Test
	public void testIndicoService() {
		IndicoSentimentAnalysisService service = new IndicoSentimentAnalysisService();
		checkTv(service);

	}

	@Test
	public void testAlyenService() {
		AlyenSentimentAnalysisService service = new AlyenSentimentAnalysisService();
		checkTv(service);
	}

	@Test
	public void testSentigemService() {
		SentimentAnalysisService service = new SentigemSentimentAnalysisService();
		checkTv(service);
	}

	@Test
	public void testViveknService() {
		SentimentAnalysisService service = new ViveknSentimentAnalysisService();
		checkTv(service);
	}

	@Test
	public void testAlchemyApiService() {
		SentimentAnalysisService service = new AlchemySentimentAnalysisService();
		checkTv(service);
	}
}
