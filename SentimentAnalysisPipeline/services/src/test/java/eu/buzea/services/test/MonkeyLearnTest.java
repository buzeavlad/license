package eu.buzea.services.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.cb.services.monkeylearn.MonkeyLearnAPI;

public class MonkeyLearnTest {

	private static final String PROFANITY_STRING = "No fucks given :P";
	private static final String PROFANITY_CLEAN_STRING = "That is a smart answer";
	private static final String SENTIMENT_POSITIVE_STRING = "Dogs are very intelligent animals";

	private static final String KEY = "0c43aadb6bcf6da0ba2ec6b997e8effbba7c0a06";
	private MonkeyLearnAPI monkeyLearnAPI;

	@Before
	public void setUp() {
		monkeyLearnAPI = new MonkeyLearnAPI();
	}

	@Test
	public void testSentimentAnalysis() {
		String label = monkeyLearnAPI.sentimentAnalysis(KEY, SENTIMENT_POSITIVE_STRING);
		assertEquals("positive", label);
	}

	@Test
	public void testProfanityAnalysis() {
		String label = monkeyLearnAPI.profanityAnalysis(KEY, PROFANITY_CLEAN_STRING);
		assertEquals("clean", label);
		label = monkeyLearnAPI.profanityAnalysis(KEY, PROFANITY_STRING);
		assertEquals("profanity", label);

	}

}
