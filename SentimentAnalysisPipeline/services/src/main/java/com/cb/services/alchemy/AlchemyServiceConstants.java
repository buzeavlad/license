/**
 * 
 */
package com.cb.services.alchemy;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class AlchemyServiceConstants {

	/**
	 * Available keys for queries
	 */
	public static final String[] API_KEYS = {"d0ee8578e98bd09294dd9857df14422754fb9dbd",
			"7aac32e006f260dc399ba45cf1a3cabe39a10f59"};

	/**
	 * Avoid instantiation
	 */
	private AlchemyServiceConstants() {
	}

}
