package com.cb.services.languagetool;

import java.io.IOException;

import org.languagetool.JLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.tools.Tools;

import eu.buzea.services.GrammarCheckingService;

/**
 * Spelling and Grammar Correction Service
 * 
 * @author Ciprian Budusan
 *
 */
public class LanguageToolAPI implements GrammarCheckingService {

	JLanguageTool langTool;

	public LanguageToolAPI() {
		langTool = new JLanguageTool(new AmericanEnglish());
	}

	/**
	 * Automatically applies suggestions to the text, as suggested by the rules
	 * that match. 
	 * Note: if there is more than one suggestion, always the first
	 * one is applied, and others are ignored silently.
	 */
	@Override
	public String correctText(String text) {
		String correctedText = null;
		try {
			correctedText = Tools.correctText(text, langTool);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return correctedText;
	}

}
