package com.cb.services.alchemy;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyVision;
import com.ibm.watson.developer_cloud.alchemy.v1.model.ImageKeyword;
import com.ibm.watson.developer_cloud.alchemy.v1.model.ImageKeywords;
import com.ibm.watson.developer_cloud.alchemy.v1.model.ImageSceneText;

import eu.buzea.services.ImageTaggingService;

/**
 * Image Tagging Service
 * 
 * @author Ciprian Budusan
 *
 */
public class AlchemyImageTaggingService implements ImageTaggingService {

	private AlchemyVision service;
	private int keyIndex;

	public AlchemyImageTaggingService() {
		service = new AlchemyVision();
		keyIndex = 0;
	}

	@Override
	public List<String> imageTagging(File file) {
		for (; keyIndex < AlchemyServiceConstants.API_KEYS.length; keyIndex++) {
			try {
				String apiKey = AlchemyServiceConstants.API_KEYS[keyIndex];
				service.setApiKey(apiKey);
				Boolean forceShowAll = false;
				Boolean knowledgeGraph = false;
				// extract keywords from an image
				ImageKeywords response = service
						.getImageKeywords(file, forceShowAll, knowledgeGraph).execute();
				List<ImageKeyword> imageKeywords = response.getImageKeywords();
				List<String> keywords = new ArrayList<String>();
				for (ImageKeyword imageKeyword : imageKeywords) {
					keywords.add(imageKeyword.getText());
				}
				return keywords;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> imageTagging(URL url) {
		for (; keyIndex < AlchemyServiceConstants.API_KEYS.length; keyIndex++) {
			try {
				String apiKey = AlchemyServiceConstants.API_KEYS[keyIndex];
				service.setApiKey(apiKey);
				Boolean forceShowAll = false;
				Boolean knowledgeGraph = false;
				// extract keywords from an image
				ImageKeywords response = service.getImageKeywords(url, forceShowAll, knowledgeGraph)
						.execute();
				List<ImageKeyword> imageKeywords = response.getImageKeywords();
				List<String> keywords = new ArrayList<String>();
				for (ImageKeyword imageKeyword : imageKeywords) {
					keywords.add(imageKeyword.getText());
				}
				return keywords;
			} catch (Exception e) {
			}
		}
		return null;
	}
	
	@Override
	public String imageText(File file) {
		for (; keyIndex < AlchemyServiceConstants.API_KEYS.length; keyIndex++) {
			try {
				String apiKey = AlchemyServiceConstants.API_KEYS[keyIndex];
				service.setApiKey(apiKey);
				// extract text from an image
				ImageSceneText response = service.getImageSceneText(file).execute();
				String text = response.getSceneText();
				return text;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	
	@Override
	public String imageText(URL url) {
		for (; keyIndex < AlchemyServiceConstants.API_KEYS.length; keyIndex++) {
			try {
				String apiKey = AlchemyServiceConstants.API_KEYS[keyIndex];
				service.setApiKey(apiKey);
				// extract text from an image
				ImageSceneText response = service.getImageSceneText(url).execute();
				String text = response.getSceneText();
				return text;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}

}
