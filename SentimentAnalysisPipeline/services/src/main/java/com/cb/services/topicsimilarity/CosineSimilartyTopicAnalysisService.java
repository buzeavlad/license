/**
 * 
 */
package com.cb.services.topicsimilarity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.buzea.common.ExternalServiceCache;
import eu.buzea.services.Sentiment;
import eu.buzea.services.TopicAnalysisService;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class CosineSimilartyTopicAnalysisService implements TopicAnalysisService {

	private ExternalServiceCache cache;
	private static final Logger LOG = LogManager
			.getLogger(CosineSimilartyTopicAnalysisService.class);

	/**
	 * 
	 */
	private static final double SENTENCE_SIMILARITY_THRESHOLD = 0.35;
	private TopicSimilarityClient sim;
	/**
	 * 
	 */
	public CosineSimilartyTopicAnalysisService() {
		cache = new ExternalServiceCache(CosineSimilartyTopicAnalysisService.class);
		sim = new TopicSimilarityClient();
		for (Sentiment s1 : Sentiment.values())
			for (Sentiment s2 : Sentiment.values())
				cache.put(s1.toString().toLowerCase() + s2.toString().toLowerCase(), "1");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSameSubject(String firstText, String secondText) {
		firstText = firstText.toLowerCase();
		secondText = secondText.toLowerCase();
		boolean isCached = cache.contains(firstText + secondText);
		double res = 0;
		if (isCached) {
			String inCache = cache.getResult(firstText + secondText);
			res = Double.parseDouble(inCache);
		} else {
			res = sim.computeSimilarity(firstText, secondText);
			cache.put(firstText + secondText, res + "");
		}
		LOG.info("Sentence similarity:\n(" + firstText + ")\n(" + secondText + ")\n = " + res);
		if (res > SENTENCE_SIMILARITY_THRESHOLD) {
			return true;
		}
		return false;
	}

}
