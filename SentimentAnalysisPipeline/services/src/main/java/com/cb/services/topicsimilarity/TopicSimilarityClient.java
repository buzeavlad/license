package com.cb.services.topicsimilarity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

/**
 * Topic Similarity Service
 * communication with Python Server established through Sockets
 * 
 * @author Ciprian Budusan
 *
 */
public class TopicSimilarityClient {

	// used for splitting the two texts
	private static final String DELIMITER = "<ENDOFDOC>";
	// size of the response, i.e. a double with 3 decimals
	private static final int RES_SIZE = 5;
	private Double result = -1.0;

	public TopicSimilarityClient() {

	}

	public double computeSimilarity(String s1, String s2) {

		try (Socket socket = new Socket("localhost", 2005);
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream());) {

			out.writeBytes(s1 + DELIMITER + s2);
			out.flush();
			byte[] buffer = new byte[RES_SIZE];
			for (int i = 0; i < RES_SIZE; i++) {
				buffer[i] = in.readByte();
			}
			String res = new String(buffer, "UTF-8");
			result = Double.parseDouble(res);

		} catch (ConnectException e) {
			System.out.println("Server Connection Closed!");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
