/**
 * 
 */
package com.cb.services.alchemy;

import java.util.HashMap;
import java.util.Map;

import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyLanguage;
import com.ibm.watson.developer_cloud.alchemy.v1.model.LanguageSelection;

import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.exceptions.ServiceException;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class AlchemySentimentAnalysisService extends SentimentAnalysisService {

	private AlchemyLanguage service;
	private int keyIndex;

	/**
	 * 
	 */
	public AlchemySentimentAnalysisService() {
		service = new AlchemyLanguage();
		service.setLanguage(LanguageSelection.ENGLISH);
		keyIndex = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Sentiment processingCall(String text) {
		for (; keyIndex < AlchemyServiceConstants.API_KEYS.length; keyIndex++) {
			String apiKey = AlchemyServiceConstants.API_KEYS[keyIndex];
			try {
				service.setApiKey(apiKey);
				Map<String, Object> params = new HashMap<>();
				params.put(AlchemyLanguage.TEXT, text);
				// params.put("language", "english");
				String sentimentString = service.getSentiment(params).execute().getSentiment()
						.getType().toString();
				return Sentiment.valueOf(sentimentString);
			} catch (Exception e) {// move on to next key
			}

		}
		throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);

	}

}
