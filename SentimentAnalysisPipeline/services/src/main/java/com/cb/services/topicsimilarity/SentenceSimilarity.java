package com.cb.services.topicsimilarity;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Topic Similarity Service 
 * calling Python module by creating a Process
 * 
 * @author Ciprian Budusan
 *
 */
public class SentenceSimilarity {

	private static final String PYTHON_SCRIPT = "sentence_similarity.py";

	public double computeSimilarity(String s1, String s2) {
		String result = null;
		ProcessBuilder pb = new ProcessBuilder("python", PYTHON_SCRIPT, "" + s1,
				"" + s2);
		try {
			File f = new File(PYTHON_SCRIPT);
			if (f.exists()) {
				Process p = pb.start();
				BufferedReader in = new BufferedReader(
						new InputStreamReader(p.getInputStream()));
				result = in.readLine();
			} else {
				System.out.println(f.getAbsolutePath());
			}
			// p.destroy();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return Double.parseDouble(result);
	}

}
