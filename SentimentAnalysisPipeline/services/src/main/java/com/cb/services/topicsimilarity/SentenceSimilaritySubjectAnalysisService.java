/**
 * 
 */
package com.cb.services.topicsimilarity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.buzea.common.ExternalServiceCache;
import eu.buzea.services.TopicAnalysisService;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class SentenceSimilaritySubjectAnalysisService
		implements
			TopicAnalysisService {

	private ExternalServiceCache cache;
	private static final Logger LOG = LogManager
			.getLogger(SentenceSimilaritySubjectAnalysisService.class);

	/**
	 * 
	 */
	private static final double SENTENCE_SIMILARITY_THRESHOLD = 0.35;

	/**
	 * 
	 */
	public SentenceSimilaritySubjectAnalysisService() {
		cache = new ExternalServiceCache(
				SentenceSimilaritySubjectAnalysisService.class);
	}

	/**
	 * 
	 * @see eu.buzea.services.TopicAnalysisService#isSameSubject(java.
	 *      lang.String, java.lang.String)
	 */
	@Override
	public boolean isSameSubject(String firstText, String secondText) {
		boolean isCached = cache.contains(firstText + secondText);
		double res = 0;
		if (isCached) {
			String inCache = cache.getResult(firstText + secondText);
			res = Double.parseDouble(inCache);
		} else {
			SentenceSimilarity sim = new SentenceSimilarity();
			res = sim.computeSimilarity(firstText, secondText);
			cache.put(firstText + secondText, res + "");
		}
		LOG.info("Sentence similarity (" + firstText + ")\n(" + secondText
				+ ")\n = " + res);
		if (res > SENTENCE_SIMILARITY_THRESHOLD) {
			return true;
		}
		return false;

	}

}
