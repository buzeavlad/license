package com.cb.services.monkeylearn;

import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.exceptions.ServiceException;
/**
 * Sentiment Analysis tool from MonkeyLearn
 * 
 * @author Vlad-Calin Buzea
 *
 */
public class MonkeyLearnSentimentAnalysisService extends SentimentAnalysisService {

	private MonkeyLearnAPI monkeyLearnAPI;

	public MonkeyLearnSentimentAnalysisService() {
		super();
		monkeyLearnAPI = new MonkeyLearnAPI();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sentiment processingCall(String text) {
		String response = null;
		for (int i = 0; i < MonkeyLearnServiceConstants.API_KEY_LIST.length
				&& response == null; i++) {
			String currentKey = MonkeyLearnServiceConstants.API_KEY_LIST[i];
			response = monkeyLearnAPI.sentimentAnalysis(currentKey, text);
		}

		if (response == null) {
			// all responses were null
			throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);
		}

		return Sentiment.valueOf(response.toUpperCase());

	}

}
