package com.cb.services.monkeylearn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.monkeylearn.MonkeyLearn;
import com.monkeylearn.MonkeyLearnResponse;

/**
 * Sentiment Analysis Service
 * 
 * @author Ciprian Budusan
 *
 */
public class MonkeyLearnAPI {

	/**
	 * Logger
	 */
	protected static final Logger LOG = LogManager.getLogger(MonkeyLearnAPI.class);
	private static final String LABEL_TAG = "label";
	public static final String SENTIMENT_ANALYSIS_MODULE_ID = "cl_qkjxv9Ly";
	public static final String TOPIC_DETECTION_MODULE_ID = "cl_5icAVzKR";
	public static final String PROFANITY_DETECTION_MODULE_ID = "cl_KFXhoTdt";

	public MonkeyLearnAPI() {

	}

	public String sentimentAnalysis(String key, String text) {

		MonkeyLearn ml = new MonkeyLearn(key);
		String[] textList = {text};
		MonkeyLearnResponse res = null;
		try {
			res = ml.classifiers.classify(SENTIMENT_ANALYSIS_MODULE_ID, textList);
			LOG.info("Remaining Queries: " + res.queryLimitRemaining);
			JSONArray array = (JSONArray) res.arrayResult.get(0);
			JSONObject obj = (JSONObject) array.get(0);
			String sentiment = obj.get(LABEL_TAG).toString();
			return sentiment;
		} catch (Exception e) {
			LOG.error(e);
		}

		return null;
	}

	public String profanityAnalysis(String key, String text) {

		MonkeyLearn ml = new MonkeyLearn(key);
		String[] textList = {text};
		MonkeyLearnResponse res = null;
		try {
			res = ml.classifiers.classify(PROFANITY_DETECTION_MODULE_ID, textList);
			LOG.info("Remaining Queries: " + res.queryLimitRemaining);
			JSONArray array = (JSONArray) res.arrayResult.get(0);
			JSONObject obj = (JSONObject) array.get(0);
			String label = obj.get(LABEL_TAG).toString();

			return label;
		} catch (Exception e) {
			LOG.error(e);
		}

		return null;
	}

}
