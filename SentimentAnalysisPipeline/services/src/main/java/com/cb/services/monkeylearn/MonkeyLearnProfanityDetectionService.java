package com.cb.services.monkeylearn;

import eu.buzea.services.ProfanityDetectionService;
import eu.buzea.services.exceptions.ServiceException;

public class MonkeyLearnProfanityDetectionService
		extends
			ProfanityDetectionService {

	private static final String PROFANITY = "profanity";
	private MonkeyLearnAPI monkeyLearnAPI;

	public MonkeyLearnProfanityDetectionService() {
		super();
		monkeyLearnAPI = new MonkeyLearnAPI();
	}

	@Override
	public Boolean processingCall(String text) {
		String response = null;
		for (int i = 0; i < MonkeyLearnServiceConstants.API_KEY_LIST.length
				&& response == null; i++) {
			String currentKey = MonkeyLearnServiceConstants.API_KEY_LIST[i];
			response = monkeyLearnAPI.profanityAnalysis(currentKey, text);
		}

		if (response == null) {
			throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);
		}

		if (response.equals(PROFANITY)) {
			return true;
		} else {
			return false;
		}
	}

}
