package eu.buzea.common;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.commons.io.IOUtils;

/**
 * Util class for posting to Rest services
 * 
 * @author Vlad-Calin Buzea
 *
 */
public final class RestUtils {

	/**
	 * Hides constructor to avoid instantiation
	 */
	private RestUtils() {
	}

	public static String callRestService(Map<String, String> parameters,
			String urlString) {

		String urlParameters = new String();
		for (String name : parameters.keySet()) {
			urlParameters += (name + "=");
			urlParameters += (parameters.get(name));
			urlParameters += "&";
		}
		urlParameters = urlParameters.substring(0, urlParameters.length() - 1);

		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			String response = RestUtils.postToConnection(connection,
					urlParameters);

			return response;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Posts to Rest service using url parameters. Uses
	 * "application/x-www-form-urlencoded" content type.
	 * 
	 * @param connection
	 * @param urlParameters
	 * @return
	 * @throws IOException
	 */
	private static String postToConnection(HttpURLConnection connection,
			String urlParameters) throws IOException {
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		connection.setRequestProperty("charset", "utf-8");
		connection.setRequestProperty("Content-Length",
				"" + Integer.toString(urlParameters.getBytes().length));
		connection.setUseCaches(true);

		DataOutputStream wr = null;

		try {
			wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
		} finally {
			wr.close();
		}
		InputStream is = connection.getInputStream();
		return IOUtils.toString(is);
	}

}
