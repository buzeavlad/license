package eu.buzea.common;

import org.ehcache.Cache;
import org.ehcache.PersistentCacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.core.Ehcache;

/**
 * Wrapper over Ehcache. Used to reduce calls to web services.
 * 
 * @author Vlad-Calin Buzea
 *
 */
public class ExternalServiceCache {

	private static final String CACHE_FOLDER = "./cache";
	private Cache<String, String> cache;
	private static PersistentCacheManager persistentCacheManager = null;

	/**
	 * Creates a persistent cache, stored on the disc in the {@literal cache}
	 * folder
	 */
	public ExternalServiceCache(Class<?> clazz) {
		String cacheName = clazz.getName();
		if (persistentCacheManager == null) {
			persistentCacheManager = CacheManagerBuilder
					.newCacheManagerBuilder()
					.with(CacheManagerBuilder.persistence(CACHE_FOLDER))
					.build();
			persistentCacheManager.init();
			addHook();
		}

		cache = persistentCacheManager.getCache(cacheName, String.class,
				String.class);
		if (cache == null) {
			cache = persistentCacheManager.createCache(cacheName,
					CacheConfigurationBuilder
							.newCacheConfigurationBuilder(String.class,
									String.class,
									ResourcePoolsBuilder
											.newResourcePoolsBuilder()
											.heap(10000, EntryUnit.ENTRIES)
											.offheap(100, MemoryUnit.MB)
											.disk(1L, MemoryUnit.GB, true))
							.build());
		}

	}

	private void addHook() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				if (persistentCacheManager != null) {
					persistentCacheManager.close();
					persistentCacheManager = null;
				}

			}
		}));
	}

	/**
	 * Wrapper over {@link Ehcache#containsKey(Object)}
	 * 
	 * @see Ehcache#containsKey(Object)
	 * @param text
	 *            the text that has been sent to the service
	 * @return true if there is a response cached, false otherwise
	 */
	public boolean contains(String key) {
		return cache.containsKey(key);
	}

	/**
	 * Wrapper over {@link Ehcache#get(Object)}
	 * 
	 * @see Ehcache#get(Object)
	 * @param text
	 *            the text that has been sent to the service
	 * @return the response from the service
	 */
	public String getResult(String key) {
		return cache.get(key);
	}

	/**
	 * Wrapper over {@link Ehcache#put(Object, Object)}
	 * 
	 * @see Ehcache#put(Object, Object)
	 * @param text
	 *            the text that has been sent to the service
	 * @param response
	 *            the response from the service
	 */
	public synchronized void put(String key, String response) {
		cache.put(key, response);
	}

	/*	@Override
		protected void finalize() throws Throwable {
			if (persistentCacheManager != null) {
				persistentCacheManager.close();
				persistentCacheManager = null;
			}
			super.finalize();
		}*/

}
