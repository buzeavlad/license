/**
 * 
 */
package eu.buzea.services.vivekn;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import eu.buzea.common.RestUtils;
import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class ViveknSentimentAnalysisService extends SentimentAnalysisService {

	/**
	 * 
	 */
	public ViveknSentimentAnalysisService() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Sentiment processingCall(String text) {
		String url = "http://sentiment.vivekn.com/api/text/";
		String response = callRestService(text, url);
		return getSentimentFromJson(response);
	}

	public String callRestService(String text, String urlString) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("txt", text);
		return RestUtils.callRestService(parameters, urlString);

	}

	private Sentiment getSentimentFromJson(String queryResult) {
		JsonElement jsonResponse = new JsonParser().parse(queryResult);
		JsonElement sentiment = jsonResponse.getAsJsonObject().get("result").getAsJsonObject()
				.get("sentiment");
		String sentimentString = sentiment.getAsString().toUpperCase().replace("\"", "");
		return Sentiment.valueOf(sentimentString);
	}

}
