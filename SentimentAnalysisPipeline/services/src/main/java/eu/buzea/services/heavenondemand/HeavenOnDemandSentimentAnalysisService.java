/**
 * 
 */
package eu.buzea.services.heavenondemand;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.exceptions.ServiceException;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class HeavenOnDemandSentimentAnalysisService extends SentimentAnalysisService {

	private static final String[] API_KEY_LIST = {
			"e7b4814b-2568-4ed7-b044-3a104dd37dfd"};
	private HeavenOnDemandAPI heavenOnDemandAPI;
	/**
	 * 
	 */
	public HeavenOnDemandSentimentAnalysisService() {
		heavenOnDemandAPI = new HeavenOnDemandAPI();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Sentiment processingCall(String text) {
		String response = null;
		for (int i = 0; i < API_KEY_LIST.length && response == null; i++) {
			String currentKey = API_KEY_LIST[i];
			response = heavenOnDemandAPI.sentimentAnalysis(currentKey, text);
		}
		if (response == null) {
			throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);
		}
		return getSentiment(response);
	}

	/**
	 * @param response
	 * @return
	 */
	private Sentiment getSentiment(String response) {
		JsonElement jsonResponse = new JsonParser().parse(response);
		JsonObject jsonObject = jsonResponse.getAsJsonObject();
		JsonElement sentiment = jsonObject.getAsJsonObject("aggregate")
				.get("sentiment");
		String sentimentString = sentiment.getAsString().toUpperCase();
		return Sentiment.valueOf(sentimentString);
	}

}
