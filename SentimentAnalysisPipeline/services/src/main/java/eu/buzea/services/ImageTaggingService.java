package eu.buzea.services;

import java.io.File;
import java.net.URL;
import java.util.List;

/**
 * Image Tagging Service
 * 
 * @author Ciprian Budusan
 *
 */
public interface ImageTaggingService {

	List<String> imageTagging(File file);

	List<String> imageTagging(URL url);
	
	String imageText(File file);
	
	String imageText(URL url);

}
