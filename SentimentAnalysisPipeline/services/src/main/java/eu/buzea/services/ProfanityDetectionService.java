package eu.buzea.services;

import eu.buzea.common.ExternalServiceCache;

/**
 * Service used to detect profanity
 * 
 * @author Vlad-Calin Buzea
 *
 */
public abstract class ProfanityDetectionService {

	/**
	 * Use cache to reduce number of calls to external service
	 */
	protected ExternalServiceCache cache;

	/**
	 * 
	 */
	public ProfanityDetectionService() {
		cache = new ExternalServiceCache(getClass());
	}

	/**
	 * Checks if a text is profane or not
	 * 
	 * @param text
	 *            input text
	 * @return true if contains swearing, false otherwise
	 */
	public boolean isProfanity(String text) {
		boolean isCached = cache.contains(text);
		if (isCached) {
			String result = cache.getResult(text).toUpperCase();
			return Boolean.parseBoolean(result);
		} else {
			Boolean r = processingCall(text);
			cache.put(text, r.toString());
			return r;
		}
	}

	/**
	 * Calls external profanity detection service
	 * 
	 * @param text
	 *            text to analyze
	 * @return true if contains swearing, false otherwise
	 */
	protected abstract Boolean processingCall(String text);
}
