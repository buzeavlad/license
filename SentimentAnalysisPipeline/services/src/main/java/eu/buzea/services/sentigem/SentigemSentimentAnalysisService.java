/**
 * 
 */
package eu.buzea.services.sentigem;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import eu.buzea.common.RestUtils;
import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;

/**
 * Web Service Endpoint Adapter for https://sentigem.com/
 * 
 * @author Vlad-Calin Buzea
 *
 */
public class SentigemSentimentAnalysisService extends SentimentAnalysisService {

	/**
	 * Creates a new instance of the endpoint
	 */
	public SentigemSentimentAnalysisService() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Sentiment processingCall(String text) {
		String urlString = "https://api.sentigem.com/external/get-sentiment";
		String key = "e7eba871f539d5c306b644db6f0d7824odsKaOqfRTScGymtYx_32JEDrj8-H0Ig";
		String resultJson = callRestService(key, text, urlString);
		return getSentimentFromJson(resultJson);

	}

	/**
	 * Extracts relevant data from web service response
	 * 
	 * @param queryResult
	 *            JSON response in string format
	 * @return the sentiment in the {@code polarity} field of the input
	 */
	private Sentiment getSentimentFromJson(String queryResult) {
		JsonElement jsonResponse = new JsonParser().parse(queryResult);
		JsonElement sentiment = jsonResponse.getAsJsonObject().get("polarity");
		String sentimentString = sentiment.getAsString().toUpperCase().replace("\"", "");
		return Sentiment.valueOf(sentimentString);
	}

	/**
	 * Sets parameters and send Rest request to url
	 * 
	 * @param key
	 *            parameter for web service
	 * @param text
	 *            parameter for web service
	 * @param urlString
	 *            url of the web service
	 * @return response from web service
	 */
	private String callRestService(String key, String text, String urlString) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("api-key", key);
		parameters.put("text", text);
		return RestUtils.callRestService(parameters, urlString);

	}

}
