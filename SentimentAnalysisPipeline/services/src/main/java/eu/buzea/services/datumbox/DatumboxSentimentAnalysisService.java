package eu.buzea.services.datumbox;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.exceptions.ServiceException;

public class DatumboxSentimentAnalysisService extends SentimentAnalysisService {

	private static final String[] API_KEY_LIST = {
			"7540aaf2d9913399ad03c98f26da6ec4"};
	private DatumboxAPI datumboxApi;

	public DatumboxSentimentAnalysisService() {
		datumboxApi = new DatumboxAPI();
	}

	@Override
	public Sentiment processingCall(String text) {
		String response = null;
		for (int i = 0; i < API_KEY_LIST.length && response == null; i++) {
			String currentKey = API_KEY_LIST[i];
			response = datumboxApi.twitterSentimentAnalisys(currentKey, text);
		}
		if (response == null) {
			throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);
		}
		return getSentimentFromJson(response);
	}

	private Sentiment getSentimentFromJson(String queryResult) {
		JsonElement jsonResponse = new JsonParser().parse(queryResult);
		JsonObject jsonObject = jsonResponse.getAsJsonObject();
		JsonElement sentiment = jsonObject.getAsJsonObject("output")
				.get("result");
		String sentimentString = sentiment.getAsString().toUpperCase()
				.replace("\"", "");
		return Sentiment.valueOf(sentimentString);
	}

}
