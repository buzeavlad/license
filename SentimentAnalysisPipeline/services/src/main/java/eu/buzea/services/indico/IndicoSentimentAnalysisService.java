/**
 * 
 */
package eu.buzea.services.indico;

import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.exceptions.ServiceException;
import io.indico.Indico;
import io.indico.api.results.IndicoResult;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class IndicoSentimentAnalysisService extends SentimentAnalysisService {

	public static final String[] API_KEYS = {"3547cfda810326fdabec70bd782ccae9"};
	/**
	 * This function will return a number between 0 and 1. This number is a
	 * probability representing the likelihood that the analyzed text is
	 * positive or negative. Values greater than 0.5 indicate positive
	 * sentiment, while values less than 0.5 indicate negative sentiment.
	 */
	public static final double THRESHOLD = 0.5;
	public IndicoSentimentAnalysisService() {

	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected Sentiment processingCall(String text) {
		for (int i = 0; i < API_KEYS.length; i++) {
			Indico indico = new Indico(API_KEYS[i]);
			try {
				IndicoResult single = indico.sentiment.predict(text);
				Double value = single.getSentiment();
				if (value > THRESHOLD)
					return Sentiment.POSITIVE;
				else
					return Sentiment.NEGATIVE;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);
	}

}
