package eu.buzea.services;

/**
 * Spelling and Grammar Correction Service
 * 
 * @author Ciprian Budusan
 *
 */
public interface GrammarCheckingService {

	public String correctText(String text);
}
