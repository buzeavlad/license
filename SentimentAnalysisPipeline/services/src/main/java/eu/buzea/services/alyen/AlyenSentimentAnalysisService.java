/**
 * 
 */
package eu.buzea.services.alyen;
import com.aylien.textapi.TextAPIClient;
import com.aylien.textapi.parameters.SentimentParams;

import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.exceptions.ServiceException;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class AlyenSentimentAnalysisService extends SentimentAnalysisService {

	/**
	 * 
	 */
	public AlyenSentimentAnalysisService() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Sentiment processingCall(String text) {
		try {
			TextAPIClient client = new TextAPIClient("7c7d07e8",
					"f792b34237fa053bf620abf9bc9bdfea");
			SentimentParams.Builder builder = SentimentParams.newBuilder();
			builder.setText(text);
			String response = client.sentiment(builder.build()).getPolarity().toUpperCase();
			return Sentiment.valueOf(response);
		} catch (Exception e) {
			throw new ServiceException(ServiceException.NO_MORE_QUERIES_LEFT);
		}
	}

}
