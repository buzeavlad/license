package eu.buzea.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.buzea.common.ExternalServiceCache;

/**
 * Service used to analyze sentiment expressed in a text
 * 
 * @author Vlad-Calin Buzea
 *
 */
public abstract class SentimentAnalysisService {

	/**
	 * Use cache to reduce number of calls to external service
	 */
	protected ExternalServiceCache cache;

	/**
	 * Boolean value that determines if caching is used or not
	 */
	private boolean isCachingEnabled;

	/**
	 * Logger
	 */
	protected static final Logger LOG = LogManager.getLogger(SentimentAnalysisService.class);

	/**
	 * Constructs a sentiment analysis service with {@link ExternalServiceCache}
	 */
	public SentimentAnalysisService() {
		cache = new ExternalServiceCache(getClass());
		isCachingEnabled = true;
	}

	/**
	 * Analyzes the sentiment expressed in the input text
	 * 
	 * @param text
	 *            input text
	 * @return {@link Sentiment#POSITIVE} or {@link Sentiment#NEGATIVE} or
	 *         {@link Sentiment#NEUTRAL}
	 */
	public Sentiment process(String text) {
		LOG.info("Processing string: " + text);
		Sentiment[] sentiments = Sentiment.values();
		for (Sentiment s : sentiments) {
			if (s.toString().equalsIgnoreCase(text)) {
				return s;
			}
		}

		boolean isCached = cache.contains(text);
		if (isCached && isCachingEnabled) {
			String sentiment = cache.getResult(text).toUpperCase();
			LOG.info("Found in cache: " + sentiment);
			return Sentiment.valueOf(sentiment);
		} else {
			Sentiment s = processingCall(text);
			cache.put(text, s.toString());
			LOG.info("Response from external Service: " + s);
			return s;
		}

	}

	/**
	 * Calls the external sentiment Analysis service
	 * 
	 * @param text
	 *            text to be analyzed
	 * @return {@link Sentiment#POSITIVE} or {@link Sentiment#NEGATIVE} or
	 *         {@link Sentiment#NEUTRAL}
	 */
	protected abstract Sentiment processingCall(String text);

	/**
	 * Getter method for isCacheEnabled
	 * 
	 * @return the isCacheEnabled
	 */
	public boolean isCachingEnabled() {
		return isCachingEnabled;
	}

	/**
	 * Setter method for isCacheEnabled
	 * 
	 * @param isCacheEnabled
	 *            the isCacheEnabled value to set
	 */
	public void setCachingEnabled(boolean isCacheEnabled) {
		this.isCachingEnabled = isCacheEnabled;
	}

}
