package eu.buzea.services;

/**
 * Representation of all possible sentiments expressed in a text
 * 
 * @author Vlad-Calin Buzea
 *
 */
public enum Sentiment {
	NEGATIVE, NEUTRAL, POSITIVE
}
