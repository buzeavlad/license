/**
 * 
 */
package eu.buzea.services.heavenondemand;

import java.util.HashMap;
import java.util.Map;

import eu.buzea.common.RestUtils;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class HeavenOnDemandAPI {

	/**
	 * 
	 */
	private static final String BASE_URL = "https://api.havenondemand.com/1/api/sync/";

	/**
	 * 
	 */
	public HeavenOnDemandAPI() {

	}

	/**
	 * @param currentKey
	 * @param text
	 * @return
	 */
	public String sentimentAnalysis(String key, String text) {
		String urlString = BASE_URL + "analyzesentiment/v1";
		return callRestService(key, text, urlString);
	}

	/**
	 * Calls HeavenOnDemand Rest Service using POST method
	 * 
	 * @param key
	 * @param text
	 * @param urlString
	 * @return
	 */
	public String callRestService(String key, String text, String urlString) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("apikey", key);
		parameters.put("text", text);
		return RestUtils.callRestService(parameters, urlString);

	}

}
