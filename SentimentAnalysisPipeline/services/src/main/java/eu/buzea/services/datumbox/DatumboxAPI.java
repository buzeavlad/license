package eu.buzea.services.datumbox;

import java.util.HashMap;
import java.util.Map;

import eu.buzea.common.RestUtils;

public class DatumboxAPI {

	public DatumboxAPI() {

	}

	public String sentimentAnalysis(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/SentimentAnalysis.json";
		return callRestService(key, text, urlString);
	}

	public String twitterSentimentAnalisys(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/TwitterSentimentAnalysis.json";
		return callRestService(key, text, urlString);
	}

	public String subjectivityAnalysis(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/SubjectivityAnalysis.json";
		return callRestService(key, text, urlString);
	}

	public String topicClassification(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/TopicClassification.json";
		return callRestService(key, text, urlString);
	}

	public String spamDetection(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/SpamDetection.json";
		return callRestService(key, text, urlString);
	}

	public String adultContentDetection(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/AdultContentDetection.json";
		return callRestService(key, text, urlString);
	}

	public String readabilityAssesment(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/ReadabilityAssessment.json";
		return callRestService(key, text, urlString);
	}

	public String languageDetection(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/LanguageDetection.json";
		return callRestService(key, text, urlString);
	}

	public String commercialDetection(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/CommercialDetection.json";
		return callRestService(key, text, urlString);
	}

	public String educationalDetection(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/EducationalDetection.json";
		return callRestService(key, text, urlString);
	}

	public String genderDetection(String key, String text) {
		String urlString = "http://api.datumbox.com/1.0/GenderDetection.json";
		return callRestService(key, text, urlString);
	}

	public String keywordExtraction(String key, String text, int n) {
		String urlString = "http://api.datumbox.com/1.0/KeywordExtraction.json";
		return callRestService(key, text, urlString, n);
	}

	/**
	 * Calls Datumbox Rest Service using POST method
	 * 
	 * @param key
	 * @param text
	 * @param urlString
	 * @return
	 */
	public String callRestService(String key, String text, String urlString) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("api_key", key);
		parameters.put("text", text);
		return RestUtils.callRestService(parameters, urlString);

	}

	/**
	 * Calls Datumbox Rest Service using POST method
	 * 
	 * @param key
	 * @param text
	 * @param urlString
	 * @param n
	 * @return
	 */
	public String callRestService(String key, String text, String urlString,
			int n) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("api_key", key);
		parameters.put("text", text);
		parameters.put("n", n + "");
		return RestUtils.callRestService(parameters, urlString);

	}
}
