package eu.buzea.services;

import com.cb.services.topicsimilarity.SentenceSimilaritySubjectAnalysisService;

/**
 * Service dealing with subject analysis <br>
 * This service is defined as an interface due to the threshold set in
 * {@link SentenceSimilaritySubjectAnalysisService}
 * 
 * @author Vlad-Calin Buzea
 */
public interface TopicAnalysisService {

	/**
	 * Checks if the two texts talk about the same thing
	 * 
	 * @param firstText
	 *            text number 1
	 * @param secondText
	 *            text number 2
	 * @return true if same topic, false otherwise
	 */
	public boolean isSameSubject(String firstText, String secondText);
}
