package eu.buzea.util;

import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import eu.buzea.inputschema.Input;

public class RestServiceUtil {

	private String serverUrl;

	public static final String STRING_PARAMETER_SERVICE = "/processing/inputParam";

	public static final String OBJECT_PARAMETER_SERVICE = "/processing/input";

	public static final String MULTIPART_FILE_PARAMETER_SERVICE = "/processing/multipartfile";

	public RestServiceUtil(String serverUrl) {
		if (serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		this.serverUrl = serverUrl;
	}

	public boolean postObject(Input input) {
		try {
			String urlString = serverUrl + OBJECT_PARAMETER_SERVICE;
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_XML);
			HttpEntity<Input> entity = new HttpEntity<>(input, headers);
			ResponseEntity<Boolean> response = restTemplate.exchange(urlString, HttpMethod.POST, entity, Boolean.class);
			return response.getBody().booleanValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean postMultipartFile(File input) {
		try {
			String urlString = serverUrl + MULTIPART_FILE_PARAMETER_SERVICE;
			RestTemplate template = new RestTemplate();
			LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
			map.add("xml", new FileSystemResource(input));
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			org.springframework.http.HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new org.springframework.http.HttpEntity<LinkedMultiValueMap<String, Object>>(
					map, headers);
			ResponseEntity<Boolean> response = template.exchange(urlString, HttpMethod.POST, requestEntity,
					Boolean.class);
			return response.getBody().booleanValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Deprecated
	public boolean postParameterString(String input) {
		try {
			String urlString = serverUrl + STRING_PARAMETER_SERVICE;
			String urlParameters = "input=" + input;
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setUseCaches(true);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			InputStream is = connection.getInputStream();
			String response = IOUtils.toString(is).trim();
			connection.disconnect();
			if (Boolean.TRUE.toString().equalsIgnoreCase(response)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
