package eu.buzea.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import eu.buzea.inputschema.Input;

public final class XmlUtil {

	private XmlUtil() {

	}

	public static final Input unmarshallInput(String xml) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		Input input = (Input) jaxbUnmarshaller.unmarshal(reader);
		return input;
	}

	public static final Input unmarshallInput(File file) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Input input = (Input) jaxbUnmarshaller.unmarshal(file);
		return input;
	}

	public static final Input unmarshallInput(InputStream inputStream) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Input input = (Input) jaxbUnmarshaller.unmarshal(inputStream);
		return input;
	}

	public static void serialize(Input input, File outputFile) throws JAXBException, FileNotFoundException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
				"http://www.buzea.eu/InputSchema http://www.buzea.eu/InputSchema.xsd ");
		FileOutputStream out = new FileOutputStream(outputFile);
		jaxbMarshaller.marshal(input, out);
	}

	public static String serialize(Input input) throws JAXBException, PropertyException, FileNotFoundException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
				"http://www.buzea.eu/InputSchema http://www.buzea.eu/InputSchema.xsd ");
		Writer writer = new StringWriter();
		jaxbMarshaller.marshal(input, writer);
		return writer.toString();
	}
}
