package eu.buzea.bechmarks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.buzea.datamodel.config.PersistenceContext;
import eu.buzea.datamodel.entities.Edge;
import eu.buzea.datamodel.repositories.EdgeRepository;
import eu.buzea.datamodel.repositories.RunRepository;
import eu.buzea.inputschema.Input;
import eu.buzea.processing.SocialNetworkProcessing;
import eu.buzea.services.sentigem.SentigemSentimentAnalysisService;
import eu.buzea.util.XmlUtil;

@ContextConfiguration(classes = {PersistenceContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManualEvaluationBenchmark {

	/**
	 * Constants
	 */
	private static final int NB_BENCHMARKS = 2;
	private static final String RESOURCES = "./src/benchmarks/resources";
	private static final String INPUT_ASK_REDDIT = RESOURCES + "/AskRedditExtractedData.xml";
	private static final String INPUT_DISCRIMINATION = RESOURCES
			+ "/DiscriminationExtractedData.xml";
	private static final String EVAL_ASK_REDDIT = RESOURCES + "/AskRedditEval.txt";
	private static final String EVAL_DISCRIMINATION = RESOURCES + "/DiscriminationEval.txt";
	private static final Logger LOG = LoggerFactory.getLogger("BenchmarkLogger");
	/**
	 * Expected Graph as adjacency matrices
	 */
	private int[][] positive = new int[100][100];
	private int[][] neutral = new int[100][100];
	private int[][] negative = new int[100][100];

	/**
	 * Absolute error Row: benchmark number. Columns: 0 - error positive 1 -
	 * error neutral 2 - error negative
	 */
	private static int error[][];

	/**
	 * "Max possible error" Row: benchmark number. Columns: 0 - Max error
	 * positive 1 - Max error neutral 2 - Max error negative
	 */
	private static int expectedTotal[][];

	/**
	 * Absolute error/Max possible error Row: benchmark number. Columns: 0 -
	 * Relative error positive 1 - Relative error neutral 2 - Relative error
	 * negative
	 */
	private static double relativeError[][];

	/**
	 * Row: benchmark number. Columns: 0 - Number of edges expected but missing
	 * 1 - Number of edges expected and found 2 - Number of edges not expected
	 * but found
	 */
	private static int edgeCount[][];

	@Autowired
	SocialNetworkProcessing processing;

	@Autowired
	EdgeRepository edgeRepository;

	@Autowired
	RunRepository runRepository;

	@BeforeClass
	public static void init() {
		error = new int[NB_BENCHMARKS][3];
		expectedTotal = new int[NB_BENCHMARKS][3];
		relativeError = new double[NB_BENCHMARKS][3];
		edgeCount = new int[NB_BENCHMARKS][3];

		for (int i = 0; i < NB_BENCHMARKS; i++)
			for (int j = 0; j < 3; j++) {
				error[i][j] = expectedTotal[i][j] = edgeCount[i][j] = 0;
				relativeError[i][j] = 0.0;
			}
	}

	public void readEval(String filePath) throws FileNotFoundException {

		Scanner scanner = new Scanner(new File(filePath));

		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				positive[i][j] = scanner.nextInt();
			}
		}

		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				neutral[i][j] = scanner.nextInt();
			}

		}

		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				negative[i][j] = scanner.nextInt();
			}

		}
		scanner.close();

	}

	@Test
	public void benckmarkAskReddit() throws Exception {
		LOG.info("\r\n\r\nSTARTED Ask Reddit benchmark");
		File file = new File(INPUT_ASK_REDDIT);
		LOG.info("Strating Unmarshall");
		Input input = XmlUtil.unmarshallInput(file);
		LOG.info("Starting processing");
		processing.setSentimentAnalisysService(new SentigemSentimentAnalysisService());
		processing.processInput(input);
		LOG.info("Starting benchmark");
		readEval(EVAL_ASK_REDDIT);
		benchmark(input, 0);
	}

	@Test
	public void benchmarkDiscrimination() throws Exception {
		LOG.info("\r\n\r\nSTARTED Discrimination benchmark");
		File file = new File(INPUT_DISCRIMINATION);
		LOG.info("Strating Unmarshall");
		Input input = XmlUtil.unmarshallInput(file);
		LOG.info("Starting processing");
		processing.setSentimentAnalisysService(new SentigemSentimentAnalysisService());
		processing.processInput(input);
		LOG.info("Starting benchmark");
		readEval(EVAL_DISCRIMINATION);
		benchmark(input, 1);
	}

	private void benchmark(Input input, int benchmarkNb) {
		long maxTimeValue = runRepository.maxTimeValue(input.getRunName());
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				compare(i, j, positive[i][j], neutral[i][j], negative[i][j], input.getRunName(),
						maxTimeValue, benchmarkNb);

			}
		}

		for (int i = 0; i < 3; i++) {
			relativeError[benchmarkNb][i] = ((error[benchmarkNb][i] + 1)
					/ ((double) expectedTotal[benchmarkNb][i] + 1));
		}
	}

	private void compare(int start, int end, int positive, int neutral, int negative,
			String runName, Long timeValue, int benchmarkNb) {
		Edge e = edgeRepository.findEdgeBefore(start, end, timeValue, runName);
		boolean isExpected = (positive > 0) || (negative > 0) || (neutral > 0);
		boolean isActual = (e != null);

		if (!isExpected && !isActual) {
			// true negative
			return;
		}
		if (isExpected && !isActual) {
			// false negative
			edgeCount[benchmarkNb][0]++;
			e = new Edge();
		}
		if (isExpected && isActual) {
			// true positive
			edgeCount[benchmarkNb][1]++;
		}
		if (!isExpected && isActual) {
			// false positive
			edgeCount[benchmarkNb][2]++;
		}

		LOG.info(start + " -> " + end + " :");
		LOG.info("Positive:	Expected " + positive + "; Real: " + e.getWeightPositive());
		error[benchmarkNb][0] += Math.abs(positive - e.getWeightPositive());
		LOG.info("Neutral:	Expected " + neutral + "; Real: " + e.getWeightNeutral());
		error[benchmarkNb][1] += Math.abs(neutral - e.getWeightNeutral());
		LOG.info("Negative:	Expected " + negative + "; Real: " + e.getWeightNegative());
		error[benchmarkNb][2] += Math.abs(negative - e.getWeightNegative());

		expectedTotal[benchmarkNb][0] += positive + e.getWeightPositive();
		expectedTotal[benchmarkNb][1] += neutral + e.getWeightNeutral();
		expectedTotal[benchmarkNb][2] += negative + e.getWeightNegative();
	}

	@AfterClass
	public static void showResults() {
		for (int i = 0; i < NB_BENCHMARKS; i++) {
			LOG.info("BENCHMARK " + (i + 1));
			LOG.info("Error Positive	= " + (int) (relativeError[i][0] * 100) + "%");
			LOG.info("Error Neutral	= " + (int) (relativeError[i][1] * 100) + "%");
			LOG.info("Error Negative	= " + (int) (relativeError[i][2] * 100) + "%");
			LOG.info("Number of edges expected but missing:\t" + edgeCount[i][0]);
			LOG.info("Number of edges expected and found:\t" + edgeCount[i][1]);
			LOG.info("Number of edges unexpected but found:\t" + edgeCount[i][2]);

		}
	}

}
