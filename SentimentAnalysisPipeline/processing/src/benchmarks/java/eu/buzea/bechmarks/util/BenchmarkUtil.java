package eu.buzea.bechmarks.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.buzea.inputschema.User;

public final class BenchmarkUtil {

	private static Random random;
	public static final String LINE_COMMENT_PREFIX = ";";
	public static final int NUMBER_VALENT_OF_WORDS_IN_SENTENCE = 4;
	private static DatatypeFactory datatypeFactory;
	private static List<String> daVinciPos, daVinciNeg, otherPos, otherNeg;
	private static List<String> topicAnimals, topicFood, verbs, stopWords, insults, positiveWords,
			negativeWords;
	static {
		try {
			datatypeFactory = DatatypeFactory.newInstance();

			random = new Random(1993);// use static seed to save queries by
			// caching

			File file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.SEMANTIC_FIELD_ANIMALS_TXT);
			topicAnimals = BenchmarkUtil.readRows(file);
			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.SEMANTIC_FIELD_FOOD_TXT);
			topicFood = BenchmarkUtil.readRows(file);
			file = new File(
					BenchmarkFileConstants.RESOURCE_FOLDER + BenchmarkFileConstants.VERBS_TXT);
			verbs = BenchmarkUtil.readRows(file);
			file = new File(
					BenchmarkFileConstants.RESOURCE_FOLDER + BenchmarkFileConstants.STOPWORDS_TXT);
			stopWords = BenchmarkUtil.readRows(file);
			file = new File(
					BenchmarkFileConstants.RESOURCE_FOLDER + BenchmarkFileConstants.INSULTS_TXT);
			insults = BenchmarkUtil.readRows(file);
			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.NEGATIVE_WORDS_TXT);
			negativeWords = BenchmarkUtil.readRows(file);
			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.POSITIVE_WORDS_TXT);
			positiveWords = BenchmarkUtil.readRows(file);

			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.KAGGE_DATA_SET_DA_VINCI_NEG);
			daVinciNeg = readRows(file);

			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.KAGGE_DATA_SET_DA_VINCI_POS);
			daVinciPos = readRows(file);

			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.KAGGE_DATA_SET_OTHER_NEG);
			otherNeg = readRows(file);

			file = new File(BenchmarkFileConstants.RESOURCE_FOLDER
					+ BenchmarkFileConstants.KAGGE_DATA_SET_OTHER_POS);
			otherPos = readRows(file);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private BenchmarkUtil() {

	}

	/**
	 * Reads the file containing words/sentences used for benchmarking. Each row
	 * from the input file is translated to one element in the output list Lines
	 * starting with ';' are ignored
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static List<String> readRows(File file) throws FileNotFoundException {
		Set<String> result = new TreeSet<>();
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				if (!line.startsWith(LINE_COMMENT_PREFIX)) {
					result.add(line);
				}
			}
		}
		return new ArrayList<String>(result);
	}

	/**
	 * Generates a list of <b>n</b> users with names U1 up to Un and ids from 1
	 * to <b>n</b>.
	 * 
	 * @param n
	 *            number of users to be generated
	 * @return list of random users
	 */
	public static List<User> generateUsers(long n) {
		List<User> result = new ArrayList<>();
		for (long i = 1; i <= n; i++) {
			User u = new User();
			u.setId(i);
			String userName = "U" + i;
			u.setName(userName);
			result.add(u);
		}
		return result;
	}

	/**
	 * Returns current date in XMLGregorianCalendar format
	 * 
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar getCurrentDate() throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		return now;
	}

	/**
	 * Generates text using words from {@link #topicFood} and
	 * {@link #negativeWords}
	 * 
	 * @param numberOfSentences
	 *            number of sentences the text will have
	 * @return
	 */
	public static String generateNegativeTextAboutFood(int numberOfSentences) {
		String text = new String();
		for (int i = 0; i < numberOfSentences; i++) {
			text += generateStructuredNegativeSentenceFood(NUMBER_VALENT_OF_WORDS_IN_SENTENCE)
					+ ". ";
		}
		return text;
	}

	/**
	 * Generates a sentence of the following structure:<br>
	 * food + verb + {@code numberOfNegativeWords}*(food + negative word + stop
	 * word)
	 * 
	 * @param numberOfNegativeWords
	 *            number of negative constructs to use in the sentence
	 * @return Positive sentence on topic "Food"
	 */
	public static String generateStructuredNegativeSentenceFood(int numberOfNegativeWords) {
		// main subject
		String text = getRandomElement(topicFood);
		// verb
		text += " " + getRandomElement(verbs);
		// generate negative opinion: topic + negative word + stop word
		for (int i = 0; i < numberOfNegativeWords; i++) {
			text += " " + getRandomElement(topicFood);
			text += " " + getRandomElement(negativeWords);
			text += " " + getRandomElement(stopWords);
		}

		return text;
	}
	/**
	 * Generates text using words from {@link #topicFood} with no valences
	 * 
	 * @param numberOfSentences
	 *            number of sentences the text will have
	 * @return
	 */
	public static String generateNeutralTextAboutFood(int numberOfSentences) {
		String text = new String();
		for (int i = 0; i < numberOfSentences; i++) {
			text += generateStructuredNeutralSentenceFood() + ". ";
		}
		return text;

	}

	/**
	 * Generates a sentence of the following structure:<br>
	 * food + verb + food
	 * 
	 * @return Neutral sentence on topic "Food"
	 */
	public static String generateStructuredNeutralSentenceFood() {
		String text = getRandomElement(topicFood);
		text += " " + getRandomElement(verbs);
		text = getRandomElement(topicFood);
		return text;
	}

	/**
	 * Generates text using words from {@link #topicFood} and
	 * {@link #positiveWords}
	 * 
	 * @param numberOfSentences
	 *            number of sentences the text will have
	 * @return
	 */
	public static String generatePositiveTextAboutFood(int numberOfSentences) {
		String text = new String();
		for (int i = 0; i < numberOfSentences; i++) {
			text += generateStructuredPositiveSentenceFood(NUMBER_VALENT_OF_WORDS_IN_SENTENCE)
					+ ". ";
		}
		return text;
	}

	/**
	 * Generates a sentence of the following structure:<br>
	 * food + verb + {@code numberOfPositiveWords} * (food + positive word +
	 * stop word)
	 * 
	 * @param numberOfPositiveWords
	 *            number of positive constructs to use in the sentence
	 * @return Positive sentence on topic "Animals"
	 */
	public static String generateStructuredPositiveSentenceFood(int numberOfPositiveWords) {
		// main subject
		String text = getRandomElement(topicFood);
		// verb
		text += " " + getRandomElement(verbs);
		// generate negative opinion: topic + negative word + stop word
		for (int i = 0; i < numberOfPositiveWords; i++) {
			text += " " + getRandomElement(topicFood);
			text += " " + getRandomElement(positiveWords);
			text += " " + getRandomElement(stopWords);
		}

		return text;
	}

	/**
	 * Generates text using words from {@link #topicAnimals} and
	 * {@link #negativeWords}
	 * 
	 * @param numberOfSentences
	 *            number of sentences the text will have
	 * @return
	 */
	public static String generateNegativeTextAboutAnimals(int numberOfSentences) {
		String text = new String();
		for (int i = 0; i < numberOfSentences; i++) {
			text += generateStructuredNegativeSentenceAnimals(NUMBER_VALENT_OF_WORDS_IN_SENTENCE)
					+ ". ";
		}
		return text;
	}

	/**
	 * Generates a sentence of the following structure:<br>
	 * animal + verb + {@code numberOfNegativeWords}*(animal + negative word +
	 * stop word)
	 * 
	 * @param numberOfNegativeWords
	 *            number of negative constructs to use in the sentence
	 * @return Positive sentence on topic "Animals"
	 */
	public static String generateStructuredNegativeSentenceAnimals(int numberOfNegativeWords) {
		// main subject
		String text = getRandomElement(topicAnimals);
		// verb
		text += " " + getRandomElement(verbs);
		// generate negative opinion: topic + negative word + stop word
		for (int i = 0; i < numberOfNegativeWords; i++) {
			text += " " + getRandomElement(topicAnimals);
			text += " " + getRandomElement(negativeWords);
			text += " " + getRandomElement(stopWords);
		}

		return text;
	}
	/**
	 * Generates text using words from {@link #topicAnimals} with no valences
	 * 
	 * @param numberOfSentences
	 *            number of sentences the text will have
	 * @return
	 */
	public static String generateNeutralTextAboutAnimals(int numberOfSentences) {
		String text = new String();
		for (int i = 0; i < numberOfSentences; i++) {
			text += generateStructuredNeutralSentenceAnimals() + ". ";
		}
		return text;

	}

	/**
	 * Generates a sentence of the following structure:<br>
	 * animal + verb + animal
	 * 
	 * @return Neutral sentence on topic "Animals"
	 */
	public static String generateStructuredNeutralSentenceAnimals() {
		String text = getRandomElement(topicAnimals);
		text += " " + getRandomElement(verbs);
		text = getRandomElement(topicAnimals);
		return text;
	}

	/**
	 * Generates text using words from {@link #topicAnimals} and
	 * {@link #positiveWords}
	 * 
	 * @param numberOfSentences
	 *            number of sentences the text will have
	 * @return
	 */
	public static String generatePositiveTextAboutAnimals(int numberOfSentences) {
		String text = new String();
		for (int i = 0; i < numberOfSentences; i++) {
			text += generateStructuredPositiveSentenceAnimals(NUMBER_VALENT_OF_WORDS_IN_SENTENCE)
					+ ". ";
		}
		return text;
	}

	/**
	 * Generates a sentence of the following structure:<br>
	 * animal + verb + {@code numberOfPositiveWords} * (animal + positive word +
	 * stop word)
	 * 
	 * @param numberOfPositiveWords
	 *            number of positive constructs to use in the sentence
	 * @return Positive sentence on topic "Animals"
	 */
	public static String generateStructuredPositiveSentenceAnimals(int numberOfPositiveWords) {
		// main subject
		String text = getRandomElement(topicAnimals);
		// verb
		text += " " + getRandomElement(verbs);
		// generate negative opinion: topic + negative word + stop word
		for (int i = 0; i < numberOfPositiveWords; i++) {
			text += " " + getRandomElement(topicAnimals);
			text += " " + getRandomElement(positiveWords);
			text += " " + getRandomElement(stopWords);
		}

		return text;
	}

	/**
	 * Chooses random swears words from {@link #insults}
	 * 
	 * @param numberOfWords
	 *            number of words to concatenate
	 * @return String containing requested number of swear words or empty string
	 *         if parameter {@code numberOfWords} <= 0
	 */
	public static String generateSwears(int numberOfWords) {
		String text = "";
		for (int i = 0; i < numberOfWords; i++) {
			int randomIndex = random.nextInt(insults.size());
			text += insults.get(randomIndex) + " ";
		}
		return text;
	}

	/**
	 * Generates of sentence with the following structure: animal + verb + n *
	 * insult
	 * 
	 * @param numberValentOfWordsInSentence
	 *            number of insults to generate
	 * @return
	 */
	public static String generateSwearTextAboutAnimals(int numberValentOfWordsInSentence) {
		String text = getRandomElement(topicAnimals);
		text += " " + getRandomElement(verbs);
		text += generateSwears(numberValentOfWordsInSentence);
		return text;
	}

	/**
	 * Generates of sentence with the following structure: food + verb + n *
	 * insult
	 * 
	 * @param numberValentOfWordsInSentence
	 *            number of insults to generate
	 * @return
	 */
	public static String generateSwearTextAboutFood(int numberValentOfWordsInSentence) {
		String text = getRandomElement(topicFood);
		text += " " + getRandomElement(verbs);
		text += generateSwears(numberValentOfWordsInSentence);
		return text;
	}

	/**
	 * Retrieves a random element from a list of Strings
	 * 
	 * @param list
	 *            The list of Strings
	 * @return random element from the list
	 */
	private static String getRandomElement(List<String> list) {
		int randomIndex = random.nextInt(list.size());
		return list.get(randomIndex);
	}

	/**
	 * 
	 * @return
	 */
	public static String getPositiveSentenceAboutDaVinci() {
		return getRandomElement(daVinciPos);
	}

	/**
	 * 
	 * @return
	 */
	public static String getNegativeSentenceAboutDaVinci() {
		return getRandomElement(daVinciNeg);
	}

	/**
	 * 
	 * @return
	 */
	public static String getPositiveSentenceOther() {
		return getRandomElement(otherPos);
	}

	/**
	 * 
	 * @return
	 */
	public static String getNegativeSentenceOther() {
		return getRandomElement(otherNeg);
	}

	/**
	 * 
	 * Selects a random element from the arguments and returns it
	 * 
	 * @return random element from the arguments
	 */
	@SafeVarargs
	private static <T> T selectRandomFrom(T... args) {
		int randomIndex = random.nextInt(args.length);
		return args[randomIndex];
	}

	/**
	 * Generates a sentence with {@code numberOfWordsPerText} words from
	 * {@link #topicAnimals} {@link #negativeWords}
	 * {@link BenchmarkUtil#stopWords} and {@link BenchmarkUtil#verbs}.
	 * 
	 * @param numberOfWordsPerText
	 *            number of words to be generated in the sentence. If smaller
	 *            than <b>3</b>, the sentence will have 3 words.
	 * @return Sentence with randomly arranged words.
	 */
	public static String generateUnstructuredNegativeTextAboutAnimals(int numberOfWordsPerText) {
		if (numberOfWordsPerText < 3) {
			numberOfWordsPerText = 3;
		}
		List<String> words = new ArrayList<>(numberOfWordsPerText);
		String subject = getRandomElement(topicAnimals);
		words.add(subject);
		String verb = getRandomElement(verbs);
		words.add(verb);
		String valence = getRandomElement(negativeWords);
		words.add(valence);
		for (int i = 3; i < numberOfWordsPerText; i++) {
			List<String> randomList = selectRandomFrom(topicAnimals, verbs, negativeWords,
					stopWords);
			String word = getRandomElement(randomList);
			words.add(word);
		}
		Collections.shuffle(words, random);
		String result = "";
		for (String word : words) {
			result += (word + " ");
		}
		return result;
	}

	/**
	 * Generates a sentence with {@code numberOfWordsPerText} words from
	 * {@link #topicFood} {@link #negativeWords} {@link BenchmarkUtil#stopWords}
	 * and {@link BenchmarkUtil#verbs}.
	 * 
	 * @param numberOfWordsPerText
	 *            number of words to be generated in the sentence. If smaller
	 *            than <b>3</b>, the sentence will have 3 words.
	 * @return Sentence with randomly arranged words.
	 */
	public static String generateUnstructuredNegativeTextAboutFood(int numberOfWordsPerText) {
		if (numberOfWordsPerText < 3) {
			numberOfWordsPerText = 3;
		}
		List<String> words = new ArrayList<>(numberOfWordsPerText);
		String subject = getRandomElement(topicFood);
		words.add(subject);
		String verb = getRandomElement(verbs);
		words.add(verb);
		String valence = getRandomElement(negativeWords);
		words.add(valence);
		for (int i = 3; i < numberOfWordsPerText; i++) {
			List<String> randomList = selectRandomFrom(topicFood, verbs, negativeWords, stopWords);
			String word = getRandomElement(randomList);
			words.add(word);
		}
		Collections.shuffle(words, random);
		String result = "";
		for (String word : words) {
			result += (word + " ");
		}
		return result;
	}

	/**
	 * Generates a sentence with {@code numberOfWordsPerText} words from
	 * {@link #topicFood} {@link #topicAnimals} {@link #negativeWords}
	 * {@link BenchmarkUtil#stopWords} and {@link BenchmarkUtil#verbs}.
	 * 
	 * @param numberOfWordsPerText
	 *            number of words to be generated in the sentence. If smaller
	 *            than <b>4</b>, the sentence will have 4 words.
	 * @return Sentence with randomly arranged words.
	 */
	public static String generateUnstructuredNegativeTextAboutMixedSubjects(
			int numberOfWordsPerText) {
		if (numberOfWordsPerText < 4) {
			numberOfWordsPerText = 4;
		}
		List<String> words = new ArrayList<>(numberOfWordsPerText);
		String subject1 = getRandomElement(topicFood);
		words.add(subject1);
		String subject2 = getRandomElement(topicAnimals);
		words.add(subject2);
		String verb = getRandomElement(verbs);
		words.add(verb);
		String valence = getRandomElement(negativeWords);
		words.add(valence);
		for (int i = 3; i < numberOfWordsPerText; i++) {
			List<String> randomList = selectRandomFrom(topicAnimals, topicFood, verbs,
					negativeWords, stopWords);
			String word = getRandomElement(randomList);
			words.add(word);
		}
		Collections.shuffle(words, random);
		String result = "";
		for (String word : words) {
			result += (word + " ");
		}
		return result;
	}

	/**
	 * Generates a sentence with {@code numberOfWordsPerText} words from
	 * {@link #topicAnimals} {@link #positiveWords}
	 * {@link BenchmarkUtil#stopWords} and {@link BenchmarkUtil#verbs}.
	 * 
	 * @param numberOfWordsPerText
	 *            number of words to be generated in the sentence. If smaller
	 *            than <b>3</b>, the sentence will have 3 words.
	 * @return Sentence with randomly arranged words.
	 */
	public static String generateUnstructuredPositiveTextAboutAnimals(int numberOfWordsPerText) {
		if (numberOfWordsPerText < 3) {
			numberOfWordsPerText = 3;
		}
		List<String> words = new ArrayList<>(numberOfWordsPerText);
		String subject = getRandomElement(topicAnimals);
		words.add(subject);
		String verb = getRandomElement(verbs);
		words.add(verb);
		String valence = getRandomElement(positiveWords);
		words.add(valence);
		for (int i = 3; i < numberOfWordsPerText; i++) {
			List<String> randomList = selectRandomFrom(topicAnimals, verbs, positiveWords,
					stopWords);
			String word = getRandomElement(randomList);
			words.add(word);
		}
		Collections.shuffle(words, random);
		String result = "";
		for (String word : words) {
			result += (word + " ");
		}
		return result;
	}

	/**
	 * Generates a sentence with {@code numberOfWordsPerText} words from
	 * {@link #topicFood} {@link #positiveWords} {@link BenchmarkUtil#stopWords}
	 * and {@link BenchmarkUtil#verbs}.
	 * 
	 * @param numberOfWordsPerText
	 *            number of words to be generated in the sentence. If smaller
	 *            than <b>3</b>, the sentence will have 3 words.
	 * @return Sentence with randomly arranged words.
	 */
	public static String generateUnstructuredPositiveTextAboutFood(int numberOfWordsPerText) {
		if (numberOfWordsPerText < 3) {
			numberOfWordsPerText = 3;
		}
		List<String> words = new ArrayList<>(numberOfWordsPerText);
		String subject = getRandomElement(topicFood);
		words.add(subject);
		String verb = getRandomElement(verbs);
		words.add(verb);
		String valence = getRandomElement(positiveWords);
		words.add(valence);
		for (int i = 3; i < numberOfWordsPerText; i++) {
			List<String> randomList = selectRandomFrom(topicFood, verbs, positiveWords, stopWords);
			String word = getRandomElement(randomList);
			words.add(word);
		}
		Collections.shuffle(words, random);
		String result = "";
		for (String word : words) {
			result += (word + " ");
		}
		return result;
	}

	/**
	 * Generates a sentence with {@code numberOfWordsPerText} words from
	 * {@link #topicFood} {@link #topicAnimals} {@link #positiveWords}
	 * {@link BenchmarkUtil#stopWords} and {@link BenchmarkUtil#verbs}.
	 * 
	 * @param numberOfWordsPerText
	 *            number of words to be generated in the sentence. If smaller
	 *            than <b>4</b>, the sentence will have 4 words.
	 * @return Sentence with randomly arranged words.
	 */
	public static String generateUnstructuredPositiveTextAboutMixedSubjects(
			int numberOfWordsPerText) {
		if (numberOfWordsPerText < 4) {
			numberOfWordsPerText = 4;
		}
		List<String> words = new ArrayList<>(numberOfWordsPerText);
		String subject1 = getRandomElement(topicFood);
		words.add(subject1);
		String subject2 = getRandomElement(topicAnimals);
		words.add(subject2);
		String verb = getRandomElement(verbs);
		words.add(verb);
		String valence = getRandomElement(positiveWords);
		words.add(valence);
		for (int i = 3; i < numberOfWordsPerText; i++) {
			List<String> randomList = selectRandomFrom(topicAnimals, topicFood, verbs,
					positiveWords, stopWords);
			String word = getRandomElement(randomList);
			words.add(word);
		}
		Collections.shuffle(words, random);
		String result = "";
		for (String word : words) {
			result += (word + " ");
		}
		return result;
	}

	/**
	 * Generates a double Negation sentence of the structure <blockquote>It is
	 * not that "{@literal negativeWord}" </blockquote>
	 * 
	 * @return String containing double negation sentence
	 */
	public static String generateDoubleNegation() {
		String word = getRandomElement(negativeWords);
		String sentence = "Is is not that " + word;
		return sentence;
	}
}
