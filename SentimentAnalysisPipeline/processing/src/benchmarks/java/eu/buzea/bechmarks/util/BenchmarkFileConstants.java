/**
 * 
 */
package eu.buzea.bechmarks.util;

/**
 * Class containing file path strings as constants
 * 
 * @author Vlad-Calin Buzea
 *
 */
public final class BenchmarkFileConstants {

	/**
	 * 
	 */
	public static final String RESOURCE_FOLDER = "./src/benchmarks/resources/";
	/**
	 * 
	 */
	public static final String POSITIVE_WORDS_TXT = "Positive-words.txt";
	/**
	 * 
	 */
	public static final String SEMANTIC_FIELD_ANIMALS_TXT = "SemanticField.Animals.txt";
	/**
	 * 
	 */
	public static final String VERBS_TXT = "Verbs.txt";
	/**
	 * 
	 */
	public static final String SEMANTIC_FIELD_FOOD_TXT = "SemanticField.Food.txt";
	/**
	 * 
	 */
	public static final String STOPWORDS_TXT = "Stopwords.txt";
	/**
	 * 
	 */
	public static final String INSULTS_TXT = "Insults.txt";
	/**
	 * 
	 */
	public static final String NEGATIVE_WORDS_TXT = "Negative-words.txt";

	/**
	 * 
	 */
	public static final String KAGGE_DATA_SET_DA_VINCI_POS = "KaggleDataSet_DaVinci.pos";

	/**
	 * 
	 */
	public static final String KAGGE_DATA_SET_DA_VINCI_NEG = "KaggleDataSet_DaVinci.neg";

	/**
	 * 
	 */
	public static final String KAGGE_DATA_SET_OTHER_POS = "KaggleDataSet_Other.pos";

	/**
	 * 
	 */
	public static final String KAGGE_DATA_SET_OTHER_NEG = "KaggleDataSet_Other.neg";
	/**
	 * 
	 */
	public static final String SYNTHETIC_BENCHMARK_OUTPUT_XML = "SyntheticBenchmarkOutput.xml";

	/**
	 * Hide constructor
	 */
	private BenchmarkFileConstants() {

	}

}
