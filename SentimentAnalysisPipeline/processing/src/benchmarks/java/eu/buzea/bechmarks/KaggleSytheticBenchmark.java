/**
 * 
 */
package eu.buzea.bechmarks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.buzea.bechmarks.util.BenchmarkFileConstants;
import eu.buzea.bechmarks.util.BenchmarkUtil;
import eu.buzea.datamodel.config.PersistenceContext;
import eu.buzea.datamodel.entities.Edge;
import eu.buzea.datamodel.repositories.EdgeRepository;
import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.Input.Posts;
import eu.buzea.inputschema.Input.Users;
import eu.buzea.inputschema.User;
import eu.buzea.processing.SocialNetworkProcessing;
import eu.buzea.util.XmlUtil;

/**
 * @author Vlad-Calin Buzea
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = {PersistenceContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class KaggleSytheticBenchmark {
	@Autowired
	SocialNetworkProcessing processing;
	@Autowired
	EdgeRepository edgeRepository;
	private static final String RUN_NAME = "Kaggle-Benchmark";
	private static final int NUMBER_OF_USERS = 40;
	private static List<User> users;
	private static Input xml;

	/**
	 * Counter for time slice currently tested
	 */
	private static int currentTimeSlice;

	/**
	 * Counter for users already used. It is also equivalent to the number of
	 * user id's already used
	 */
	private static int currentUser;

	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {
		users = BenchmarkUtil.generateUsers(NUMBER_OF_USERS);
		xml = new Input();
		xml.setRunName(RUN_NAME);
		xml.setOverwritePreviousDataSet(true);
		Input.Users postUsers = new Users();
		postUsers.getUser().addAll(users);
		xml.setUsers(postUsers);
		xml.setPosts(new Posts());

	}

	/**
	 * <b>Everyone agrees on topic 1.</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and positive
	 * adjectives <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and positive adjectives Replies go up to depth 2 (reply to reply)<br>
	 * Check that graph contains K positive interactions, from the T users
	 * 
	 * @throws Exception
	 */
	@Test
	public void scenario1() throws Exception {
		Input input = new Input();
		Content mainPost = setUpFirstRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String postText = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		mainPost.setText(postText);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String replyText = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		reply.setText(replyText);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String reply2Text = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		reply2.setText(reply2Text);
		reply.getReply().add(reply2);

		processing.processInput(input);

		currentTimeSlice = 1;
		Edge edge = edgeRepository.findEdgeBefore(3, 2, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(2, 1, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(3, 1, currentTimeSlice, RUN_NAME);
		assertNull(edge);

	}

	/**
	 * <b>Disagreement on topic</b><br>
	 * 1 BenchmarkUtil.generate post using words from TOPIC 1 and positive
	 * adjectives <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and negative adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K negative interactions, from the T users
	 * 
	 * @throws DatatypeConfigurationException
	 */
	@Test
	public void scenario2() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String postText = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		mainPost.setText(postText);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String replyText = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply.setText(replyText);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String subReplyText = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		reply2.setText(subReplyText);
		reply.getReply().add(reply2);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(5, 4, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(6, 5, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(6, 4, currentTimeSlice, RUN_NAME);
		assertNull(edge);

	}

	/**
	 * <b>Everyone disagrees case 2</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and negative
	 * adjectives <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and positive adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K negative interactions, from the T users
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario3() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextDaVinci = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		mainPost.setText(negativeTextDaVinci);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextDaVinci = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		reply.setText(positiveTextDaVinci);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextDaVinci2 = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply2.setText(negativeTextDaVinci2);
		reply.getReply().add(reply2);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(8, 7, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(9, 8, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(9, 7, currentTimeSlice, RUN_NAME);
		assertNull(edge);
	}

	/**
	 * <b>Negative responses-> Agreement</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and negative
	 * adjectives<br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and negative adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K positive interactions, from the T users<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario4() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextDaVinci = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		mainPost.setText(negativeTextDaVinci);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextDaVinci2 = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply.setText(negativeTextDaVinci2);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextDaVinci3 = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply2.setText(negativeTextDaVinci3);
		reply.getReply().add(reply2);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(11, 10, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(12, 11, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(12, 10, currentTimeSlice, RUN_NAME);
		assertNull(edge);
	}

	/**
	 * <b>Off topic responses</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and no adjectives
	 * <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 2
	 * and both positive and negative adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K neutral interactions, from the T users<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario5() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String neutralTextDaVinci = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		mainPost.setText(neutralTextDaVinci);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String neutralTextOther = BenchmarkUtil.getPositiveSentenceOther();
		reply.setText(neutralTextOther);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextOther = BenchmarkUtil.getNegativeSentenceOther();
		reply2.setText(negativeTextOther);
		mainPost.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(++currentUser);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextOther = BenchmarkUtil.getPositiveSentenceOther();
		reply3.setText(positiveTextOther);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(14, 13, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(15, 13, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(16, 13, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(16, 14, currentTimeSlice, RUN_NAME);
		assertNull(edge);

		// edge = edgeRepository.findEdgeBefore(16, 15, currentTimeSlice,
		// RUN_NAME);
		// assertNull(edge);
		//
		// edge = edgeRepository.findEdgeBefore(15, 14, currentTimeSlice,
		// RUN_NAME);
		// assertNull(edge);
	}

	/**
	 * <b>Comment to previous sibling</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and no adjectives
	 * <br>
	 * BenchmarkUtil.generate 1 reply at depth 1 using words from TOPIC 2 and
	 * positive adjectives, by user T1<br>
	 * 
	 * BenchmarkUtil.generate K replies at depth 2 using words from TOPIC 2 and
	 * positive adjectives, by users T11-T1k<br>
	 * 
	 * BenchmarkUtil.generate 2nd reply at depth 2 using words from TOPIC 2 and
	 * negative adjectives, by user T2<br>
	 * Check that user T2 has K+1 negative interactions with (T1, T11-T1k)<br>
	 * Check that user T1 has K positive interactions with T11-T1k<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario6() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String mainPostText = BenchmarkUtil.getPositiveSentenceOther();
		mainPost.setText(mainPostText);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String replyText1 = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		reply.setText(replyText1);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String replyText2 = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		reply2.setText(replyText2);
		reply.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(++currentUser);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextOther = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply3.setText(negativeTextOther);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(19, 18, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(20, 18, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(20, 19, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(18, 17, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());
	}

	/**
	 * <b>Swearing</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and negative
	 * adjectives<br>
	 * BenchmarkUtil.generate K replies from T users, using swear words<br>
	 * All K replies are counted as negative interactions<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario7() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextDaVinci = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		mainPost.setText(negativeTextDaVinci);

		Content reply3 = new Content();
		reply3.setAuthorId(++currentUser);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String swears = BenchmarkUtil
				.generateSwears(BenchmarkUtil.NUMBER_VALENT_OF_WORDS_IN_SENTENCE);
		reply3.setText(swears);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(currentUser, (currentUser - 1), currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());

	}

	/**
	 * <b>"Yes, but  (no)</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and no adjectives
	 * <br>
	 * BenchmarkUtil.generate one reply using words from TOPIC 1 and both
	 * POSITIVE and NEGATIVE adjectives. The adjectives will not be mixed and
	 * will be separated by the word but<br>
	 * We will check that the algorithm counts two interactions: one positive
	 * and one negative<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario8() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String postText = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		mainPost.setText(postText);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String replyText = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
		replyText += " But, ";
		replyText += BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply.setText(replyText);
		mainPost.getReply().add(reply);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(currentUser, (currentUser - 1), currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightPositive());
	}

	/**
	 * <b>Lost Interaction</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and positive
	 * adjectives<br>
	 * BenchmarkUtil.generate first reply from user U1 using words from TOPIC 2
	 * and negative adjectives<br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and positive adjectives<br>
	 * BenchmarkUtil.generate K-th reply from user U2, using words from TOPIC 2
	 * and negative adjectives<br>
	 * All replies are depth 1<br>
	 * Check that interaction between U1 and U2 is lost <br>
	 * Replies from U1 and U2 are marked as neutral in reaction to the main
	 * thread<br>
	 * The K replies between U1 and U2 are counted as positive interactions<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario9() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(++currentUser);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextDaVinci = BenchmarkUtil.getPositiveSentenceOther();
		mainPost.setText(positiveTextDaVinci);

		Content reply = new Content();
		reply.setAuthorId(++currentUser);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextOther = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply.setText(negativeTextOther);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(++currentUser);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextDaVinci2 = BenchmarkUtil.getPositiveSentenceOther();
		reply2.setText(positiveTextDaVinci2);
		mainPost.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(++currentUser);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextDaVinci3 = BenchmarkUtil.getPositiveSentenceOther();
		reply3.setText(positiveTextDaVinci3);
		mainPost.getReply().add(reply3);

		Content reply4 = new Content();
		reply4.setAuthorId(++currentUser);
		reply4.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextOther2 = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
		reply4.setText(negativeTextOther2);
		mainPost.getReply().add(reply4);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(currentUser, (currentUser - 3), currentTimeSlice,
				RUN_NAME);
		assertNull(edge); // further development, must detect edge

		edge = edgeRepository.findEdgeBefore((currentUser - 3), (currentUser - 4), currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(currentUser, (currentUser - 4), currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		 assertEquals(1, edge.getWeightNeutral());
		//assertEquals(1, edge.getWeightPositive());
	}

	@AfterClass
	public static void afterClass() throws Exception {
		File outputFile = new File(BenchmarkFileConstants.RESOURCE_FOLDER,
				BenchmarkFileConstants.SYNTHETIC_BENCHMARK_OUTPUT_XML);
		XmlUtil.serialize(xml, outputFile);
	}

	/**
	 * Sets up first run, that overwrites previous data in database
	 * 
	 * @param input
	 *            Input containing users and just one single post
	 * @return the main post in the input
	 */
	private Content setUpFirstRun(Input input) {
		Content mainPost = new Content();
		input.setRunName(RUN_NAME);
		input.setOverwritePreviousDataSet(true);
		Input.Users postUsers = new Users();
		postUsers.getUser().addAll(users);
		input.setUsers(postUsers);
		input.setPosts(new Posts());
		input.getPosts().getPost().add(mainPost);
		xml.getPosts().getPost().add(mainPost);
		return mainPost;
	}

	/**
	 * Sets up run, that adds new time slices to previous data
	 * 
	 * @param input
	 *            Input containing users and just one single post
	 * @return the main post in the input
	 */
	private Content setUpUlteriorRun(Input input) {
		Content mainPost = new Content();
		input.setRunName(RUN_NAME);
		input.setOverwritePreviousDataSet(false);
		Input.Users postUsers = new Users();
		postUsers.getUser().addAll(users);
		input.setUsers(postUsers);
		Input.Posts posts = new Posts();
		input.setPosts(posts);
		List<Content> postList = posts.getPost();
		postList.add(mainPost);
		xml.getPosts().getPost().add(mainPost);
		return mainPost;
	}

}
