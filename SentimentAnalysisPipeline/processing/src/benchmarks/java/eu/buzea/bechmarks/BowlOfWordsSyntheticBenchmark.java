package eu.buzea.bechmarks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.buzea.bechmarks.util.BenchmarkUtil;
import eu.buzea.datamodel.config.PersistenceContext;
import eu.buzea.datamodel.entities.Edge;
import eu.buzea.datamodel.repositories.EdgeRepository;
import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.inputschema.Input.Posts;
import eu.buzea.inputschema.Input.Users;
import eu.buzea.inputschema.User;
import eu.buzea.processing.SocialNetworkProcessing;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = {PersistenceContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
// @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
/**
 * Tests if the processing component performs as expected
 * 
 * @author Vlad
 *
 */
public class BowlOfWordsSyntheticBenchmark {

	@Autowired
	SocialNetworkProcessing processing;
	@Autowired
	EdgeRepository edgeRepository;
	private static final String RUN_NAME = "Synthetic-Benchmark";
	private static final int NUMBER_OF_USERS = 40;
	private static List<User> users;

	/**
	 * Counter for time slice currently tested
	 */
	private static int currentTimeSlice;

	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {
		users = BenchmarkUtil.generateUsers(NUMBER_OF_USERS);
	}

	/**
	 * <b>Everyone agrees on topic 1.</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and positive
	 * adjectives <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and positive adjectives Replies go up to depth 2 (reply to reply)<br>
	 * Check that graph contains K positive interactions, from the T users
	 * 
	 * @throws Exception
	 */
	@Test
	public void scenario1() throws Exception {
		Input input = new Input();
		Content mainPost = setUpFirstRun(input);

		mainPost.setAuthorId(1);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals1 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		mainPost.setText(positiveTextAnimals1);

		Content reply = new Content();
		reply.setAuthorId(2);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals2 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		reply.setText(positiveTextAnimals2);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(3);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals3 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		reply2.setText(positiveTextAnimals3);
		reply.getReply().add(reply2);

		processing.processInput(input);

		currentTimeSlice = 1;
		Edge edge = edgeRepository.findEdgeBefore(3, 2, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(2, 1, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(3, 1, currentTimeSlice, RUN_NAME);
		assertNull(edge);

	}

	/**
	 * <b>Disagreement on topic</b><br>
	 * 1 BenchmarkUtil.generate post using words from TOPIC 1 and positive
	 * adjectives <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and negative adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K negative interactions, from the T users
	 * 
	 * @throws DatatypeConfigurationException
	 */
	@Test
	public void scenario2() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(4);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals1 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		mainPost.setText(positiveTextAnimals1);

		Content reply = new Content();
		reply.setAuthorId(5);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		reply.setText(negativeTextAnimals);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(6);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals3 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		reply2.setText(positiveTextAnimals3);
		reply.getReply().add(reply2);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(5, 4, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(6, 5, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(6, 4, currentTimeSlice, RUN_NAME);
		assertNull(edge);

	}

	/**
	 * <b>Everyone disagrees case 2</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and negative
	 * adjectives <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and positive adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K negative interactions, from the T users
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario3() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(7);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		mainPost.setText(negativeTextAnimals);

		Content reply = new Content();
		reply.setAuthorId(8);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		reply.setText(positiveTextAnimals);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(9);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals2 = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		reply2.setText(negativeTextAnimals2);
		reply.getReply().add(reply2);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(8, 7, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(9, 8, currentTimeSlice, RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(9, 7, currentTimeSlice, RUN_NAME);
		assertNull(edge);
	}

	/**
	 * <b>Negative responses-> Agreement</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and negative
	 * adjectives<br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and negative adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K positive interactions, from the T users<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario4() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(10);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		mainPost.setText(negativeTextAnimals);

		Content reply = new Content();
		reply.setAuthorId(11);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals2 = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		reply.setText(negativeTextAnimals2);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(12);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals3 = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		reply2.setText(negativeTextAnimals3);
		reply.getReply().add(reply2);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(11, 10, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(12, 11, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(0, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(12, 10, currentTimeSlice,
				RUN_NAME);
		assertNull(edge);
	}

	/**
	 * <b>Off topic responses</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and no adjectives
	 * <br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 2
	 * and both positive and negative adjectives<br>
	 * Replies go up to depth 2 (reply to reply) <br>
	 * Check that graph contains K neutral interactions, from the T users<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario5() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(13);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String neutralTextAnimals = BenchmarkUtil
				.generateNeutralTextAboutAnimals(2);
		mainPost.setText(neutralTextAnimals);

		Content reply = new Content();
		reply.setAuthorId(14);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String neutralTextFood = BenchmarkUtil.generateNeutralTextAboutFood(2);
		reply.setText(neutralTextFood);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(15);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextFood = BenchmarkUtil
				.generateNegativeTextAboutFood(2);
		reply2.setText(negativeTextFood);
		mainPost.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(16);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextFood = BenchmarkUtil
				.generatePositiveTextAboutFood(2);
		reply3.setText(positiveTextFood);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(14, 13, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(15, 13, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(16, 13, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(16, 14, currentTimeSlice,
				RUN_NAME);
		assertNull(edge);

		// edge = edgeRepository.findEdgeBefore(16, 15, currentTimeSlice,
		// RUN_NAME);
		// assertNull(edge);
		//
		// edge = edgeRepository.findEdgeBefore(15, 14, currentTimeSlice,
		// RUN_NAME);
		// assertNull(edge);
	}

	/**
	 * <b>Comment to previous sibling</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and no adjectives
	 * <br>
	 * BenchmarkUtil.generate 1 reply at depth 1 using words from TOPIC 2 and
	 * positive adjectives, by user T1<br>
	 * 
	 * BenchmarkUtil.generate K replies at depth 2 using words from TOPIC 2 and
	 * positive adjectives, by users T11-T1k<br>
	 * 
	 * BenchmarkUtil.generate 2nd reply at depth 2 using words from TOPIC 2 and
	 * negative adjectives, by user T2<br>
	 * Check that user T2 has K+1 negative interactions with (T1, T11-T1k)<br>
	 * Check that user T1 has K positive interactions with T11-T1k<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario6() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(17);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String neutralTextAnimals = BenchmarkUtil
				.generateNeutralTextAboutAnimals(2);
		mainPost.setText(neutralTextAnimals);

		Content reply = new Content();
		reply.setAuthorId(18);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextFood = BenchmarkUtil
				.generatePositiveTextAboutFood(2);
		reply.setText(positiveTextFood);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(19);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextFood2 = BenchmarkUtil
				.generatePositiveTextAboutFood(2);
		reply2.setText(positiveTextFood2);
		reply.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(20);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextFood = BenchmarkUtil
				.generateNegativeTextAboutFood(2);
		reply3.setText(negativeTextFood);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(19, 18, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(0, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(20, 18, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(20, 19, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(18, 17, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(0, edge.getWeightPositive());
	}

	/**
	 * <b>Lost Interaction</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and positive
	 * adjectives<br>
	 * BenchmarkUtil.generate first reply from user U1 using words from TOPIC 2
	 * and negative adjectives<br>
	 * BenchmarkUtil.generate K replies from T users, using words from TOPIC 1
	 * and positive adjectives<br>
	 * BenchmarkUtil.generate K-th reply from user U2, using words from TOPIC 2
	 * and negative adjectives<br>
	 * All replies are depth 1<br>
	 * Check that interaction between U1 and U2 is lost <br>
	 * Replies from U1 and U2 are marked as neutral in reaction to the main
	 * thread<br>
	 * The K replies between U1 and U2 are counted as positive interactions<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario7() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(21);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		mainPost.setText(positiveTextAnimals);

		Content reply = new Content();
		reply.setAuthorId(22);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextFood = BenchmarkUtil
				.generateNegativeTextAboutFood(2);
		reply.setText(negativeTextFood);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(23);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals2 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		reply2.setText(positiveTextAnimals2);
		mainPost.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(24);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals3 = BenchmarkUtil
				.generatePositiveTextAboutAnimals(2);
		reply3.setText(positiveTextAnimals3);
		mainPost.getReply().add(reply3);

		Content reply4 = new Content();
		reply4.setAuthorId(25);
		reply4.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextFood2 = BenchmarkUtil
				.generateNegativeTextAboutFood(2);
		reply4.setText(negativeTextFood2);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(22, 21, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(23, 21, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(24, 21, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(25, 21, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		// assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(25, 22, currentTimeSlice,
				RUN_NAME);
		assertNull(edge);
	}

	/**
	 * <b>Swearing</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and negative
	 * adjectives<br>
	 * BenchmarkUtil.generate K replies from T users, using words from both
	 * TOPIC 1 and TOPIC 2 and both positive and negative adjectives AND swear
	 * words<br>
	 * All K replies are counted as negative interactions<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario8() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(26);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String negativeTextAnimals = BenchmarkUtil
				.generateNegativeTextAboutAnimals(2);
		mainPost.setText(negativeTextAnimals);

		Content reply = new Content();
		reply.setAuthorId(27);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String swearTextFood = BenchmarkUtil.generateSwearTextAboutFood(
				BenchmarkUtil.NUMBER_VALENT_OF_WORDS_IN_SENTENCE);
		reply.setText(swearTextFood);
		mainPost.getReply().add(reply);

		Content reply2 = new Content();
		reply2.setAuthorId(28);
		reply2.setDate(BenchmarkUtil.getCurrentDate());
		String swearTextAnimals = BenchmarkUtil.generateSwearTextAboutAnimals(
				BenchmarkUtil.NUMBER_VALENT_OF_WORDS_IN_SENTENCE);
		reply2.setText(swearTextAnimals);
		mainPost.getReply().add(reply2);

		Content reply3 = new Content();
		reply3.setAuthorId(29);
		reply3.setDate(BenchmarkUtil.getCurrentDate());
		String positiveTextAnimals3 = BenchmarkUtil.generateSwears(
				BenchmarkUtil.NUMBER_VALENT_OF_WORDS_IN_SENTENCE);
		reply3.setText(positiveTextAnimals3);
		mainPost.getReply().add(reply3);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(27, 26, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(28, 26, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());

		edge = edgeRepository.findEdgeBefore(29, 26, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		// assertEquals(1, edge.getWeightNeutral());
		// assertEquals(1, edge.getWeightPositive());
	}

	/**
	 * <b>"Yes, but … (no)”</b><br>
	 * BenchmarkUtil.generate post using words from TOPIC 1 and no adjectives
	 * <br>
	 * BenchmarkUtil.generate one reply using words from TOPIC 1 and both
	 * POSITIVE and NEGATIVE adjectives. The adjectives will not be mixed and
	 * will be separated by the word “but”<br>
	 * We will check that the algorithm counts two interactions: one positive
	 * and one negative<br>
	 * 
	 * @throws DatatypeConfigurationException
	 * 
	 */
	@Test
	public void scenario9() throws DatatypeConfigurationException {
		Input input = new Input();
		Content mainPost = setUpUlteriorRun(input);

		mainPost.setAuthorId(30);
		mainPost.setDate(BenchmarkUtil.getCurrentDate());
		String postText = BenchmarkUtil.generateNeutralTextAboutAnimals(2);
		mainPost.setText(postText);

		Content reply = new Content();
		reply.setAuthorId(31);
		reply.setDate(BenchmarkUtil.getCurrentDate());
		String replyText = BenchmarkUtil.generatePositiveTextAboutAnimals(1);
		replyText += "But, ";
		replyText += BenchmarkUtil.generateNegativeTextAboutAnimals(1);
		reply.setText(replyText);
		mainPost.getReply().add(reply);

		processing.processInput(input);

		// evaluation
		currentTimeSlice++;

		Edge edge = edgeRepository.findEdgeBefore(31, 30, currentTimeSlice,
				RUN_NAME);
		assertNotNull(edge);
		assertEquals(1, edge.getWeightNegative());
		assertEquals(1, edge.getWeightPositive());
	}

	/**
	 * Sets up first run, that overwrites previous data in database
	 * 
	 * @param input
	 *            Input containing users and just one single post
	 * @return the main post in the input
	 */
	private Content setUpFirstRun(Input input) {
		Content mainPost = new Content();
		input.setRunName(RUN_NAME);
		input.setOverwritePreviousDataSet(true);
		Input.Users postUsers = new Users();
		postUsers.getUser().addAll(users);
		input.setUsers(postUsers);
		Input.Posts posts = new Posts();
		input.setPosts(posts);
		List<Content> postList = posts.getPost();
		postList.add(mainPost);
		return mainPost;
	}

	/**
	 * Sets up run, that adds new time slices to previous data
	 * 
	 * @param input
	 *            Input containing users and just one single post
	 * @return the main post in the input
	 */
	private Content setUpUlteriorRun(Input input) {
		Content mainPost = new Content();
		input.setRunName(RUN_NAME);
		input.setOverwritePreviousDataSet(false);
		Input.Users postUsers = new Users();
		postUsers.getUser().addAll(users);
		input.setUsers(postUsers);
		Input.Posts posts = new Posts();
		input.setPosts(posts);
		List<Content> postList = posts.getPost();
		postList.add(mainPost);
		return mainPost;
	}

}
