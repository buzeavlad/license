/**
 * 
 */
package eu.buzea.bechmarks;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cb.services.alchemy.AlchemySentimentAnalysisService;
import com.cb.services.monkeylearn.MonkeyLearnSentimentAnalysisService;

import eu.buzea.bechmarks.util.BenchmarkUtil;
import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.alyen.AlyenSentimentAnalysisService;
import eu.buzea.services.datumbox.DatumboxSentimentAnalysisService;
import eu.buzea.services.heavenondemand.HeavenOnDemandSentimentAnalysisService;
import eu.buzea.services.indico.IndicoSentimentAnalysisService;
import eu.buzea.services.sentigem.SentigemSentimentAnalysisService;
import eu.buzea.services.vivekn.ViveknSentimentAnalysisService;

/**
 * @author Vlad-Calin Buzea
 *
 */
public class SentimentAnalysisServiceBenchmark {

	public static final int NUMBER_OF_SAMPLES_PER_TEST = 25;
	public static final Class<?>[] modules = {AlchemySentimentAnalysisService.class,
			ViveknSentimentAnalysisService.class, SentigemSentimentAnalysisService.class,
			AlyenSentimentAnalysisService.class, MonkeyLearnSentimentAnalysisService.class,
			DatumboxSentimentAnalysisService.class, HeavenOnDemandSentimentAnalysisService.class,
			IndicoSentimentAnalysisService.class};
	private static List<SentimentAnalysisService> instances;
	private static double[][] relativeScore;
	private static int measurementNumber;
	private static int numberOfModules;
	private static final Logger LOG = LoggerFactory.getLogger("BenchmarkLogger");

	@BeforeClass
	public static void beforeClass() throws InstantiationException, IllegalAccessException {
		measurementNumber = -1;
		numberOfModules = modules.length;
		int numberOfBenchmarkMethods = 0;
		Method[] methods = SentimentAnalysisServiceBenchmark.class.getDeclaredMethods();
		for (Method m : methods) {
			if (m.isAnnotationPresent(Test.class))
				numberOfBenchmarkMethods++;
		}
		relativeScore = new double[numberOfModules][numberOfBenchmarkMethods];
		instances = new ArrayList<>(numberOfModules);
		for (Class<?> c : modules) {
			SentimentAnalysisService instance = (SentimentAnalysisService) c.newInstance();
			instances.add(instance);
		}
	}

	/**
	 * Benchmark checking for most likely scenario data
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	@Test
	public void benchmarkAnnotatedData() throws Exception {

		measurementNumber++;
		int[] absoluteScore = new int[numberOfModules];
		for (int k = 0; k < NUMBER_OF_SAMPLES_PER_TEST; k++) {
			String textNegative1 = BenchmarkUtil.getNegativeSentenceAboutDaVinci();
			String textNegative2 = BenchmarkUtil.getNegativeSentenceOther();
			String textPositive1 = BenchmarkUtil.getPositiveSentenceAboutDaVinci();
			String textPositive2 = BenchmarkUtil.getPositiveSentenceOther();
			// test all services with the same 4 strings
			for (int i = 0; i < numberOfModules; i++) {
				SentimentAnalysisService module = instances.get(i);

				Sentiment s = module.process(textNegative1);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}
				s = module.process(textNegative2);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textPositive1);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textPositive2);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;

				}

			}
		}
		LOG.info("\nAnnotated Sentences: ");
		for (int i = 0; i < numberOfModules; i++) {
			relativeScore[i][measurementNumber] = ((double) absoluteScore[i]
					/ (4 * NUMBER_OF_SAMPLES_PER_TEST)) * 100.0;
			LOG.info(instances.get(i).getClass().getSimpleName() + " = "
					+ relativeScore[i][measurementNumber] + "%");
		}

	}

	/**
	 * Benchmark checking for partially unfamiliar data
	 */
	@Test
	public void benchmarkGeneratedStructuredSentence() {
		measurementNumber++;
		int[] absoluteScore = new int[numberOfModules];
		for (int k = 0; k < NUMBER_OF_SAMPLES_PER_TEST; k++) {
			String textNegative1 = BenchmarkUtil.generateNegativeTextAboutAnimals(2);
			String textNegative2 = BenchmarkUtil.generateNegativeTextAboutFood(2);
			String textPositive1 = BenchmarkUtil.generatePositiveTextAboutAnimals(2);
			String textPositive2 = BenchmarkUtil.generatePositiveTextAboutFood(2);
			// test all services with the same 4 strings
			for (int i = 0; i < numberOfModules; i++) {
				SentimentAnalysisService module = instances.get(i);

				Sentiment s = module.process(textNegative1);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}
				s = module.process(textNegative2);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textPositive1);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textPositive2);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;

				}

			}
		}
		LOG.info("\nGenerated Structured Sentences: ");
		for (int i = 0; i < numberOfModules; i++) {
			relativeScore[i][measurementNumber] = ((double) absoluteScore[i]
					/ (4 * NUMBER_OF_SAMPLES_PER_TEST)) * 100.0;
			LOG.info(instances.get(i).getClass().getSimpleName() + " = "
					+ relativeScore[i][measurementNumber] + "%");
		}
	}

	/**
	 * Benchmark checking for response to new catch phrases
	 */
	@Test
	public void benchmarkGeneratedUnstructuredSentence() {
		measurementNumber++;
		int numberOfWordsPerText = 10;
		int[] absoluteScore = new int[numberOfModules];
		for (int k = 0; k < NUMBER_OF_SAMPLES_PER_TEST; k++) {
			String textNegative1 = BenchmarkUtil
					.generateUnstructuredNegativeTextAboutAnimals(numberOfWordsPerText);
			String textNegative2 = BenchmarkUtil
					.generateUnstructuredNegativeTextAboutFood(numberOfWordsPerText);
			String textNegative3 = BenchmarkUtil
					.generateUnstructuredNegativeTextAboutMixedSubjects(numberOfWordsPerText);
			String textPositive1 = BenchmarkUtil
					.generateUnstructuredPositiveTextAboutAnimals(numberOfWordsPerText);
			String textPositive2 = BenchmarkUtil
					.generateUnstructuredPositiveTextAboutFood(numberOfWordsPerText);
			String textPositive3 = BenchmarkUtil
					.generateUnstructuredPositiveTextAboutMixedSubjects(numberOfWordsPerText);

			// test all services with the same 4 strings
			for (int i = 0; i < numberOfModules; i++) {
				SentimentAnalysisService module = instances.get(i);

				Sentiment s = module.process(textNegative1);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}
				s = module.process(textNegative2);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textNegative3);
				if (Sentiment.NEGATIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textPositive1);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;
				}

				s = module.process(textPositive2);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;

				}

				s = module.process(textPositive3);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;

				}

			}
		}
		LOG.info("\nGenerated Unstructured Sentences: ");
		for (int i = 0; i < numberOfModules; i++) {
			relativeScore[i][measurementNumber] = ((double) absoluteScore[i]
					/ (6 * NUMBER_OF_SAMPLES_PER_TEST)) * 100.0;
			LOG.info(instances.get(i).getClass().getSimpleName() + " = "
					+ relativeScore[i][measurementNumber] + "%");
		}
	}

	@Test
	public void benchmarkDoubleNegation() {
		measurementNumber++;
		int[] absoluteScore = new int[numberOfModules];
		for (int k = 0; k < NUMBER_OF_SAMPLES_PER_TEST; k++) {
			String doubleNegation = BenchmarkUtil.generateDoubleNegation();
			// test all services with the same 4 strings
			for (int i = 0; i < numberOfModules; i++) {
				SentimentAnalysisService module = instances.get(i);

				Sentiment s = module.process(doubleNegation);
				if (Sentiment.POSITIVE.equals(s)) {
					absoluteScore[i]++;
				}

			}
		}
		LOG.info("\nGenerated Double Negations: ");
		for (int i = 0; i < numberOfModules; i++) {
			relativeScore[i][measurementNumber] = ((double) absoluteScore[i]
					/ (NUMBER_OF_SAMPLES_PER_TEST)) * 100.0;
			LOG.info(instances.get(i).getClass().getSimpleName() + " = "
					+ relativeScore[i][measurementNumber] + "%");
		}
	}

	@AfterClass
	public static void afterClass() {
		measurementNumber++;
		LOG.info("\nAverages:");
		double[] averageScore = new double[numberOfModules];
		for (int i = 0; i < numberOfModules; i++) {
			for (int j = 0; j < measurementNumber; j++) {
				averageScore[i] += relativeScore[i][j];
			}
			averageScore[i] = averageScore[i] / measurementNumber;
			LOG.info(instances.get(i).getClass().getSimpleName() + " = " + averageScore[i] + "%");
		}
	}

}
