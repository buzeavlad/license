package eu.buzea.datamodel.entities;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Entity mapping the Neo4j User Node<br>
 * Represents a User in Time Varying Graph (TVG)
 * 
 * @author Vlad
 *
 */
@NodeEntity(label = "User")
public class UserNode {

	/**
	 * Neo4j internal ID. Automatically set.
	 */
	@GraphId
	private Long id;

	/**
	 * Name of the user
	 */
	private String name;

	/**
	 * Social network id. Unique identifier of a user in the dataset
	 */
	private long socialNetworkId;

	public UserNode() {

	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (socialNetworkId ^ (socialNetworkId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserNode other = (UserNode) obj;
		if (socialNetworkId != other.socialNetworkId)
			return false;
		return true;
	}

	public long getSocialNetworkId() {
		return socialNetworkId;
	}

	public void setSocialNetworkId(long socialNetworkId) {
		this.socialNetworkId = socialNetworkId;
	}

	@Override
	public String toString() {
		return "UserNode [id=" + id + ", name=" + name + ", socialNetworkId="
				+ socialNetworkId + "]";
	}

}
