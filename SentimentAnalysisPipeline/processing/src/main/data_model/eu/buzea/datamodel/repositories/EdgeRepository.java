package eu.buzea.datamodel.repositories;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import eu.buzea.datamodel.entities.Edge;

/**
 * Edge related queries and CRUD operations
 * 
 * @author Vlad
 *
 */
@Repository
public interface EdgeRepository extends GraphRepository<Edge> {

	/**
	 * Finds the latest edge present between the two users, before the specified
	 * time value.
	 * 
	 * @param startNodeId
	 *            The id of the start node
	 * @param endNodeId
	 *            The id of the end node
	 * @param timeValue
	 *            The time value before which to search for the edge
	 * @param runName
	 *            The name of the dataset in which to search
	 * @return The edge if exists, null otherwise
	 */
	@Query("match (r:Run{name: {3} })-[:CONTAINS_TIME]->(t:Time),(t)-[:CONTAINS_EDGE]->(e:Edge),(e)-[:START_NODE]->(startNode:User{socialNetworkId:({0})}), (e)-[:END_NODE]->(endNode:User{socialNetworkId:({1}) }) where t.value <= {2} RETURN e ORDER BY t.value DESC LIMIT 1; ")
	Edge findEdgeBefore(long startNodeId, long endNodeId, long timeValue,
			String runName);

}
