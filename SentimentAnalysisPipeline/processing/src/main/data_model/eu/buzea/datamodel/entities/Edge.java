package eu.buzea.datamodel.entities;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Entity mapping the Neo4j Edge Node<br>
 * Represents an edge in Time Varying Graph (TVG)
 * 
 * @author Vlad
 *
 */
@NodeEntity(label = "Edge")
public class Edge {
	/**
	 * Neo4j internal ID. Automatically set.
	 */
	@GraphId
	private Long id;

	/**
	 * Start node; the user who performed the interaction.
	 */
	@Relationship(type = "START_NODE", direction = "OUTGOING")
	private UserNode startNode;

	/**
	 * End node; the user who was the target of the interaction.
	 */
	@Relationship(type = "END_NODE", direction = "OUTGOING")
	private UserNode endNode;

	/**
	 * Time slice in which the edge was updated
	 */
	@Relationship(type = "CONTAINS_EDGE", direction = "INCOMING")
	private Time time;

	/**
	 * Edge weights
	 */
	private long weightNegative, weightPositive, weightNeutral;

	public Edge() {

	}

	public Long getId() {
		return id;
	}

	public UserNode getStartNode() {
		return startNode;
	}

	public void setStartNode(UserNode startNode) {
		this.startNode = startNode;
	}

	public UserNode getEndNode() {
		return endNode;
	}

	public void setEndNode(UserNode endNode) {
		this.endNode = endNode;
	}

	public long getWeightNegative() {
		return weightNegative;
	}

	public void setWeightNegative(long weightNegative) {
		this.weightNegative = weightNegative;
	}

	public long getWeightPositive() {
		return weightPositive;
	}

	public void setWeightPositive(long weightPositive) {
		this.weightPositive = weightPositive;
	}

	public long getWeightNeutral() {
		return weightNeutral;
	}

	public void setWeightNeutral(long weightNeutral) {
		this.weightNeutral = weightNeutral;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endNode == null) ? 0 : endNode.hashCode());
		result = prime * result
				+ ((startNode == null) ? 0 : startNode.hashCode());
		result = prime * result
				+ (int) (weightNegative ^ (weightNegative >>> 32));
		result = prime * result
				+ (int) (weightNeutral ^ (weightNeutral >>> 32));
		result = prime * result
				+ (int) (weightPositive ^ (weightPositive >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (endNode == null) {
			if (other.endNode != null)
				return false;
		} else if (!endNode.equals(other.endNode))
			return false;
		if (startNode == null) {
			if (other.startNode != null)
				return false;
		} else if (!startNode.equals(other.startNode))
			return false;
		if (weightNegative != other.weightNegative)
			return false;
		if (weightNeutral != other.weightNeutral)
			return false;
		if (weightPositive != other.weightPositive)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Edge [id=" + id + ", startNode=" + startNode + ", endNode="
				+ endNode + ", weightNegative=" + weightNegative
				+ ", weightPositive=" + weightPositive + ", weightNeutral="
				+ weightNeutral + "]";
	}

}
