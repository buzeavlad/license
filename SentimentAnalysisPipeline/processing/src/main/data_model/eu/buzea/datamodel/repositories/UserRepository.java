package eu.buzea.datamodel.repositories;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import eu.buzea.datamodel.entities.UserNode;

/**
 * User related queries and CRUD operations
 * 
 * @author Vlad
 *
 */
@Repository
public interface UserRepository extends GraphRepository<UserNode> {

	/**
	 * Finds User in the given dataset, based on id
	 * 
	 * @param socialNetworkId
	 *            id of the user to be found
	 * @param runName
	 *            Name of the dataset in which to look
	 * @return the User if found, null otherwise
	 */
	@Query("match (r:Run{name: {1} })-[]->(t),(t)-[]->(e),(e)-[]->(u:User {socialNetworkId: ({0}) }) return u;")
	UserNode findByRunAndSocialNetworkId(long socialNetworkId, String runName);
}
