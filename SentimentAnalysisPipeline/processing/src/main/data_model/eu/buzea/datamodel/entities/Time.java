package eu.buzea.datamodel.entities;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Entity mapping the Neo4j Time Node<br>
 * Represents time slice in Time Varying Graph (TVG)
 * 
 * @author Vlad
 *
 */
@NodeEntity
public class Time {

	/**
	 * Neo4j internal ID. Automatically set.
	 */
	@GraphId
	private Long id;

	/**
	 * Time value
	 */
	private long value;

	/**
	 * Content of the post related to this time entity/node
	 */
	private String postText;

	/**
	 * Data set containing this time entity/node
	 */
	@Relationship(type = "CONTAINS_TIME", direction = "INCOMING")
	private Run parent;

	public Time() {

	}

	public Time(long value) {
		super();
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (value ^ (value >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Time other = (Time) obj;
		if (value != other.value)
			return false;
		return true;
	}

	public Run getParent() {
		return parent;
	}

	public void setParent(Run parent) {
		this.parent = parent;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

}
