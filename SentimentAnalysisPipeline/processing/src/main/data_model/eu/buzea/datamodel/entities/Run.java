package eu.buzea.datamodel.entities;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Entity mapping the Neo4j Run Node<br>
 * Represents one graph/dataset in Time Varying Graph (TVG)
 * 
 * @author Vlad
 *
 */
@NodeEntity(label = "Run")
public class Run {
	/**
	 * Neo4j internal ID. Automatically set.
	 */
	@GraphId
	private Long id;

	/**
	 * Name of the dataset (example: Karate Club) Uniquely identifies each
	 * dataset
	 */
	@Index(unique = true)
	private String name;

	/**
	 * Description of data set being analyzed
	 */
	private String description;

	public Run() {

	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Run other = (Run) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
