package eu.buzea.datamodel.repositories;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import eu.buzea.datamodel.entities.Run;

/**
 * Dataset related queries and CRUD operations
 * 
 * @author Vlad
 *
 */
@Repository
public interface RunRepository extends GraphRepository<Run> {

	/**
	 * Deletes entire dataset, with all the contained data.
	 * 
	 * @param name
	 *            Name of the dataset to delete
	 */
	@Query("Match (r:Run{name: {0} }) -[contains_time]->(t:Time), t-[contains_edge]->(e:Edge),e-[contains_user]->(u:User) delete r,contains_time,t,contains_edge,e,u,contains_user;")
	void deleteRunByName(String name);

	/**
	 * Retrieves a dataset Node from the database using it's name
	 * 
	 * @param name
	 *            Name of the Run to be searched
	 * @return the Run if found, null otherwise
	 */
	Run findByName(String name);

	/**
	 * Finds the value of the latest time slice created in the run
	 * 
	 * @param runName
	 *            The name of the run in which to get the value
	 * @return The maximum time value or null if no time slices exist
	 */
	@Query("match (r:Run{name: {0} })-[]->(t:Time) return max(t.value);")
	Long maxTimeValue(String runName);
}
