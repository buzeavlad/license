package eu.buzea.processing;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.buzea.inputschema.Input;
import eu.buzea.util.XmlUtil;

/**
 * Rest Service exposed to call {@link SocialNetworkProcessing}
 * 
 * @author Vlad
 *
 */
@RestController
public class SpringRestController {

	/**
	 * Inferior layer. Actual processing happens here.
	 */
	@Autowired
	SocialNetworkProcessing processing;

	/**
	 * Receives xml input data as multipart file. This method is useful when the
	 * extracted data is already saved as a file in the file system.
	 * 
	 * @param xml
	 *            The file containing the xml with data extracted from social
	 *            networks
	 * @return {@literal false} in case an error occurred during processing,
	 *         {@literal true} otherwise
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/processing/multipartfile")
	public boolean processingMultipart(
			@RequestParam(value = "xml", required = true) MultipartFile xml) {
		try {
			InputStream is = xml.getInputStream();
			Input input = XmlUtil.unmarshallInput(is);
			processing.processInput(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Receives extracted data as xml in the Request Body. This method is most
	 * recommended when the data from the social network has just been extracted
	 * and can be sent for processing without saving it to a file on the hard
	 * disk.
	 * 
	 * @param input
	 *            XML String unmarshalled by Spring
	 * @return {@literal false} in case an error occurred during processing,
	 *         {@literal true} otherwise
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/processing/input")
	@Consumes(MediaType.APPLICATION_XML)
	public boolean processingInput(@RequestBody Input input) {
		try {

			processing.processInput(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/processing/input")
	public String sayHello() {
		return "At this adress you should send the xml String using in the request body of a HTTP POST method";
	}
	/**
	 * Method that receives extracted data xml as a http parameter String. The
	 * use of this method is highly discouraged
	 * 
	 * @param inputString
	 *            the xml String containing data extracted from social networks
	 * @return {@literal false} in case an error occurred during processing,
	 *         {@literal true} otherwise
	 */
	@Deprecated
	@RequestMapping(method = RequestMethod.POST, value = "/processing/inputParam")
	public boolean processingInputParam(
			@RequestParam(name = "input", required = true) String inputString) {
		try {

			Input input = XmlUtil.unmarshallInput(inputString);
			processing.processInput(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
