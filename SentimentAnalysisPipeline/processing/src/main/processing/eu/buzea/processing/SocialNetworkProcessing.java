package eu.buzea.processing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.cb.services.alchemy.AlchemySentimentAnalysisService;
import com.cb.services.monkeylearn.MonkeyLearnProfanityDetectionService;
import com.cb.services.topicsimilarity.CosineSimilartyTopicAnalysisService;

import eu.buzea.datamodel.entities.Edge;
import eu.buzea.datamodel.entities.Run;
import eu.buzea.datamodel.entities.Time;
import eu.buzea.datamodel.entities.UserNode;
import eu.buzea.datamodel.repositories.EdgeRepository;
import eu.buzea.datamodel.repositories.RunRepository;
import eu.buzea.datamodel.repositories.UserRepository;
import eu.buzea.inputschema.Content;
import eu.buzea.inputschema.Input;
import eu.buzea.services.ProfanityDetectionService;
import eu.buzea.services.Sentiment;
import eu.buzea.services.SentimentAnalysisService;
import eu.buzea.services.TopicAnalysisService;

/**
 * Class that receives as input the data extracted from the social networks,
 * processes it using sentiment analysis tools and stores the resulting TVG in
 * the neo4j Database
 * 
 * @author Vlad-Calin Buzea
 *
 */
@Service
@Scope("prototype")
public class SocialNetworkProcessing {

	/**
	 * Log4j Logger
	 */
	private static final Logger LOG = Logger.getLogger(SocialNetworkProcessing.class);

	/**
	 * Constant determining after how many comments to create a new time slice,
	 * even though we have not moved to the next post from the input. The new
	 * time slice will be created only after reaching the next comment of depth
	 * 1.
	 */
	private static final int COMMENTS_PER_TIME_SLICE = 500;

	/**
	 * Neo4j Repository
	 */
	@Autowired
	RunRepository runRepository;
	/**
	 * Neo4j Repository
	 */
	@Autowired
	EdgeRepository edgeRepository;
	/**
	 * Neo4j Repository
	 */
	@Autowired
	UserRepository userRepository;

	/**
	 * Sentiment Analysis Service
	 */
	private SentimentAnalysisService sentimentAnalisysService;

	/**
	 * Subject Analysis Service
	 */
	private TopicAnalysisService topicAnalysisService;

	/**
	 * Profanity Detection Service
	 */
	private ProfanityDetectionService profanityDetectionService;

	/**
	 * The users referenced in the current input
	 */
	private Map<Long, UserNode> users;

	/**
	 * The current dataset being processed
	 */
	private Run run;

	/**
	 * Number of comments/replies processed until now in current time-slice
	 */
	private int interactionCount;

	/**
	 * Current time slice value counter
	 */
	private long currentTimeValue;

	/**
	 * The time entity currenly processed
	 */
	private Time time;

	/**
	 * List of words that segment comment in multiple opinions.
	 *
	 */
	public static final String[] OPINION_SEPARATORS = {"but", "although", "however",
			"nevertheless"};

	/**
	 * Any Spring service requires a default constructor, with no parameters
	 */
	public SocialNetworkProcessing() {
		// could be made spring beans
		sentimentAnalisysService = new AlchemySentimentAnalysisService();
		profanityDetectionService = new MonkeyLearnProfanityDetectionService();
		topicAnalysisService = new CosineSimilartyTopicAnalysisService();
	}

	/**
	 * Processes data extracted from social networks using the sentiment
	 * analysis tool and stores the resulting TVG in the neo4j Database
	 * 
	 * @param input
	 */
	public void processInput(Input input) {
		// set up run (data set)
		LOG.info("Setting up run");
		setUpRun(input);

		// read users
		LOG.info("Reading Users");
		readAllUsers(input);

		// sort comments by date
		LOG.info("Sorting Posts");
		List<Content> posts = input.getPosts().getPost();
		posts.sort(new ContentComparator());

		LOG.info("Processing Posts");
		// continuous numbering time value
		Long prevTimeValue = runRepository.maxTimeValue(run.getName());
		if (prevTimeValue == null) {
			prevTimeValue = new Long(0);
		}
		currentTimeValue = prevTimeValue.longValue() + 1;

		int size = posts.size();
		for (int i = 0; i < size; i++) {
			LOG.info("Processing Post " + i);
			Content post = posts.remove(0);
			time = new Time();
			time.setParent(run);
			time.setValue(currentTimeValue);
			String postText = post.getText().trim();
			// posts that were pictures or other type content
			if (postText.isEmpty()) {
				postText = Sentiment.NEUTRAL.toString();
				post.setText(postText);
			}
			time.setPostText(postText);
			processAllReplies(post, true);
			currentTimeValue++;
			LOG.info("Finished processing comments");
		}

	}

	/**
	 * Finds or Creates new run with the given name. Deletes previous run if
	 * Necessary.
	 * 
	 * @param input
	 *            Unmarshalled xml input
	 */
	private void setUpRun(Input input) {
		run = null;
		String runName = input.getRunName();

		if (input.isOverwritePreviousDataSet()) {
			runRepository.deleteRunByName(runName);
		} else {
			run = runRepository.findByName(runName);
		}

		if (run == null) {
			run = new Run();
			run.setName(runName);
			run.setDescription(input.getDescription());
		}
	}

	/**
	 * Reads all the users from the input and saves them in {@link #users}. Also
	 * synchronizes existing users with ones existing the database based on the
	 * social network id.
	 * 
	 * @param input
	 */
	private void readAllUsers(Input input) {
		users = new HashMap<>();
		List<eu.buzea.inputschema.User> userList = input.getUsers().getUser();
		String runName = run.getName();
		for (eu.buzea.inputschema.User u : userList) {
			long id = u.getId();
			UserNode neo4jUser = userRepository.findByRunAndSocialNetworkId(id, runName);
			if (neo4jUser == null) {
				neo4jUser = new UserNode();
				neo4jUser.setName(u.getName());
				neo4jUser.setSocialNetworkId(id);
				// userRepository.save(neo4jUser);
			}
			users.put(neo4jUser.getSocialNetworkId(), neo4jUser);
		}
	}

	/**
	 * Processes all comments/replies of a post
	 * 
	 * @param post
	 *            main thread
	 * @param shouldSplit
	 *            true if you want to slit time slice after
	 *            {@link SocialNetworkProcessing#COMMENTS_PER_TIME_SLICE}
	 *            comments (i.e. depth = 1), false otherwise
	 * 
	 */
	private void processAllReplies(Content post, boolean shouldSplit) {
		List<Content> comments = post.getReply();
		LOG.info("Processing comments");
		Content previousComment = null;
		for (Content comm : comments) {
			processReply(post, comm, previousComment);
			/*
			 * Initialize and avoid null pointers
			 */
			if (previousComment == null) {
				previousComment = comm;
			} else {
				// Replace previous comment if and only if valid comment
				if (comm != null) {
					// If comment is from the same user then concatenate text
					if (previousComment.getAuthorId() == comm.getAuthorId())
						previousComment.setText(previousComment.getText() + ". " + comm.getText());
					else {
						// normal case
						previousComment = comm;
					}
				}
			}

			if (interactionCount > COMMENTS_PER_TIME_SLICE && shouldSplit) {
				interactionCount = 0;
				currentTimeValue++;
				time = new Time();
				time.setParent(run);
				time.setValue(currentTimeValue);
				time.setPostText(post.getText());
			}
		}
	}

	/**
	 * Processes single reply, along with it's sub-replies.
	 * 
	 * @param post
	 *            Original Thread
	 * @param comment
	 *            Reply to Original Thread
	 * @param previousComment
	 *            Previous Sibling to comment
	 */
	private void processReply(Content post, Content comment, Content previousComment) {
		if (comment == null) {
			return;// deleted comment
		}
		String commentText = comment.getText().trim();
		if (commentText.isEmpty()) {
			commentText = Sentiment.NEUTRAL.toString();
			comment.setText(commentText);
		}
		LOG.info("Processing comment: " + commentText);
		long commAuthorId = comment.getAuthorId();
		long postAuthorId = post.getAuthorId();
		boolean isSameAuthor = (commAuthorId == postAuthorId);
		boolean isOnMainSubject = topicAnalysisService.isSameSubject(post.getText(), commentText);
		boolean isReplyToSibling = false;
		if (previousComment != null && !isOnMainSubject) {
			isReplyToSibling = topicAnalysisService.isSameSubject(previousComment.getText(),
					commentText);
		}
		if (isSameAuthor || (!isOnMainSubject && isReplyToSibling)) {
			// reply to sibling node
			processSiblingReply(previousComment, comment);
			processAllReplies(comment, false);
			return;
		}
		// reply on main thread
		boolean isOffTopic = !(isOnMainSubject || isReplyToSibling);

		boolean isSegmented = determineIfSegmented(commentText);
		Edge edge = findOrCreateEdge(postAuthorId, commAuthorId);
		if (isOffTopic) {
			boolean isProfanity = profanityDetectionService.isProfanity(commentText);
			if (isProfanity) {
				edge.setWeightNegative(edge.getWeightNegative() + 1);
				LOG.info("Profanity detected");
			} else {
				LOG.info("Off topic detected");
				edge.setWeightNeutral(edge.getWeightNeutral() + 1);
			}
		} else if (isSegmented) {
			edge = processSegmentedComment(post, comment, edge);
		} else {
			Sentiment postSentiment = sentimentAnalisysService.process(post.getText());
			LOG.info("Post sentiment = " + postSentiment);
			Sentiment commentSentiment = sentimentAnalisysService.process(commentText);
			LOG.info("Comment sentiment = " + commentSentiment);
			boolean isPositiveInteraction = isPositiveInteraction(commentSentiment, postSentiment);
			if (isPositiveInteraction)
				edge.setWeightPositive(edge.getWeightPositive() + 1);
			else
				edge.setWeightNegative(edge.getWeightNegative() + 1);
		}

		saveEdge(edge);
		processAllReplies(comment, false);

	}

	/**
	 * Splits comment into chunks, using
	 * {@link SocialNetworkProcessing#OPINION_SEPARATORS} and processes each
	 * chunk individually
	 * 
	 * @param post
	 *            main thread
	 * @param comment
	 *            comment to main thread
	 * @param edge
	 *            edge between comment author and post author
	 * @return the edge with new values
	 */
	private Edge processSegmentedComment(Content post, Content comment, Edge edge) {
		String commentText = comment.getText().toLowerCase();
		// separate input into chunks
		List<String> result = new ArrayList<>();
		result.add(commentText);
		for (String separator : SocialNetworkProcessing.OPINION_SEPARATORS) {
			result = separate(result, separator);
		}
		Sentiment postSentiment = sentimentAnalisysService.process(post.getText());
		LOG.info("Post sentiment = " + postSentiment);
		for (String commentChunk : result) {
			Sentiment commentSentiment = sentimentAnalisysService.process(commentChunk);
			LOG.info("Comment sentiment = " + commentSentiment);
			boolean isPositiveInteraction = isPositiveInteraction(commentSentiment, postSentiment);
			if (isPositiveInteraction)
				edge.setWeightPositive(edge.getWeightPositive() + 1);
			else
				edge.setWeightNegative(edge.getWeightNegative() + 1);
		}
		return edge;
	}

	/**
	 * Separates elements of the input list into more elements using the
	 * separator parameter
	 * 
	 * @param result
	 * @param separator
	 * @return
	 */
	private List<String> separate(List<String> input, String separator) {
		List<String> result = new ArrayList<>();
		for (String i : input) {
			String wordDelimiterRegex = "[\\., \\?\\!]+";
			String[] chunks = i.split(wordDelimiterRegex + separator + wordDelimiterRegex);
			List<String> temp = Arrays.asList(chunks);
			result.addAll(temp);
		}
		return result;
	}

	/**
	 * Checks if the comment contains any of the opinion separators
	 * 
	 * @param commentText
	 *            comment text
	 * @return true if the comment should be segmented, false otherwise
	 */
	private boolean determineIfSegmented(String commentText) {
		commentText = commentText.toLowerCase();
		for (String opinionSeparator : SocialNetworkProcessing.OPINION_SEPARATORS) {
			String startOfWordRegex = "(?s).*\\b";
			String endOfWordRegex = "\\b.*";
			boolean isSeparatorPresent = commentText
					.matches(startOfWordRegex + opinionSeparator + endOfWordRegex);
			if (isSeparatorPresent)
				return true;
		}
		return false;
	}

	/**
	 * Process comment to sibling comment, that is not related to main thread
	 * 
	 * @param previousComment
	 * @param comment
	 */
	private void processSiblingReply(Content previousComment, Content comment) {
		if (previousComment == null || comment == null) {
			return;
		}
		long postAuthorId = previousComment.getAuthorId();
		long commentAuthorId = comment.getAuthorId();
		boolean isDifferentAuthor = (postAuthorId != commentAuthorId);
		if (isDifferentAuthor) {
			Edge edge = findOrCreateEdge(postAuthorId, commentAuthorId);
			String commentText = comment.getText();
			boolean isSegmented = determineIfSegmented(commentText);
			if (isSegmented) {
				edge = processSegmentedComment(previousComment, comment, edge);
			} else {
				Sentiment postSentiment = sentimentAnalisysService
						.process(previousComment.getText());
				LOG.info("Post sentiment = " + postSentiment);

				Sentiment commentSentiment = sentimentAnalisysService.process(commentText);
				LOG.info("Comment sentiment = " + commentSentiment);
				boolean isPositiveInteraction = isPositiveInteraction(commentSentiment,
						postSentiment);
				if (isPositiveInteraction)
					edge.setWeightPositive(edge.getWeightPositive() + 1);
				else
					edge.setWeightNegative(edge.getWeightNegative() + 1);
			}
			saveEdge(edge);
		}

		for (Content reply : previousComment.getReply()) {
			processSiblingReply(reply, comment);
		}

	}

	/**
	 * Mapping of interaction table
	 * 
	 * @param commentSentiment
	 *            The sentiment detected in the comment (Content #2)
	 * @param postSentiment
	 *            The sentiment detected in the post (Content #1)
	 * @return true if positive interaction, false otherwise
	 */
	private boolean isPositiveInteraction(Sentiment commentSentiment, Sentiment postSentiment) {
		if (commentSentiment.equals(Sentiment.NEGATIVE)) {
			if (!postSentiment.equals(Sentiment.NEGATIVE)) {
				return false;
			}
		} else if (postSentiment.equals(Sentiment.NEGATIVE)) {
			return false;
		}

		return true;
	}

	/**
	 * Finds edge between user with startAuthorId and user with endAuthorId, in
	 * the dataset currently processed. If no such edge exists then it creates
	 * one.
	 * 
	 * @param endAuthorId
	 *            social network id if the end node (user)
	 * @param startAuthorId
	 *            social network id if the start node (user)
	 * @return The edge between the two users
	 */
	private Edge findOrCreateEdge(Long endAuthorId, Long startAuthorId) {
		Edge previousEdge = edgeRepository.findEdgeBefore(startAuthorId, endAuthorId,
				currentTimeValue, run.getName());
		if (previousEdge != null) {
			previousEdge = edgeRepository.findOne(previousEdge.getId());// get
																		// all
																		// references
			if (currentTimeValue == previousEdge.getTime().getValue()) {
				return previousEdge;
			} else {
				Edge newEdge = new Edge();
				newEdge.setTime(time);
				UserNode commAuthor = users.get(startAuthorId);
				UserNode postAuthor = users.get(endAuthorId);
				newEdge.setEndNode(postAuthor);
				newEdge.setStartNode(commAuthor);
				newEdge.setWeightNegative(previousEdge.getWeightNegative());
				newEdge.setWeightNeutral(previousEdge.getWeightNeutral());
				newEdge.setWeightPositive(previousEdge.getWeightPositive());
				return newEdge;

			}
		}
		Edge newEdge = new Edge();
		newEdge.setTime(time);
		UserNode commAuthor = users.get(startAuthorId);
		UserNode postAuthor = users.get(endAuthorId);
		newEdge.setEndNode(postAuthor);
		newEdge.setStartNode(commAuthor);

		return newEdge;
	}

	/**
	 * Saves edge in neo4j database. Retries once in case of error.
	 * 
	 * @param edge
	 */
	private void saveEdge(Edge edge) {
		int tryesLeft = 2;
		while (tryesLeft > 0) {
			try {
				edge = edgeRepository.save(edge);
				interactionCount++;
				LOG.info("New Edge processed&saved: (" + edge.getStartNode().getSocialNetworkId()
						+ "->" + edge.getEndNode().getSocialNetworkId() + ") = ["
						+ edge.getWeightPositive() + ", " + edge.getWeightNeutral() + ", "
						+ edge.getWeightNegative() + "]");
				tryesLeft = 0;
			} catch (Exception e) {
				LOG.error("Spring Data NEO4J Exception", e);
				tryesLeft--;
			}
		}
	}

	/**
	 * Compares {@link Content} based on {@link Content#getDate()} in
	 * chronological order
	 * 
	 * @author Vlad
	 *
	 */
	public class ContentComparator implements Comparator<Content> {

		@Override
		public int compare(Content o1, Content o2) {
			XMLGregorianCalendar date1 = o1.getDate();
			XMLGregorianCalendar date2 = o2.getDate();
			return date1.compare(date2);
		}
	}

	public SentimentAnalysisService getSentimentAnalisysService() {
		return sentimentAnalisysService;
	}

	public void setSentimentAnalisysService(SentimentAnalysisService sentimentAnalisysService) {
		this.sentimentAnalisysService = sentimentAnalisysService;
	}
}
