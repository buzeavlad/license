package eu.buzea.start;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.server.Neo4jServer;
import org.springframework.data.neo4j.server.RemoteServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring configuration and main method
 * 
 * @author Vlad
 *
 */
@ComponentScan("eu.buzea")
@EnableAutoConfiguration
@EnableNeo4jRepositories("eu.buzea.datamodel.repositories")
@EnableTransactionManagement
@SpringBootApplication
public class Application extends Neo4jConfiguration {

	public Application() {
		System.setProperty("username", "neo4j");
		System.setProperty("password", "Software");

	}

	@Override
	public Neo4jServer neo4jServer() {
		return new RemoteServer("http://localhost:7474");
	}

	@Override
	public SessionFactory getSessionFactory() {
		return new SessionFactory("eu.buzea.datamodel.entities");
	}

	@Override
	@Bean
	@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public Session getSession() throws Exception {
		return super.getSession();
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
