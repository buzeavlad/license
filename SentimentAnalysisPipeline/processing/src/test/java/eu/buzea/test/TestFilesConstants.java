package eu.buzea.test;

/**
 * String Constants used in JUnit Tests.
 * 
 * @author Vlad-Calin Buzea
 *
 */
public final class TestFilesConstants {

	/**
	 * Test resource folder path
	 */
	public static final String SRC_TEST_RESOURCES = "./src/test/resources";
	/**
	 * Crash output file path
	 */
	public static final String CRASH_OUTPUT_FILE = SRC_TEST_RESOURCES
			+ "/CrashOutput.xml";
	/**
	 * Processing input file path
	 */
	public static final String REDDIT_EXTRACTION_INPUT_2 = SRC_TEST_RESOURCES
			+ "/RedditExtractionInputV2.xml";
	/**
	 * Processing input file path
	 */
	public static final String SIMPLE_DEMO_INPUT = SRC_TEST_RESOURCES
			+ "/SimpleDemoInput.xml";
	/**
	 * Processing input file path
	 */
	public static final String DEMO_INPUT = SRC_TEST_RESOURCES
			+ "/DemoInput.xml";
	/**
	 * Processing input file path
	 */
	public static final String FACEBOOK_POLITICS_INPUT = SRC_TEST_RESOURCES
			+ "/FacebookPoliticsInput.xml";

}
