package eu.buzea.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.buzea.datamodel.config.PersistenceContext;
import eu.buzea.datamodel.entities.Edge;
import eu.buzea.datamodel.entities.Run;
import eu.buzea.datamodel.entities.Time;
import eu.buzea.datamodel.entities.UserNode;
import eu.buzea.datamodel.repositories.EdgeRepository;
import eu.buzea.datamodel.repositories.RunRepository;

@ContextConfiguration(classes = {PersistenceContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DataModelTest {

	private static final String SNA_CLUB = "SNA Club";

	@Autowired
	EdgeRepository edgeRepository;

	@Autowired
	RunRepository runRepository;

	@Test
	public void createExampleGraph() {
		runRepository.deleteRunByName(SNA_CLUB);
		Edge edge = new Edge();

		UserNode vlad = new UserNode();
		vlad.setName("Vlad-JAVA");
		vlad.setSocialNetworkId(0);

		UserNode cipri = new UserNode();
		cipri.setName("Cipri-JAVA");
		cipri.setSocialNetworkId(1);

		edge.setEndNode(vlad);
		edge.setStartNode(cipri);
		edge.setWeightNegative(0);
		edge.setWeightPositive(100);

		// edgeRepository.save(edge);

		Time time1 = new Time(1);

		Time time2 = new Time(2);

		Run run = new Run();
		run.setName(SNA_CLUB);
		run.setDescription("Best team in Cluj");

		// run.add(time1);
		time1.setParent(run);
		// run.add(time2);
		time2.setParent(run);

		edge.setTime(time2);

		edgeRepository.save(edge);

	}

	@Test
	public void testFindRun() {
		runRepository.deleteRunByName("deleteME");
		Run run = runRepository.findByName("deleteME");
		assertNull(run);
	}

	@Test
	public void testFindEdge() {
		Edge e = edgeRepository.findEdgeBefore(1, 0, 2, SNA_CLUB);
		assertNotNull(e);
		assertNull(e.getTime());
		e = edgeRepository.findOne(e.getId());
		assertNotNull(e.getTime());
	}

}
