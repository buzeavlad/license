package eu.buzea.test;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.buzea.datamodel.config.PersistenceContext;
import eu.buzea.inputschema.Input;
import eu.buzea.processing.SocialNetworkProcessing;
import eu.buzea.util.XmlUtil;

@ContextConfiguration(classes = {PersistenceContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProcessingTest {

	private static final Logger LOG = Logger.getLogger(ProcessingTest.class);
	@Autowired
	SocialNetworkProcessing processing;

	// @Test
	public void test() throws JAXBException {
		LOG.info("\r\n\r\nSTARTED TEST");
		File file = new File(TestFilesConstants.FACEBOOK_POLITICS_INPUT);
		LOG.info("Unmarshall Strating");
		Input input = XmlUtil.unmarshallInput(file);
		LOG.info("Unmarshall Finished");
		LOG.info("Starting processing");
		try {
			processing.processInput(input);
			LOG.info("Finished processing");
		} catch (Exception fatality) {
			try {
				fatality.printStackTrace();
				XmlUtil.serialize(input, new File(TestFilesConstants.CRASH_OUTPUT_FILE));
			} catch (FileNotFoundException | JAXBException e) {
				LOG.error("FAILED TO SAVE DATA");
			}
		}
	}

	@Test
	public void demo() throws Exception {
		File file = new File(TestFilesConstants.DEMO_INPUT);
		Input input = XmlUtil.unmarshallInput(file);
		processing.processInput(input);
	}

}
