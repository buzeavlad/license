package eu.buzea.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;

import eu.buzea.inputschema.Input;
import eu.buzea.util.RestServiceUtil;
import eu.buzea.util.XmlUtil;

public class RestServiceTest {

	public static final String SMALL_INPUT_FILE = TestFilesConstants.DEMO_INPUT;
	public static final String BIG_INPUT_FILE = TestFilesConstants.REDDIT_EXTRACTION_INPUT_2;
	private static RestServiceUtil restUtil = new RestServiceUtil("http://localhost:8080");

	// @Test
	public void testMultipartFilePost() throws FileNotFoundException, IOException {
		File file = new File(BIG_INPUT_FILE);
		Boolean response = restUtil.postMultipartFile(file);
		assertTrue(response);

	}

	@Test
	public void testRestBodyPost() throws Exception {
		File file = new File(SMALL_INPUT_FILE);
		Input input = XmlUtil.unmarshallInput(file);
		Boolean response = restUtil.postObject(input);
		assertTrue(response);

	}

	// @Test
	public void testRestBodyPostConcurrent() throws Exception {
		File file = new File(TestFilesConstants.DEMO_INPUT);
		Input input = XmlUtil.unmarshallInput(file);
		Boolean response = restUtil.postObject(input);
		assertTrue(response);

	}

	@Test
	@SuppressWarnings("deprecation")
	public void testRestStringParameterPost() throws Exception {
		File file = new File(BIG_INPUT_FILE);
		String input = IOUtils.toString(new FileInputStream(file));
		Boolean response = restUtil.postParameterString(input);
		assertTrue(response);

	}

	public String postMultipart(File xml, String uri) throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost uploadFile = new HttpPost(uri);

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addBinaryBody("xml", xml, ContentType.APPLICATION_OCTET_STREAM, "file.ext");
		HttpEntity multipart = builder.build();
		uploadFile.setEntity(multipart);
		CloseableHttpResponse response = httpClient.execute(uploadFile);
		InputStream contentStream = response.getEntity().getContent();
		return IOUtils.toString(contentStream);
	}
}
