package eu.buzea.datamodel.config;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.server.InProcessServer;
import org.springframework.data.neo4j.server.Neo4jServer;

/**
 * Spring Configuration class used ONLY for tests. It uses a local instance of
 * the Neo4j Database.
 * 
 * @author Vlad-Calin Buzea
 *
 */
@Configuration
@ComponentScan("eu.buzea")
@EnableAutoConfiguration
@EnableNeo4jRepositories("eu.buzea.datamodel.repositories")
public class PersistenceContext extends Neo4jConfiguration {

	public static final int NEO4J_PORT = 7474;

	public PersistenceContext() {
		super();
		System.setProperty("username", "neo4j");
		System.setProperty("password", "Software");
	}

	@Override
	public Neo4jServer neo4jServer() {
		return new InProcessServer();
	}

	@Override
	public SessionFactory getSessionFactory() {
		return new SessionFactory("eu.buzea.datamodel.entities");
	}

	@Override
	@Bean
	public Session getSession() throws Exception {
		return super.getSession();
	}

}
