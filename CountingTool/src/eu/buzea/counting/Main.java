package eu.buzea.counting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

	private static final String OUTPUT_TXT = "output.txt";

	public static void main(String[] args) {
		launch(args);
	}

	private int[][] positive = new int[100][100];
	private int[][] neutral = new int[100][100];
	private int[][] negative = new int[100][100];
	private Spinner<Integer> startUserField;
	private Spinner<Integer> endUserField;

	@Override
	public void init() {
		try {
			Scanner scanner = new Scanner(new File(OUTPUT_TXT));

			for (int i = 0; i < 100; i++) {
				for (int j = 0; j < 100; j++) {
					positive[i][j] = scanner.nextInt();
				}
			}

			for (int i = 0; i < 100; i++) {
				for (int j = 0; j < 100; j++) {
					neutral[i][j] = scanner.nextInt();
				}

			}

			for (int i = 0; i < 100; i++) {
				for (int j = 0; j < 100; j++) {
					negative[i][j] = scanner.nextInt();
				}

			}
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("No previous data saved");
		}
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX Counting Tool");
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Scene scene = new Scene(grid);
		primaryStage.setScene(scene);

		Text scenetitle = new Text("Counting tool");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);

		Label userName = new Label("Start User ID:");
		grid.add(userName, 0, 1);

		startUserField = new Spinner<Integer>();
		startUserField.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100));
		startUserField.setEditable(true);
		grid.add(startUserField, 1, 1);

		Label pw = new Label("End User ID:");
		grid.add(pw, 0, 3);

		endUserField = new Spinner<Integer>();
		endUserField.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100));
		endUserField.setEditable(true);
		grid.add(endUserField, 1, 3);

		Button btn = new Button("Positive");
		HBox hbBtn = new HBox(20);
		hbBtn.setAlignment(Pos.CENTER);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 2, 1);

		Button btnNeutral = new Button("Neutral");
		HBox hbBtnNeutral = new HBox(20);
		hbBtnNeutral.setAlignment(Pos.CENTER);
		hbBtnNeutral.getChildren().add(btnNeutral);
		grid.add(hbBtnNeutral, 2, 2);

		Button btnNeg = new Button("Negative");
		HBox hbBtnNeg = new HBox(20);
		hbBtnNeg.setAlignment(Pos.CENTER);
		hbBtnNeg.getChildren().add(btnNeg);
		grid.add(hbBtnNeg, 2, 3);

		Button swap = new Button("Swap");
		HBox hbSwap = new HBox(20);
		hbSwap.setAlignment(Pos.CENTER);
		hbSwap.getChildren().add(swap);
		grid.add(hbSwap, 1, 2);

		swap.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				int start = startUserField.getValue();
				int end = endUserField.getValue();
				endUserField.getValueFactory().setValue(start);
				startUserField.getValueFactory().setValue(end);
			}
		});
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				int start = startUserField.getValue();
				int end = endUserField.getValue();
				positive[start][end]++;

				showAlert(start, end);
			}

		});

		btnNeutral.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				int start = startUserField.getValue();
				int end = endUserField.getValue();
				neutral[start][end]++;
				showAlert(start, end);
			}
		});

		btnNeg.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				int start = startUserField.getValue();
				int end = endUserField.getValue();
				negative[start][end]++;
				showAlert(start, end);
			}
		});

		Button save = new Button("Save");
		HBox hbSave = new HBox(20);
		hbSave.setAlignment(Pos.BOTTOM_RIGHT);
		hbSave.getChildren().add(save);
		grid.add(hbSave, 1, 4);
		save.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					PrintWriter out = new PrintWriter(OUTPUT_TXT);
					for (int i = 0; i < 100; i++) {
						for (int j = 0; j < 100; j++) {
							out.write(positive[i][j] + " ");
						}
						out.write("\n");
					}
					out.write("\n");
					for (int i = 0; i < 100; i++) {
						for (int j = 0; j < 100; j++) {
							out.write(neutral[i][j] + " ");
						}
						out.write("\n");
					}
					out.write("\n");
					for (int i = 0; i < 100; i++) {
						for (int j = 0; j < 100; j++) {
							out.write(negative[i][j] + " ");
						}
						out.write("\n");
					}
					out.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

			}
		});

		primaryStage.show();
	}

	private void showAlert(int start, int end) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information Dialog");
		alert.setHeaderText("Current Value " + start + " -> " + end);
		alert.setContentText(
				"(" + positive[start][end] + ", " + neutral[start][end] + ", " + negative[start][end] + ")");

		alert.showAndWait();
	}

}
